<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\Admin\AccessRightController;
use Illuminate\Support\Facades\Route;
use Auth0\Login\Auth0Controller;
use App\Http\Controllers\Auth\Auth0IndexController;

Route::get('/auth0/callback', [Auth0Controller::class, 'callback'])->name('auth0-callback');
Route::get('/login', [Auth0IndexController::class, 'login'])->name('login');
Route::post('/logout', [Auth0IndexController::class, 'logout'])->name('logout');
Route::get('/profile', [Auth0IndexController::class, 'profile'])->name('profile');
Route::get('/', function () {
    return redirect()->route('admin.command.index');
});

Route::group(['prefix' => 'admin'], function () {

    // Command Routes
    Route::get('/command', 'Admin\AdminController@index')->name('admin.command.index');

    // Access-right Route
    Route::get('/access-rights', [AccessRightController::class, 'index'])->name('admin.access-right.index');
    Route::post('/access-rights', [AccessRightController::class, 'store'])->name('admin.access-right.store');
    Route::post('/access-rights/{id}', [AccessRightController::class, 'update'])->name('admin.access-right.update');
    Route::delete('/access-rights/{id}', [AccessRightController::class, 'destroy'])->name('admin.access-right.destroy');
    // Crawlers Route
    Route::get('/crawler-edit/{id}', 'Admin\CrawlerController@edit')->name('admin.command.crawler-edit');
    Route::post('/crawler-update', 'Admin\CrawlerController@update')->name('admin.command.crawler-update');
    Route::get('/crawler-result/{id}', 'Admin\CrawlerController@crawlerResult')->name('admin.command.crawler-result');
    Route::get('/crawler-result-edit/{id}/{index}', 'Admin\CrawlerController@crawlerResultEdit')->name('admin.command.crawler-result-edit');
    Route::get('/crawler-result-delete/{id}/{index}', 'Admin\CrawlerController@crawlerResultDelete')->name('admin.command.crawler-result-delete');
    Route::post('/crawler-result-update', 'Admin\CrawlerController@crawlerResultUpdate')->name('admin.command.crawler-result-update');
    Route::get('/crawler-status', 'Admin\CrawlerController@crawlerStatus')->name('admin.command.crawler-status');

    // Parser Route
    Route::get('/parser-list/{code}', 'Admin\ParserController@list')->name('admin.command.parser-list');
    Route::get('/parser-edit/{id}/{code}', 'Admin\ParserController@edit')->name('admin.command.parser-edit');
    Route::post('/parser-update', 'Admin\ParserController@update')->name('admin.command.parser-update');
    Route::get('/parser-delete/{id}/{code}', 'Admin\ParserController@destroy')->name('admin.command.parser-delete');
    Route::get('/parser-status', 'Admin\ParserController@parserStatus')->name('admin.command.parser-status');

    // Publication Routes
    Route::get('/publication', 'Admin\AdminController@publication')->name('admin.publication.index');
    Route::get('/publication-list/{code}', 'Admin\AdminController@publicationList')->name('admin.publication.list');
    Route::get('/publication-publish/{id}', 'Admin\AdminController@publicationPublish')->name('admin.publication.publish');
    Route::get('/publication-unpublish/{id}', 'Admin\AdminController@publicationUnPublish')->name('admin.publication.unpublish');
    Route::get('/publication-publishunpublish', 'Admin\AdminController@publicationPublishUnPublish')->name('admin.publication.publishunpublish');
    Route::post('/publication-update', 'Admin\AdminController@publicationUpdate')->name('admin.publication.update');

    Route::get('/publication-delete/{id}', 'Admin\AdminController@publicationDelete')->name('admin.publication.delete');

    /**
     * Parameters Routes
     */
    // Referentiels Routes
    Route::get('/referentiels', 'Admin\ReferentielsController@index')->name('admin.parameter.referentiels-index');
    Route::get('/referentiels-edit/{id}', 'Admin\ReferentielsController@edit')->name('admin.parameter.referentiels-edit');

    // Update Referenciel Fonction
    Route::post('/referentiels-create-fonction', 'Admin\ReferentielsController@storeFonction')->name('admin.parameter.referentiels-create-fonction');
    Route::post('/referentiels-update-fonction', 'Admin\ReferentielsController@updateFonction')->name('admin.parameter.referentiels-update-fonction');

    // Update Referenciel Secteur
    Route::post('/referentiels-create-secteur', 'Admin\ReferentielsController@storeSecteur')->name('admin.parameter.referentiels-create-secteur');
    Route::post('/referentiels-update-secteur', 'Admin\ReferentielsController@updateSecteur')->name('admin.parameter.referentiels-update-secteur');

    // Update Referenciel Theme
    Route::post('/referentiels-create-theme', 'Admin\ReferentielsController@storeTheme')->name('admin.parameter.referentiels-create-theme');
    Route::post('/referentiels-update-theme', 'Admin\ReferentielsController@updateTheme')->name('admin.parameter.referentiels-update-theme');

    // Update Referenciel TypeDeInfo
    Route::post('/referentiels-create-typedeinfo', 'Admin\ReferentielsController@storeTypeInfo')->name('admin.parameter.referentiels-create-typedeinfo');
    Route::post('/referentiels-update-typedeinfo', 'Admin\ReferentielsController@updateTypeInfo')->name('admin.parameter.referentiels-update-typedeinfo');

    // Update Referenciel Langue
    Route::post('/referentiels-update-langue', 'Admin\ReferentielsController@updateLangue')->name('admin.parameter.referentiels-update-langue');

    // create Referenciel Application
    Route::post('/referentiels-create-application', 'Admin\ReferentielsController@createApplication')->name('admin.parameter.referentiels-create-application');

    // create Referenciel Application
    Route::post('/referentiels-create-authgroup', 'Admin\ReferentielsController@createAuthGroup')->name('admin.parameter.referentiels-create-authgroup');
    Route::post('/referentiels-update-authgroup', 'Admin\ReferentielsController@updateAuthGroup')->name('admin.parameter.referentiels-update-authgroup');

    // Delete Referenciel Items
    Route::post('/referentiels-delete-items', 'Admin\ReferentielsController@destroy')->name('admin.parameter.referentiels-delete-items');


    // XML Routes
    Route::get('/xml', 'Admin\XMLController@index')->name('admin.parameter.xml-index');

    // KM Routes
    Route::get('/km', 'Admin\KMController@index')->name('admin.parameter.km-index');
    Route::post('/km-store', 'Admin\KMController@store')->name('admin.parameter.km-store');
    Route::post('/km-update', 'Admin\KMController@update')->name('admin.parameter.km-update');
    Route::post('/km-delete', 'Admin\KMController@destroy')->name('admin.parameter.km-delete');

    Route::get('export', 'Admin\KMController@export')->name('admin.parameter.km-export');
    // Route::get('importExportView', 'Admin\KMController@importExportView');
    Route::post('import', 'Admin\KMController@import')->name('admin.parameter.km-import');

    // Filter Routes
    Route::get('/filter', 'Admin\FilterController@index')->name('admin.parameter.filter-index');

    // Origine Routes
    Route::get('/origin', 'Admin\OriginController@index')->name('admin.parameter.origin-index');
    Route::get('/origin-create', 'Admin\OriginController@create')->name('admin.parameter.origin-create');
    Route::post('/origin-store', 'Admin\OriginController@store')->name('admin.parameter.origin-store');
    Route::post('/origin-filter-store', 'Admin\OriginController@filterStore')->name('admin.parameter.origin-filter-store');
    Route::post('/origin-xml-store', 'Admin\OriginController@xmlStore')->name('admin.parameter.origin-xml-store');

    Route::get('/origin-edit/{code}', 'Admin\OriginController@edit')->name('admin.parameter.origin-edit-code');
    Route::post('/origin-update', 'Admin\OriginController@update')->name('admin.parameter.origin-update');
    Route::post('/origin-filter-update', 'Admin\OriginController@filterUpdate')->name('admin.parameter.origin-filter-update');
    Route::post('/origin-xml-update', 'Admin\OriginController@xmlUpdate')->name('admin.parameter.origin-xml-update');

    Route::post('/origin-delete', 'Admin\OriginController@destroy')->name('admin.parameter.origin-delete');
    Route::post('/origin-filter-delete', 'Admin\OriginController@destroyFilter')->name('admin.parameter.origin-filter-delete');
    Route::post('/origin-xml-delete', 'Admin\OriginController@destroyXml')->name('admin.parameter.origin-xml-delete');

    Route::get('/origin-edit/{id}', 'Admin\OriginController@edit')->name('admin.parameter.origin-edit');


    // Collecte Routes
    Route::get('/collect', 'Admin\CollectController@index')->name('admin.parameter.collect-index');
    Route::post('/collect-store', 'Admin\CollectController@store')->name('admin.parameter.collect-store');
    Route::get('/collect-edit/{id?}', 'Admin\CollectController@edit')->name('admin.parameter.collect-edit');
    Route::post('/collect-update', 'Admin\CollectController@update')->name('admin.parameter.collect-update');
    // Route::post('/origin-filter-store', 'Admin\OriginController@filterStore')->name('admin.parameter.origin-filter-store');
    // Route::post('/origin-xml-store', 'Admin\OriginController@xmlStore')->name('admin.parameter.origin-xml-store');


    // Check nom in jobs table API
    Route::post('/check-noms-in-jobs', 'Admin\ParserController@checkNomsInCollecte')->name('check-noms-in-jobs');

    Route::get('/import-log', 'Admin\AdminController@importLog')->name('admin.import.log');
    Route::get('/bi-engine', 'Admin\AdminController@biEngine')->name('admin.application.biengine');

});
