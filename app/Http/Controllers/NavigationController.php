<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class NavigationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dashboard()
    {
        $dashboardUrl = sprintf(
            '%sdashboard',
           substr( config('app.search_app_url'),0,-5 )
        );
        return Redirect::intended($dashboardUrl);
    }
}
