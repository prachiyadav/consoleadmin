<?php

namespace App\Http\Controllers\Admin;

use App\AuthGroup;
use App\Theme;
use App\Langue;
use App\Secteur;
use App\Fonction;
use App\TypeInfo;
use Carbon\Carbon;
use App\Application;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReferentielsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('admin.parameter.referentiels.index');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function storeFonction(Request $request)
    {
        if ($request->fonction_code == 'newcode') {
            // Get last parent origin
            $parentFonction = Fonction::where('code', 'LIKE', '%F-__')->limit(1)->orderBy('code', 'DESC')->first();

            if (empty($parentFonction)) {
                $parentFonction = Fonction::where('code', 'LIKE', '%F-_')->limit(1)->orderBy('code', 'DESC')->first();
            }

            // Create new Fonction code
            $lastDigitOfCode = explode('-', $parentFonction->code)[1];
            $newCode = 'F-' . ((int)$lastDigitOfCode + 1);

            $code = $newCode;

        } else {
            // Get parent Fonction if the code is same
            $parentFonction = Fonction::where('code', '=', $request->fonction_code)->first();

            if ($parentFonction) {
                // Get last child Fonction for the selected parent Fonction
                $childFonction = Fonction::where('code', 'LIKE', '%' . $parentFonction->code . '-%')->limit(1)->orderBy('id', 'DESC')->first();

                if ($childFonction) {
                    // Child found
                    $lastDigitOfChild = explode('-', $childFonction->code)[2];
                    $newChildCode = $parentFonction->code . '-' . ((int)$lastDigitOfChild + 1);
                    // Create new Child Fonction record
                    $code = $newChildCode;
                } else {
                    // Child not found [Create new child]
                    $newChildCode = $parentFonction->code . '-1';
                    $code = $newChildCode;
                }
            }
        }

        $fonction = [
            'code' => $code,
            'desc_fr' => $request->fonction_desc_fr,
            'desc_en' => $request->fonction_desc_en,
            'applications' => $this->arrayToString($request->fonction_applications)
        ];

        $res = Fonction::create($fonction);

        if ($res) {
            return back()->withSuccess('Fonction Created Successfully !');
        } else {
            return back()->withAlert('There was an error !');
        }
    }

    /**
     * Update Fonction In Referenciels.
     */
    public function updateFonction(Request $request)
    {

        // return $request->all();

        // dd(gettype($request->fonction_applications));

        $fonction = [
            'desc_fr' => $request->fonction_desc_fr,
            'desc_en' => $request->fonction_desc_en,
            'applications' => $this->arrayToString($request->fonction_applications)
        ];
        $res = Fonction::whereId($request->fonction_id)->update($fonction);

        if ($res) {
            return back()->withSuccess('Fonction Updated Successfully !');
        } else {
            return back()->withAlert('There was an error !');
        }
    }


    /**
     * =====================
     *  Secteur
     * ======================
     */

    /**
     * Store a newly created resource in storage.
     */
    public function storeSecteur(Request $request)
    {

        if ($request->secteur_code == 'newcode') {

            // Get last parent origin
            // $parentSecteur = Secteur::where('code', 'LIKE', '%S-__')->orWhere('code', 'LIKE', '%S-_')->limit(1)->orderBy('code', 'DESC')->first();
            $parentSecteur = Secteur::where('code', 'LIKE', '%S-__')->limit(1)->orderBy('code', 'DESC')->first();

            if (empty($parentSecteur)) {
                $parentSecteur = Secteur::where('code', 'LIKE', '%S-_')->limit(1)->orderBy('code', 'DESC')->first();
            }


            // Create new Secteur code
            $lastDigitOfCode = explode('-', $parentSecteur->code)[1];
            $newCode = 'S-' . ((int)$lastDigitOfCode + 1);

            $code = $newCode;

        } else {
            // Get parent Secteur if the code is same
            $parentSecteur = Secteur::where('code', '=', $request->secteur_code)->first();

            if ($parentSecteur) {
                // Get last child Secteur for the selected parent Secteur
                $childSecteur = Secteur::where('code', 'LIKE', '%' . $parentSecteur->code . '-%')->limit(1)->orderBy('id', 'DESC')->first();

                if ($childSecteur) {
                    // Child found
                    $lastDigitOfChild = explode('-', $childSecteur->code)[2];
                    $newChildCode = $parentSecteur->code . '-' . ((int)$lastDigitOfChild + 1);
                    // Create new Child Secteur record
                    $code = $newChildCode;
                } else {
                    // Child not found [Create new child]
                    $newChildCode = $parentSecteur->code . '-1';
                    $code = $newChildCode;
                }
            }
        }


        $secteur = [
            'code' => $code,
            'desc_fr' => $request->secteur_desc_fr,
            'desc_en' => $request->secteur_desc_en,
            'applications' => $this->arrayToString($request->secteur_applications)
        ];

        $res = Secteur::create($secteur);

        if ($res) {
            return back()->withSuccess('Secteur Created Successfully !');
        } else {
            return back()->withAlert('There was an error !');
        }
    }

    /**
     * Update Secteur In Referenciels.
     */
    public function updateSecteur(Request $request)
    {
        $secteur = [
            'desc_fr' => $request->secteur_desc_fr,
            'desc_en' => $request->secteur_desc_en,
            'applications' => $this->arrayToString($request->secteur_applications)
        ];
        $res = Secteur::whereId($request->secteur_id)->update($secteur);

        if ($res) {
            return back()->withSuccess('Secteur Updated Successfully !');
        } else {
            return back()->withAlert('There was an error !');
        }
    }


    /**
     * =====================
     *  Theme
     * ======================
     */

    /**
     * Store a newly created resource in storage.
     */
    public function storeTheme(Request $request)
    {
        if ($request->theme_code == 'newcode') {
            // Get last parent origin
            // $parentTheme = Theme::where('code', 'LIKE', '%T-_')->orWhere('code', 'LIKE', '%T-__')->limit(1)->orderBy('code', 'DESC')->first();

            $parentTheme = Theme::where('code', 'LIKE', '%T-__')->limit(1)->orderBy('code', 'DESC')->first();

            if (empty($parentTheme)) {
                $parentTheme = Theme::where('code', 'LIKE', '%T-_')->limit(1)->orderBy('code', 'DESC')->first();
            }

            // Create new Theme code
            $lastDigitOfCode = explode('-', $parentTheme->code)[1];
            $newCode = 'T-' . ((int)$lastDigitOfCode + 1);

            $code = $newCode;

        } else {
            // Get parent Theme if the code is same
            $parentTheme = Theme::where('code', '=', $request->theme_code)->first();

            if ($parentTheme) {
                // Get last child Theme for the selected parent Theme
                $childTheme = Theme::where('code', 'LIKE', '%' . $parentTheme->code . '-%')->limit(1)->orderBy('id', 'DESC')->first();

                if ($childTheme) {
                    // Child found
                    $lastDigitOfChild = explode('-', $childTheme->code)[2];
                    $newChildCode = $parentTheme->code . '-' . ((int)$lastDigitOfChild + 1);
                    // Create new Child Theme record
                    $code = $newChildCode;
                } else {
                    // Child not found [Create new child]
                    $newChildCode = $parentTheme->code . '-1';
                    $code = $newChildCode;
                }
            }
        }


        $theme = [
            'code' => $code,
            'desc_fr' => $request->theme_desc_fr,
            'desc_en' => $request->theme_desc_en,
            'applications' => $this->arrayToString($request->theme_applications)
        ];

        $res = Theme::create($theme);

        if ($res) {
            return back()->withSuccess('Theme Created Successfully !');
        } else {
            return back()->withAlert('There was an error !');
        }
    }

    /**
     * Update Theme In Referenciels.
     */
    public function updateTheme(Request $request)
    {
        $theme = [
            'desc_fr' => $request->theme_desc_fr,
            'desc_en' => $request->theme_desc_en,
            'applications' => $this->arrayToString($request->theme_applications)
        ];
        $res = Theme::whereId($request->theme_id)->update($theme);

        if ($res) {
            return back()->withSuccess('Theme Updated Successfully !');
        } else {
            return back()->withAlert('There was an error !');
        }
    }



    /**
     * =====================
     *  TypeInfo
     * ======================
     */

    /**
     * Store a newly created resource in storage.
     */
    public function storeTypeInfo(Request $request)
    {

        if ($request->typedeinfo_code == 'newcode') {
            // Get last parent origin
            // $parentTypeInfo = TypeInfo::where('code', 'LIKE', '%TI-_')->orWhere('code', 'LIKE', '%TI-__')->limit(1)->orderBy('code', 'DESC')->first();

            $parentTypeInfo = TypeInfo::where('code', 'LIKE', '%TI-__')->limit(1)->orderBy('code', 'DESC')->first();

            if (empty($parentTypeInfo)) {
                $parentTypeInfo = TypeInfo::where('code', 'LIKE', '%TI-_')->limit(1)->orderBy('code', 'DESC')->first();
            }

            // Create new TypeInfo code
            $lastDigitOfCode = explode('-', $parentTypeInfo->code)[1];
            $newCode = 'TI-' . ((int)$lastDigitOfCode + 1);

            $code = $newCode;

        } else {
            // Get parent TypeInfo if the code is same
            $parentTypeInfo = TypeInfo::where('code', '=', $request->typedeinfo_code)->first();

            if ($parentTypeInfo) {
                // Get last child TypeInfo for the selected parent TypeInfo
                $childTypeInfo = TypeInfo::where('code', 'LIKE', '%' . $parentTypeInfo->code . '-%')->limit(1)->orderBy('id', 'DESC')->first();

                if ($childTypeInfo) {
                    // Child found
                    $lastDigitOfChild = explode('-', $childTypeInfo->code)[2];
                    $newChildCode = $parentTypeInfo->code . '-' . ((int)$lastDigitOfChild + 1);
                    // Create new Child TypeInfo record
                    $code = $newChildCode;
                } else {
                    // Child not found [Create new child]
                    $newChildCode = $parentTypeInfo->code . '-1';
                    $code = $newChildCode;
                }
            }
        }


        $typedeinfo = [
            'code' => $code,
            'desc_fr' => $request->typedeinfo_desc_fr,
            'desc_en' => $request->typedeinfo_desc_en,
            'applications' => $this->arrayToString($request->typedeinfo_applications)
        ];

        $res = TypeInfo::create($typedeinfo);

        if ($res) {
            return back()->withSuccess('TypeInfo Created Successfully !');
        } else {
            return back()->withAlert('There was an error !');
        }
    }

    /**
     * Update TypeInfo In Referenciels.
     */
    public function updateTypeInfo(Request $request)
    {
        $typedeinfo = [
            'desc_fr' => $request->typedeinfo_desc_fr,
            'desc_en' => $request->typedeinfo_desc_en,
            'applications' => $this->arrayToString($request->typedeinfo_applications)
        ];
        $res = TypeInfo::whereId($request->typedeinfo_id)->update($typedeinfo);

        if ($res) {
            return back()->withSuccess('TypeInfo Updated Successfully !');
        } else {
            return back()->withAlert('There was an error !');
        }
    }


    /**
     * Update Langue In Referenciels.
     */
    public function updateLangue(Request $request)
    {
        $langue = [
            'description' => $request->langue_description,
        ];
        $res = Langue::whereId($request->langue_id)->update($langue);

        if ($res) {
            return back()->withSuccess('Langue Updated Successfully !');
        } else {
            return back()->withAlert('There was an error !');
        }
    }


    /**
     * Create Application In Referenciels.
     */
    public function createApplication(Request $request)
    {
        $application = [
            'name' => $request->application_name,
            'description' => $request->application_description,
            'solrcore_url' => $request->application_solrcore_url,
        ];
        $res = Application::create($application);

        if ($res) {
            return back()->withSuccess('Application Created Successfully !');
        } else {
            return back()->withAlert('There was an error !');
        }
    }


    /**
     * Create Application In Referenciels.
     */
    public function createAuthGroup(Request $request)
    {
        $lastAuthGroup = AuthGroup::latest()->first();
        $lastCodeInt = substr($lastAuthGroup->code, 2, strlen($lastAuthGroup->code) - 1);
        $newCode = 'AG' . ((int)$lastCodeInt + 1);
        $authGroup = [
            'code' => $newCode,
            'description' => $request->get('auth_group_desc'),
            'applications' => implode(', ', $request->get('auth_group_applications')),
        ];

        $res = AuthGroup::create($authGroup);

        if ($res) {
            return back()->withSuccess('Auth group Created Successfully !');
        } else {
            return back()->withAlert('There was an error !');
        }
    }

    /**
     * Create Application In Referenciels.
     */
    public function updateAuthGroup(Request $request)
    {
        $authGroup = AuthGroup::where('id', $request->get('auth_group_id'))->first();

        $res = $authGroup->update([
            'description' => $request->get('auth_group_desc'),
            'applications' => $request->has('auth_group_applications') && $request->get('auth_group_applications') != null ? implode(', ', $request->get('auth_group_applications')) : null,
        ]);

        if ($res) {
            return back()->withSuccess('Auth group updated Successfully !');
        } else {
            return back()->withAlert('There was an error !');
        }
    }


    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request)
    {
        $id = $request->id;
        if ($request->target == 'fonction') {
            $res = Fonction::whereId($id)->first()->delete();
            if ($res) {
                return response()->json(['message' => 'Fonction deleted Successfully !'], 200);
            } else {
                return response()->json(['message' => 'There was an error !'], 500);
            }
        }

        if ($request->target == 'secteur') {
            $res = Secteur::whereId($id)->first()->delete();
            if ($res) {
                return response()->json(['message' => 'Secteur deleted Successfully !'], 200);
            } else {
                return response()->json(['message' => 'There was an error !'], 500);
            }
        }

        if ($request->target == 'theme') {
            $res = Theme::whereId($id)->first()->delete();
            if ($res) {
                return response()->json(['message' => 'Theme deleted Successfully !'], 200);
            } else {
                return response()->json(['message' => 'There was an error !'], 500);
            }
        }

        if ($request->target == 'typedeinfo') {
            $res = TypeInfo::whereId($id)->first()->delete();
            if ($res) {
                return response()->json(['message' => 'Type De Info deleted Successfully !'], 200);
            } else {
                return response()->json(['message' => 'There was an error !'], 500);
            }
        }

        if ($request->target == 'langue') {
            $res = Langue::whereId($id)->first()->delete();
            if ($res) {
                return response()->json(['message' => 'Langue deleted Successfully !'], 200);
            } else {
                return response()->json(['message' => 'There was an error !'], 500);
            }
        }

        if ($request->target == 'application') {
            $res = Application::whereId($id)->first()->delete();
            if ($res) {
                return response()->json(['message' => 'Application deleted Successfully !'], 200);
            } else {
                return response()->json(['message' => 'There was an error !'], 500);
            }
        }

        if ($request->target == 'authgroup') {
            $res = AuthGroup::whereId($id)->first()->delete();
            if ($res) {
                return response()->json(['message' => 'Authgroup deleted Successfully !'], 200);
            } else {
                return response()->json(['message' => 'There was an error !'], 500);
            }
        }

    }


    private function arrayToString($array)
    {
        $string = '';
        if (is_array($array)) {
            if (count($array) == 1) {
                $string = $array[0];
            } elseif (count($array) > 1) {
                foreach ($array as $index => $str) {
                    if ($index == count($array) - 1) {
                        $string .= $str;
                    } else {
                        $string .= $str . ',';
                    }
                }
            }
        } elseif (is_string($array)) {
            $string = $array;
        }
        return $string;
    }

}
