<?php

namespace App\Http\Controllers\Admin;

use App\Job;
use App\Origin;
use App\Collecte;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class CrawlerController extends Controller
{

    // Index
    public function index()
    {
        //
    }


    // Create / Add 
    public function create()
    {
        //
    }


    // Store / Save
    public function store(Request $request)
    {
        //
    }

    // Show / View single
    public function show($id)
    {
        //
    }


    // Edit
    public function edit($descFr)
    {

        $origin = Origin::where('code', 'like', 'O-__')->where('desc_fr', 'like', '%'.$descFr.'%')->pluck('origin_url');

        $desc_fr = $descFr;

        if (count($origin) > 0) {
            $collecte = Collecte::where('urls', 'like', '%'.$origin[0].'%')->get();
        }  else {
            $collecte = [];
        }

        return view('admin.command.crawler-edit', compact(['collecte', 'desc_fr']));
    }


    // Update
    public function update(Request $request)
    {


        $collecte = Collecte::whereId($request->id)->first();

        $collecte->nom = $request->nom;
        // $collecte->status = $request->status;

        $urls = $this->makeArray($request->urls);

        $collecte->urls = $urls;
        $res = $collecte->save();
        if ($res) {
            return back()->withSuccess('Crawler updated successfully !');
        } else {
            return back()->withAlert('There was an error !');
        }
    }


    // Delete / Destroy
    public function destroy($id)
    {
        //
    }


    // Result
    public function crawlerResult($id)
    {
        $results = Collecte::whereId($id)->first()->result;
        $data['results'] = json_decode($results);
        $data['id'] = $id;
        // return $data;
        return view('admin.command.crawler-result', $data);
    }


    // Result Edit
    public function crawlerResultEdit($id, $index)
    {
        $results = Collecte::whereId($id)->first()->result;
        $results = json_decode($results);
        $data['result'] = $results[$index];
        $data['id'] = $id;
        $data['index'] = $index;
        return view('admin.command.crawler-result-edit', $data);
    }


    // Result Update / Save
    public function crawlerResultUpdate(Request $request)
    {
        $results = Collecte::whereId($request->id)->first();
        $result = json_decode($results->result);
        
        $result[$request->index]->url = $request->url;
        $result[$request->index]->Lang = $request->lang;
        $result[$request->index]->Type = $request->type;
        $result[$request->index]->counter = $request->counter;
        $result[$request->index]->a_parser = $request->parser;
        
        $themes = $this->makeArray($request->themes);
        $secteurs = $this->makeArray($request->secteurs);
        $fonctions = $this->makeArray($request->fonctions);
        $types_infos = $this->makeArray($request->types_infos);
        
        $result[$request->index]->themes = $themes;
        $result[$request->index]->secteurs = $secteurs;
        $result[$request->index]->fonctions = $fonctions;
        $result[$request->index]->types_infos = $types_infos;

        $results->result = json_encode($result);
        $res = $results->save();

        if ($res) {
            return redirect()->route('admin.command.crawler-result',$request->id)->withSuccess('Result Updated Successfully !');
        } else {
            return back()->withAlert('There was an error !');
        }

    }


    // Result Delete
    public function crawlerResultDelete($id, $index)
    {
        $results = Collecte::whereId($id)->first();
        $result = json_decode($results->result);
        
        array_splice($result, $index, 1);
        
        $results->result = json_encode($result);
        $res = $results->save();
        if ($res) {
            return redirect()->route('admin.command.crawler-result',$id)->withSuccess('Result Deleted Successfully !');
        } else {
            return back()->withAlert('There was an error !');
        }
    
    }


    public function crawlerStatus()
    {
        $jobs = Job::orderBy('created_at', 'desc')->paginate(10);
        return view('admin.command.crawler-status', ['jobs' => $jobs]);
    }



    // MakeArray
    public function makeArray($string)
    {
        // Check if there is a comma at the end
        $checkComma = substr(trim($string), strlen($string) - 1, 1);
        if ($checkComma != ',') {
            // if not comma then just continue
            $value = trim($string);
        } else {
            // If comma then just remove it
            $value = trim(substr($string, 0, -1));
        }

        // convert $value in array
        $value = explode(',', $value);

        // Spit each array index as $val
        foreach ($value as $val) {
            // Check if $val available in $value
            if(in_array($val, $value)){
                // if yes then just remove it
                array_shift($value);
            }

            // Check If $val is not empty
            if ($val != null || $val != '') {
                // Just add $val in $value array again after trimming
                $value[] = trim($val);
            } else {
                // if $val is empty then just go as it is
                $value = $value;
            }
        }

        // Return $value Array
        return $value;
    }

}


