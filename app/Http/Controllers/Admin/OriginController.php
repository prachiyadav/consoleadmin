<?php

namespace App\Http\Controllers\Admin;

use App\Filter;
use App\Origin;
use Carbon\Carbon;
use App\Xml as XML;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class OriginController extends Controller
{
    public function __construct()
    {
        DB::statement( 'SET GLOBAL FOREIGN_KEY_CHECKS=0' );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.parameter.origin.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.parameter.origin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'origin_url' => ['required']
        ]);

        $url = substr($request->get('origin_url'), -1) != '/'
            ? $request->get('origin_url') . '/'
            : $request->get('origin_url');

        if (Origin::where('origin_url', $url)->first()){
            return response()->json(['message' => 'Origine url existe'], 422);
        }

        // Init Origin Object
        $origin = new Origin;

        if($request->code == 'newcode') {
            // Create new origin record

            // Get last parent origin
            $parentOrigin = Origin::where('code', 'LIKE', '%O-__')->limit(1)->orderBy('code', 'DESC')->first();

            // Create new Origin code
            $lastDigitOfCode = explode('-', $parentOrigin->code)[1];
            $newCode = 'O-' . ((int) $lastDigitOfCode + 1);

            // Convert Array values to string values
            $fonction = $this->arrayToString($request->fonction);
            $secteurs = $this->arrayToString($request->secteurs);
            $themes = $this->arrayToString($request->themes);
            $typededoc = $this->arrayToString($request->typededoc);
            $type_info = $this->arrayToString($request->type_info);

            // Prepare data for new origin
            $origin->code = $newCode;
            $origin->desc_fr = $request->descfr;
            $origin->origin_url = substr($request->origin_url, -1) != '/' ? $request->origin_url . '/' : $request->origin_url;
            $origin->langue = $request->langue;
            $origin->a_indexer = $request->a_indexer ?? 0;
            $origin->profondeur_crawl = $request->crawlingdepth;
            $origin->limite_nb_doc = $request->limitdedoc;
            $origin->desc_en = $request->descen;
            $origin->parent = 'NULL';
            $origin->application_name = $this->arrayToString($request->application);
            $origin->authgroups = $this->arrayToString($request->authgroups);
            $origin->type_de_doc = $typededoc;
            $origin->fonction_code  = $fonction;
            $origin->secteurs_code  = $secteurs;
            $origin->themes_code  = $themes;
            $origin->type_infos_code  = $type_info;
            $res = $origin->save();

            return $origin;

        } else {
            // Get parent origin if the code is same
            $parentOrigin = Origin::where('code', '=', $request->code)->first();

            if($parentOrigin){
                // Get last child origin for the selected parent origin
                $childOrigin = Origin::where('code', 'LIKE', '%'.$parentOrigin->code.'-%')->limit(1)->orderBy('id', 'DESC')->first();

                if($childOrigin) {
                    // Child found
                    $lastDigitOfChild = explode('-', $childOrigin->code)[2];
                    $newChildCode = $parentOrigin->code . '-' . ((int) $lastDigitOfChild + 1);
                    // Create new Child origin record
                    
                    // Convert Array values to string values
                    $fonction = $this->arrayToString($request->fonction);
                    $secteurs = $this->arrayToString($request->secteurs);
                    $themes = $this->arrayToString($request->themes);
                    $typededoc = $this->arrayToString($request->typededoc);
                    $type_info = $this->arrayToString($request->type_info);

                    // Prepare data for new origin
                    $origin->code = $newChildCode;
                    $origin->desc_fr = $request->descfr;
                    $origin->origin_url = $request->origin_url;
                    $origin->langue = $request->langue;
                    $origin->a_indexer = $request->a_indexer ?? 0;
                    $origin->profondeur_crawl = $request->crawlingdepth;
                    $origin->limite_nb_doc = $request->limitdedoc;
                    $origin->desc_en = $request->descen;
                    $origin->parent = $parentOrigin->code;
                    $origin->application_name = $this->arrayToString($request->application);
                    $origin->authgroups = $this->arrayToString($request->authgroups);
                    $origin->type_de_doc = $typededoc;
                    $origin->fonction_code  = $fonction;
                    $origin->secteurs_code  = $secteurs;
                    $origin->themes_code  = $themes;
                    $origin->type_infos_code  = $type_info;
                    
                    $res = $origin->save();

                    return $origin;
                    
                    
                } else {
                    // Child not found [Create new child]
                    $newChildCode = $parentOrigin->code . '-1';

                    // Create new Child origin record
                    
                    // Convert Array values to string values
                    $fonction = $this->arrayToString($request->fonction);
                    $secteurs = $this->arrayToString($request->secteurs);
                    $themes = $this->arrayToString($request->themes);
                    $typededoc = $this->arrayToString($request->typededoc);
                    $type_info = $this->arrayToString($request->type_info);

                    // Prepare and save data for new origin
                    $origin->code = $newChildCode;
                    $origin->desc_fr = $request->descfr;
                    $origin->origin_url = $request->origin_url;
                    $origin->langue = $request->langue;
                    $origin->a_indexer = $request->a_indexer ?? 0;
                    $origin->profondeur_crawl = $request->crawlingdepth;
                    $origin->limite_nb_doc = $request->limitdedoc;
                    $origin->desc_en = $request->descen;
                    $origin->parent = $parentOrigin->code;
                    $origin->application_name = $this->arrayToString($request->application);
                    $origin->authgroups = $this->arrayToString($request->authgroups);
                    $origin->type_de_doc = $typededoc;
                    $origin->fonction_code  = $fonction;
                    $origin->secteurs_code  = $secteurs;
                    $origin->themes_code  = $themes;
                    $origin->type_infos_code  = $type_info;
                    $res = $origin->save();

                    return $origin;
                }
            }
        }


    }

    /**
     * Store a newly created filter in db.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function filterStore(Request $request)
    {
        $filter = new Filter;
        $filter->origin_code = $request->origin_code;
        $filter->URL_Pas_Crawl = $request->URL_Pas_Crawl;
        $filter->save();
        return $filter;
    }

    /**
     * Store a newly created XML in db.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function xmlStore(Request $request)
    {
        $xml = new XML;
        $xml->origine = substr($request->origin, -1) != '/' ? $request->origin . '/' : $request->origin;
        $xml->droit_acces = $request->droit_access;
        $xml->encodage = $request->encodage;
        $xml->origine_applicative = $request->origine_applicative;
        $xml->origine_code = $request->origine_code;
        $xml->serveur = $request->serveur;
        $xml->url = $request->url;
        $xml->id_document = $request->id_document;
        $xml->langue = $request->xmllangue;
        $xml->titre  = $request->titre;
        $xml->km  = $request->km;
        $xml->texte  = $request->texte;
        $xml->longDoc  = 'x';
        $xml->shortDoc  = 'x';
        $xml->ID_PJ_HTML  = $request->id_pj_html;
        $xml->PJ_audience_HTML  = $request->pj_audience_html;
        $xml->PJ_text_HTML  = $request->pj_text_html;
        $xml->ID_PJ_PDF  = $request->id_pj_pdf;
        $xml->PJ_audience_PDF  = $request->pj_audience_pdf;
        $xml->PJ_text_PDF  = $request->pj_text_pdf;
        $xml->PJ_path  = $request->pj_path;
        $xml->deleted  = false;
        $xml->date  = date('Y-m-d');
        $xml->long_doc  = $request->long_doc;
        $xml->short_doc  = $request->short_doc;

        $xml->save();
        
        return $xml;
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($code)
    {
        $data['editOrigin'] = Origin::whereCode($code)->first();
        if ($data['editOrigin']) {
            return view('admin.parameter.origin.edit', $data);
        }
        return redirect()->route('admin.parameter.origin-index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // return $request->all();
        $origin = Origin::whereId($request->origin_id)->first();

        // Convert Array values to string values
        $fonction = $this->arrayToString($request->fonction);
        $secteurs = $this->arrayToString($request->secteurs);
        $themes = $this->arrayToString($request->themes);
        $typededoc = $this->arrayToString($request->typededoc);
        $type_info = $this->arrayToString($request->type_info);

        // Prepare data for new origin
        $origin->code = $request->code;
        $origin->desc_fr = $request->descfr;
        $origin->origin_url = substr($request->origin_url, -1) != '/' ? $request->origin_url . '/' : $request->origin_url;
        $origin->langue = $request->langue;
        $origin->a_indexer = $request->a_indexer ?? 0;
        $origin->profondeur_crawl = $request->crawlingdepth;
        $origin->limite_nb_doc = $request->limitdedoc;
        $origin->desc_en = $request->descen;
        $origin->parent = $request->origin_parent;
        $origin->application_name = $this->arrayToString($request->application);
        $origin->authgroups = $this->arrayToString($request->authgroups);
        $origin->type_de_doc = $typededoc; // Array to string
        $origin->fonction_code  = $fonction; // Array to string
        $origin->secteurs_code  = $secteurs; // Array to string
        $origin->themes_code  = $themes; // Array to string
        $origin->type_infos_code  = $type_info; // Array to string

        // return $origin;

        $origin->save();

        return $origin;
    }


    /**
     * Store a newly created filter in db.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function filterUpdate(Request $request)
    {

        $filter = Filter::whereId($request->filter_id)->whereOriginCode($request->origin_code)->first();
        
        if (!$filter) {
            // No old record
            $newFilter = new Filter;
            $newFilter->origin_code = $request->origin_code;
            $newFilter->URL_Pas_Crawl = $request->URL_Pas_Crawl;
            $newFilter->save();
            return $newFilter;
        }
        
        $filter->origin_code = $request->origin_code;
        $filter->URL_Pas_Crawl = $request->URL_Pas_Crawl;
        $filter->save();
        return $filter;
    }


    /**
     * Store a newly created XML in db.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function xmlUpdate(Request $request)
    {

        $xml = XML::whereId($request->xml_id)->first();
        
        if (!$xml) {
            // No old record
            // *********************
            // Create new xml record
            // *********************
            $newXml = new XML;
            $newXml->origine = $request->origin;
            $newXml->droit_acces = $request->droit_access;
            $newXml->encodage = $request->encodage;
            $newXml->origine_applicative = $request->origine_applicative;
            $xml->origine_code = $request->origine_code;
            $newXml->serveur = $request->serveur;
            $newXml->url = $request->url;
            $newXml->id_document = $request->id_document;
            $newXml->langue = $request->xmllangue;
            $newXml->titre  = $request->titre;
            $newXml->km  = $request->km;
            $newXml->texte  = $request->texte;
            $newXml->longDoc  = 'x';
            $newXml->shortDoc  = 'x';
            $newXml->ID_PJ_HTML  = $request->id_pj_html;
            $newXml->PJ_audience_HTML  = $request->pj_audience_html;
            $newXml->PJ_text_HTML  = $request->pj_text_html;
            $newXml->ID_PJ_PDF  = $request->id_pj_pdf;
            $newXml->PJ_audience_PDF  = $request->pj_audience_pdf;
            $newXml->PJ_text_PDF  = $request->pj_text_pdf;
            $newXml->PJ_path  = $request->pj_path;
            $newXml->deleted  = false;
            $newXml->date  = date('Y-m-d');
            $newXml->long_doc  = $request->long_doc;
            $newXml->short_doc  = $request->short_doc;
            $newXml->save();
            return $newXml;
        }

        // Update xml record
        $xml->origine = $request->origin;
        $xml->droit_acces = $request->droit_access;
        $xml->encodage = $request->encodage;
        $xml->origine_applicative = $request->origine_applicative;
        $xml->serveur = $request->serveur;
        $xml->url = $request->url;
        $xml->id_document = $request->id_document;
        $xml->langue = $request->xmllangue;
        $xml->titre  = $request->titre;
        $xml->km  = $request->km;
        $xml->texte  = $request->texte;
        $xml->longDoc  = 'x';
        $xml->shortDoc  = 'x';
        $xml->ID_PJ_HTML  = $request->id_pj_html;
        $xml->PJ_audience_HTML  = $request->pj_audience_html;
        $xml->PJ_text_HTML  = $request->pj_text_html;
        $xml->ID_PJ_PDF  = $request->id_pj_pdf;
        $xml->PJ_audience_PDF  = $request->pj_audience_pdf;
        $xml->PJ_text_PDF  = $request->pj_text_pdf;
        $xml->PJ_path  = $request->pj_path;
        $xml->deleted  = false;
        $xml->date  = date('Y-m-d');
        $xml->long_doc  = $request->long_doc;
        $xml->short_doc  = $request->short_doc;

        $xml->save();
        
        return $xml;
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $origin = Origin::find($request->id);

        if (strlen($origin->code) <= 4) {
            $childOrigins = Origin::where('code', 'LIKE', '%'.$origin->code.'-%')->get();
            foreach ($childOrigins as $index => $childOrigin) {
                $childOrigin->delete();
            }
        }

        if ($origin) {
            $origin->delete();
            return $origin;
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyFilter(Request $request)
    {
        // $filter = Filter::find($request->id);

        $filters = Filter::where('origin_code', 'LIKE', '%'.$request->origin_code.'%')->get();
        if ($filters) {
            foreach ($filters as $index => $filter) {
                $filter->delete();
            }
            // $filter->delete();
            return $filters;
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyXml(Request $request)
    {
        // $xml = Xml::find($request->id);
        $xmls = Xml::where('origine_applicative', 'LIKE', '%'.$request->origin_code.'%')->get();
        if ($xmls) {
            foreach ($xmls as $index => $xml) {
                $xml->delete();
            }
            // $xml->delete();
            return $xmls;
        }
    }

    
    /**
     * Convert array to string
     *
     * @param  array  $array
     * @return string $string
     */
    private function arrayToString($array)
    {
        $string = '';
        if (is_array($array)) {
            if (count($array) == 1) {
                $string = $array[0];
            } elseif(count($array) > 1) {
                foreach($array as $index => $str){
                    if ($index == count($array) - 1) {
                        $string .= $str;
                    } else {
                        $string .= $str.',';
                    }
                }
            }
        } elseif (is_string($array)) {
            $string = $array;
        }
        return $string;
    }
}
