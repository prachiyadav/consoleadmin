<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Collecte;

class CollectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.parameter.collecte.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $urls = '';
        foreach ($request->origines as $index => $url) {
            if ($index != (count($request->origines) - 1)) {
                $urls .= '"'.$url.'",';
            } else {
                $urls .= '"'.$url.'"';
            }
        }

        if (!$request->has('update') && !$request->update) {
            $collect = new Collecte();
            $checkOldNom = Collecte::whereNom('C-'.$request->name)->first();
            if ($checkOldNom) {
                return response()->json(['status' => 'alert', 'message' => 'Selected nom already exist !']);
            }
            $data = [
                'nom' => 'C-'.$request->name,
                'urls' => "[$urls]"
            ];
            $collect->create($data);
            return response()->json(['status' => 'success', 'message' => 'Collect Created Successfully !']);
        } else {
            $checkOldNom = Collecte::whereNom('C-'.$request->name)->first();
            $data = [
                'nom' => 'C-'.$request->name,
                'urls' => "[$urls]",
                'result' => null
            ];
            $checkOldNom->update($data);
            return response()->json(['status' => 'success', 'message' => 'Collect Updated Successfully !']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $collecte = Collecte::whereId($id)->first();
        return view('admin.parameter.collecte.edit', compact('collecte'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $collecteNomCheck = Collecte::whereNom($request->name)->where('id', '!=', $request->id)->first();

        if ($collecteNomCheck) {
            return back()->with('alert', 'This nom already exist in the database please choose different nom !')->withInput();
        }
        
        $collecte = Collecte::whereId($request->id)->first();
        $data = [
            'nom' => $request->name,
            'urls' => json_decode($request->urls),
            'result' => $request->result ? json_decode($request->result) : null
        ];


        $collecte->update($data);
        
        return back()->with('success', 'Collect Updated Successfully !');;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
