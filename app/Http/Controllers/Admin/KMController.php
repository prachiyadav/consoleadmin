<?php

namespace App\Http\Controllers\Admin;

use App\KeyMetadata;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\KmExport;
use App\Imports\KmImport;

class KMController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['keyMetas'] = KeyMetadata::get();
        return view('admin.parameter.km.index', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $res = KeyMetaData::create([
            'key_metadata' => $request->key_metadata,
            'fonction' => $request->fonction,
            'secteur' => $request->secteur,
            'theme' => $request->theme,
            'type_de_info' => $request->typedeinfo,
        ]);

        if ($res) {
            return back()->withSuccess('Key meta data created successfully !');
        } else {
            return back()->withAlert('There was an error !');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $res = KeyMetaData::whereId($request->id)->update([
            'key_metadata' => $request->key_metadata,
            'fonction' => $request->fonction,
            'secteur' => $request->secteur,
            'theme' => $request->theme,
            'type_de_info' => $request->typedeinfo,
        ]);

        if ($res) {
            return back()->withSuccess('Key meta data updated successfully !');
        } else {
            return back()->withAlert('There was an error !');
        }
    }

    /**
     * Export bulk data
     */
    public function export() 
    {
        return Excel::download(new KmExport, 'KeyMetadata.csv');
    }
   
    /**
     * Import bulk data
     */
    public function import() 
    {
        Excel::import(new KmImport, request()->file('csvfile'));
           
        return redirect()->back();
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $res = KeyMetaData::whereId($request->id)->delete();
        if ($res) {
            return response()->json(['message' => 'Key meta data deleted successfully !']);
        } else {
            return response()->json(['message' => 'There was an error !']);
        }
    }


}
