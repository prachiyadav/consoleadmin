<?php

namespace App\Http\Controllers\Admin;

use App\Job;
use App\JobParser;
use App\Origin;
use App\Collecte;
use App\Indexationengine;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ParserController extends Controller
{
    public function index()
    {
        //
    }

    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }


    public function list($code)
    {
        // $id = explode(',', $id);

        // $parsers = [];
        
        // foreach ($id as $key => $id) {
        //     array_push($parsers, Indexationengine::whereId($id)->first());
        // }

        // return view('admin.command.parser-edit', compact('parsers'));


        // $origin = Origin::where('code', 'like', 'O-__')->where('origin_url', 'like', '%'.$code.'%')->pluck('origin_url');

        $parsers = Indexationengine::where('origine', 'like', '%'.$code.'%')->get();

        $origin = Origin::where('code', $code)->first();
        $originName = $origin->desc_fr;
        $code = $code;
        // return view('admin.command.parser-edit', compact('parsers'));
        return view('admin.command.parsers-lists', compact(['parsers','originName', 'code']));

    }

    public function edit($id, $code)
    {
        $data['parser'] = Indexationengine::whereId($id)->first();
        $data['code'] = $code;
        return view('admin.command.parser-edit', $data);

    } 


    public function update(Request $request)
    {
        $parser = Indexationengine::whereId($request->id)->first();

        $parser->title = $request->title;
        $parser->fonction = $request->fonction;
        $parser->secteur = $request->secteur;
        $parser->Typedinfo = $request->typedinfo;
        $parser->theme = $request->theme;
        $parser->origine = $request->origine;
        $parser->dynamic = $request->dynamic;
        $parser->language = $request->language;
        $parser->typededoc = $request->typededoc;
        $parser->status = $request->status;
        $parser->sku = $request->sku;
        // $parser->publicated = $request->publicated ? 1 : 0;


        $res = $parser->save();
        if ($res) {
            return back()->withSuccess('Parser updated successfully !');
        } else {
            return back()->withAlert('There was an error !');
        }
    }


    public function destroy($id, $code)
    {
        $parser = Indexationengine::whereId($id)->first();

        $res = $parser->delete();
        if ($res) {
            return response()->json('Parser deleted successfully !', 200);
        } else {
            return response()->json('There was an error !', 503);
        }
    }



    public function checkNomsInCollecte(Request $request)
    {
        
        $collecteIds = explode(',',$request->collecteIds);

        $arr = [];
        $arrNot = [];
        foreach ($collecteIds as $index => $collecteid) {
            $data = Collecte::whereId($collecteid)->first();
            if($this->checkNom($data)){
                array_push($arr, ['id' => $data->id, 'nom' => $data->nom]);
            } else {
                array_push($arrNot, $data->nom);
            }
        }

        return response()->json([
            'passed' => $arr,
            'failed' => $arrNot,
        ]);

        // return $request->all();
    }

    private function checkNom($data){
        // $nomInJob = Job::where('payload', 'LIKE', '%'.$data->nom.'%')->latest()->first();
        // if ($nomInJob && $nomInJob->state == 'success') {
        //     return true;
        // } else {
        //     return false;
        // }

        $haveResult = Collecte::whereId($data->id)->first();
        if ($haveResult && ($haveResult->result != null || !empty(json_decode($haveResult->result)))) {
            return true;
        } else {
            return false;
        }

    }

    
    public function parserStatus()
    {
        $jobs = JobParser::orderBy('created_at', 'desc')->paginate(10);
        return view('admin.command.parser-status', ['jobs' => $jobs]);
    }
}