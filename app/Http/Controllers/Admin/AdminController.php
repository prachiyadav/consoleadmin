<?php

namespace App\Http\Controllers\Admin;

use App\LogSearch;
use App\Origin;
use App\Indexationengine;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Http;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    // Index Method
    public function index()
    {
        return view('admin.command.index');
    }

    // publication Method
    public function publication()
    {
        return view('admin.publication.index');
    }

    public function thesaurus()
    {
        return view('admin.parameter.thesaurus-index');
    }

    public function publicationUpdate(Request $request)
    {
        $origin = Indexationengine::whereId($request->id)->first();

        $origin->sku = $request->sku;
        $origin->origine = $request->origine;
        $origin->publicated = $request->publicated ? 1 : 0;

        $res = $origin->save();
        if ($res) {
            return back()->withSuccess('Publication updated successfully !');
        } else {
            return back()->withAlert('There was an error !');
        }
    }

    public function publicationList($code)
    {
        $publications = Indexationengine::where('origine', 'like', '%' . $code . '%')->get();
        $origin = Origin::where('code', $code)->first();
        $originName = $origin->desc_fr;
        return view('admin.command.publications-lists', compact(['publications', 'originName']));
    }

    public function publicationPublish($id)
    {
        $publication = Indexationengine::whereId($id)->first();
        $publication->publicated = 1;
        $res = $publication->save();
        if ($res) {
            return response()->json('Publication updated successfully !', 200);
        } else {
            return response()->json('There was an error !', 503);
        }
    }

    public function publicationPublishUnPublish(Request $request)
    {
        $publication = Indexationengine::whereId($request->id)->first();
        $publication->publicated = $publication->publicated ? 0 : 1;
        $res = $publication->save();

        // return $res;

        if ($res) {
            return response()->json('Publication updated successfully !', 200);
        } else {
            return response()->json('There was an error !', 503);
        }
    }

    public function publicationUnPublish($id)
    {
        $publication = Indexationengine::whereId($id)->first();
        $publication->publicated = 0;
        $res = $publication->save();
        if ($res) {
            return response()->json('Publication updated successfully !', 200);
        } else {
            return response()->json('There was an error !', 503);
        }
    }

    public function publicationDelete($id)
    {
        $res = Indexationengine::whereId($id)->delete();
        if ($res) {
            return back()->withSuccess('Publication deleted successfully !');
        } else {
            return back()->withAlert('There was an error !');
        }
    }


    public function importLog(Request $request)
    {
        $filePath = config('app.json_search_log_path');
        $file = File::get($filePath);
        $file = json_decode($file);

        $lastEntry = LogSearch::orderBy('date', 'DESC')->limit(1)->first();

        if ($lastEntry) {
            $filtered = array_filter($file->searchs, function ($val) use ($lastEntry) {
                return strtotime($val->date) > strtotime($lastEntry->date);
            });
        } else {
            $filtered = $file->searchs;
        }

        foreach ($filtered as $search) {
            LogSearch::create([
                'keyword' => $search->keyword,
                'origins' => implode(',', $search->origins),
                'type_search' => $search->type_search,
                'username' => $search->username,
                'date' => $search->date
            ]);
        }

        return response()->json(['message' => 'Log Imported Successfully!'], 200);
    }

    public function biEngine(Request $request)
    {
        $solr_core_name = $request->get('application');
        $solr_core_url = $request->get('url');

        Http::get(sprintf('%s/application=%s&url=%s', config('app.update_solr_url'), $solr_core_name, $solr_core_url));
    }

}
