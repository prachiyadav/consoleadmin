<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AccessRightController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    // Index Method
    public function index()
    {
        return view('admin.access-right.index',
            [
                "users" => \App\User::get(),
                "authgroups" => \App\AuthGroup::get()->pluck('code'),
                "applications" => \App\Application::get()->pluck('name')
            ]);
    }

    public function store(Request $request)
    {

    }

    public function update(Request $request)
    {
        dd($request);
    }

    public function destroy(Request $request)
    {

    }

}

