<?php

namespace App\Exports;

use App\KeyMetaData;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class KmExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return KeyMetaData::all();
    }
    
    public function headings(): array
    {
        return [
            '#',
            'keymetadata',
            'fonction',
            'secteur',
            'theme',
            'typedeinfo',
            'createdat',
            'updatedat'
        ];
    }

}
