<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Xml extends Model
{
    protected $table = 'xmls';
    protected $guarded = ['id'];
}
