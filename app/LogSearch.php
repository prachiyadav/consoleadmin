<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogSearch extends Model
{
    protected $table = 'log_search';
    protected $guarded = ['id'];
}
