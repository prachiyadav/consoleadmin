<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeInfo extends Model
{
    protected $table = 'type_infos';
    protected $guarded = [];
}
