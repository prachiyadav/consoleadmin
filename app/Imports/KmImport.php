<?php

namespace App\Imports;

use App\KeyMetaData;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class KmImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new KeyMetaData([
            'key_metadata' => $row['keymetadata'],
            'fonction' => $row['fonction'],
            'secteur' => $row['secteur'],
            'theme' => $row['theme'],
            'type_de_info' => $row['typedeinfo']
        ]);
    }
}
