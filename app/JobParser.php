<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobParser extends Model
{
    protected $table = 'jobsParser';
    protected $guarded = [];
}
