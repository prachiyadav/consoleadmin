<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KeyMetadata extends Model
{
    protected $table = 'table_de_km';
    protected $guarded = [];
}
