<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Indexationengine extends Model
{
    protected $table = 'indexationengine';
    protected $guarded = [];
}
