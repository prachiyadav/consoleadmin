@extends('layouts.admin.master')

@section('title', 'Edit Referentiels')

@section('scoped_css')
    <style>
        th, *{
            text-transform: none !important;
        }
    </style>
@endsection

@section('content')
    <div class="app-content">
        <div class="side-app">

            <!--Page header-->
            <div class="page-header">
                <div class="page-leftheader">
                    <h4 class="page-title">Edit Referentiels</h4>
                    <ol class="breadcrumb pl-0">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item">Paramètres</li>
                        <li class="breadcrumb-item active" aria-current="page">Edit Referentiels</li>
                    </ol>
                </div>
            </div>
            <!--End Page header-->


            <!--Row-->
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <h4 class="p-0 m-0 text-muted">Edit Référentiels</h4>
                    <hr class="my-3">
                    {{-- Tab 1 --}}
                    <div class="card">
                        <div class="card-header bg-light">
                            <h3 class="card-title mr-auto">Edition.</h3>
                            {{-- <button class="btn btn-info"><i class="fa fa-plus"></i> Créer</button> --}}
                        </div>
                        <div class="card-body">
                            
                            <form action="">
                                @csrf
                                <div class="row">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Code</label>
                                            <input type="text" class="form-control" value="jgdsh">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Description FR</label>
                                            <input type="text" class="form-control" value="jgdsh">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Description EN</label>
                                            <input type="text" class="form-control" value="jgdsh">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Profondeur Du Crawl</label>
                                            <input type="text" class="form-control" value="jgdsh">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Description EN</label>
                                            <textarea class="form-control" name="" rows="5">fhdvfhjbgdfghj</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <button class="btn btn-info">Enregistrement</button>
                                        </div>
                                    </div>

                                </div>
                            </form>

                        </div>
                    </div>


                </div>

            </div>
            <!--End row-->


        </div>
    </div><!-- end app-content-->
@endsection


@section('scoped_js')
    <script>
        $('.datatable').each(function () {
            $(this).DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
            });
        });
    </script>
@endsection

