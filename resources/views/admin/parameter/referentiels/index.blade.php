@extends('layouts.admin.master')

@section('title', 'Referentiels')

@section('scoped_css')
    <style>
        th, *{
            text-transform: none !important;
        }
        tr td button.btn {
            background: transparent;
            outline: none !important;
            box-shadow: none !important
        }
        .select2-container {
            width: 100%;
            display: block;
        }
    </style>
@endsection

@section('content')
    <div class="app-content">
        <div class="side-app">

            <!--Page header-->
            <div class="page-header">
                <div class="page-leftheader">
                    <h4 class="page-title">Referentiels</h4>
                    <ol class="breadcrumb pl-0">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item">Paramètres</li>
                        <li class="breadcrumb-item active" aria-current="page">Referentiels</li>
                    </ol>
                </div>
            </div>
            <!--End Page header-->


            <!--Row-->
            <div class="row">
                <div class="col-md-12">
                    <h4 class="p-0 m-0 text-muted">Référentiels clients</h4>
                    <hr class="my-3">
                    {{-- Tab 1 --}}
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title mr-auto">Ci-dessous la liste des <span id="tabName">référentiels</span> disponibles.</h3>
                            {{-- <button class="btn btn-info"><i class="fa fa-plus"></i> Créer</button> --}}
                        </div>
                        <div class="card-body">
                        
                            <div class="tab-content">
                                <div class="tab-pane active " id="fr">
                                    <div class="mb-5">
                                        <div class="tabs-menu1">
                                            <!-- Tabs -->
                                            <ul class="nav panel-tabs">
                                                <li><a href="#fonctions" class="tab-btn active" class="active" data-toggle="tab">Fonctions</a></li>
                                                <li><a href="#secteurs" class="tab-btn" data-toggle="tab">Secteur</a></li>
                                                <li><a href="#themes" class="tab-btn" data-toggle="tab">Thème</a></li>
                                                <li><a href="#typedinfo" class="tab-btn" data-toggle="tab">Type d'Info</a></li>
                                                <li><a href="#langue" class="tab-btn" data-toggle="tab">Langue</a></li>
                                                <li><a href="#application" class="tab-btn" data-toggle="tab">Application</a></li>
                                                <li><a href="#authgroups" class="tab-btn" data-toggle="tab">Auth Groups</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="fonctions" style="position: relative">
                                            {{-- <table id="dataTableFr" class="table table-bordered table-hover datatable">
                                                <thead>
                                                    <tr>
                                                        <th>Code</th>
                                                        <th>Description FR</th>
                                                        <th>Description EN</th>
                                                        <th>Opérations</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    @foreach (App\Fonction::get() as $index => $fonction)
                                                        <tr>
                                                            <td>{{ $fonction->code }}</td>
                                                            <td>{{ $fonction->desc_fr }}</td>
                                                            <td>{{ $fonction->desc_en }}</td>
                                                            <td>
                                                                <a href="{{ route('admin.parameter.referentiels-edit', 1) }}" class="btn text-info btn-sm"><i class="fa fa-edit"></i></a>
                                                                <a href="{{ route('admin.parameter.referentiels-edit', 1) }}" class="btn text-danger btn-sm"><i class="fa fa-trash"></i></a>
                                                            </td>
                                                        </tr>
                                                    @endforeach

                                                    
                                                </tbody>
                                            </table> --}}
                                            <div class="py-2">
                                                <button type="button" class="btn btn-primary btn-sm" id="addFonction"><i class="fa fa-plus"></i> Add New</button>
                                            </div>
                                            <table class="table table-bordered table-hover" style="height: 52vh !important; display: block; width: 100% !important; overflow-y: scroll;">
                                                <thead>
                                                    <tr class="w-100">
                                                        <th style="width: 10%">#</th>
                                                        <th>Code</th>
                                                        <th>Description FR</th>
                                                        <th>Description EN</th>
                                                        <th>Applications</th>
                                                        <th style="width: 10%">Opérations</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach (App\Fonction::get() as $index => $fonction)
                                                        <tr>
                                                            <td>{{ ++$index }}</td>
                                                            <td>{{ $fonction->code }}</td>
                                                            <td>{{ $fonction->desc_fr }}</td>
                                                            <td>{{ $fonction->desc_en }}</td>
                                                            <td>{{ $fonction->applications }}</td>
                                                            <td>
                                                                <button type="button" class="btn text-info btn-sm editFonction" data-fonction="{{ json_encode($fonction) }}"><i class="fa fa-edit"></i></button>
                                                                <button type="button" class="btn text-danger btn-sm deleteTarget" data-target="fonction" data-id="{{ $fonction->id }}"><i class="fa fa-trash"></i></button>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="tab-pane" id="secteurs">
                                            <div class="py-2">
                                                <button type="button" class="btn btn-primary btn-sm" id="addSecteur"><i class="fa fa-plus"></i> Add New</button>
                                            </div>
                                            <table class="table table-bordered table-hover" style="height: 52vh !important; display: block; width: 100% !important; overflow-y: scroll;">
                                                <thead>
                                                    <tr class="w-100">
                                                        <th style="width: 10%">#</th>
                                                        <th>Code</th>
                                                        <th>Description FR</th>
                                                        <th>Description EN</th>
                                                        <th>Applications</th>
                                                        <th style="width: 10%">Opérations</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach (App\Secteur::get() as $index => $secteur)
                                                        <tr>
                                                            <td>{{ ++$index }}</td>
                                                            <td>{{ $secteur->code }}</td>
                                                            <td>{{ $secteur->desc_fr }}</td>
                                                            <td>{{ $secteur->desc_en }}</td>
                                                            <td>{{ $secteur->applications }}</td>
                                                            <td>
                                                                <button type="button" class="btn text-info btn-sm editSecteur" data-secteur="{{ json_encode($secteur) }}"><i class="fa fa-edit"></i></button>
                                                                <button type="button" class="btn text-danger btn-sm deleteTarget" data-target="secteur" data-id="{{ $secteur->id }}"><i class="fa fa-trash"></i></button>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="tab-pane" id="themes">
                                            <div class="py-2">
                                                <button type="button" class="btn btn-primary btn-sm" id="addTheme"><i class="fa fa-plus"></i> Add New</button>
                                            </div>
                                            <table class="table table-bordered table-hover" style="height: 52vh !important; display: block; width: 100% !important; overflow-y: scroll;">
                                                <thead>
                                                    <tr class="w-100">
                                                        <th style="width: 10%">#</th>
                                                        <th>Code</th>
                                                        <th>Description FR</th>
                                                        <th>Description EN</th>
                                                        <th>Applications</th>
                                                        <th style="width: 10%">Opérations</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach (App\Theme::get() as $index => $theme)
                                                        <tr>
                                                            <td>{{ ++$index }}</td>
                                                            <td>{{ $theme->code }}</td>
                                                            <td>{{ $theme->desc_fr }}</td>
                                                            <td>{{ $theme->desc_en }}</td>
                                                            <td>{{ $theme->applications }}</td>
                                                            <td>
                                                                <button type="button" class="btn text-info btn-sm editTheme" data-theme="{{ json_encode($theme) }}"><i class="fa fa-edit"></i></button>
                                                                <button type="button" class="btn text-danger btn-sm deleteTarget" data-target="theme" data-id="{{ $theme->id }}"><i class="fa fa-trash"></i></button>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="tab-pane" id="typedinfo">
                                            <div class="py-2">
                                                <button type="button" class="btn btn-primary btn-sm" id="addTypeInfo"><i class="fa fa-plus"></i> Add New</button>
                                            </div>
                                            <table class="table table-bordered table-hover" style="height: 52vh !important; display: block; width: 100% !important; overflow-y: scroll;">
                                                <thead>
                                                    <tr class="w-100">
                                                        <th style="width: 10%">#</th>
                                                        <th>Code</th>
                                                        <th>Description FR</th>
                                                        <th>Description EN</th>
                                                        <th>Applications</th>
                                                        <th style="width: 10%">Opérations</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach (App\TypeInfo::get() as $index => $typedeinfo)
                                                        <tr>
                                                            <td>{{ ++$index }}</td>
                                                            <td>{{ $typedeinfo->code }}</td>
                                                            <td>{{ $typedeinfo->desc_fr }}</td>
                                                            <td>{{ $typedeinfo->desc_en }}</td>
                                                            <td>{{ $typedeinfo->applications }}</td>
                                                            <td>
                                                                <button type="button" class="btn text-info btn-sm editTypeInfo" data-typedeinfo="{{ json_encode($typedeinfo) }}"><i class="fa fa-edit"></i></button>
                                                                <button type="button" class="btn text-danger btn-sm deleteTarget" data-target="typedeinfo" data-id="{{ $typedeinfo->id }}"><i class="fa fa-trash"></i></button>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="tab-pane" id="langue">
                                            <table class="table table-bordered table-hover" style="height: 52vh !important; display: block; width: 100% !important; overflow-y: scroll;">
                                                <thead>
                                                    <tr class="w-100">
                                                        <th style="width: 10%">#</th>
                                                        <th style="width: 10%">Code</th>
                                                        <th style="width: 70%">Description</th>
                                                        <th style="width: 10%">Opérations</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach (App\Langue::get() as $index => $langue)
                                                        <tr>
                                                            <td>{{ ++$index }}</td>
                                                            <td>{{ $langue->code }}</td>
                                                            <td>{{ $langue->description }}</td>
                                                            <td>
                                                                <button type="button" class="btn text-info btn-sm editLangue" data-langue="{{ json_encode($langue) }}"><i class="fa fa-edit"></i></button>
                                                                <button type="button" class="btn text-danger btn-sm deleteTarget" data-target="langue" data-id="{{ $langue->id }}"><i class="fa fa-trash"></i></button>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="tab-pane" id="application">
                                            <div class="py-2">
                                                <button type="button" class="btn btn-primary btn-sm" id="addApplication"><i class="fa fa-plus"></i> Add New</button>
                                            </div>
                                            <table class="table table-bordered table-hover" style="height: 52vh !important; display: block; width: 100% !important; overflow-y: scroll;">
                                                <thead>
                                                    <tr class="w-100">
                                                        <th style="width: 10%">#</th>
                                                        <th style="width: 10%">Name</th>
                                                        <th style="width: 35%">Description</th>
                                                        <th style="width: 35%">Solrcore Url</th>
                                                        <th style="width: 10%">Opérations</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach (App\Application::get() as $index => $application)
                                                        <tr>
                                                            <td>{{ ++$index }}</td>
                                                            <td>{{ $application->name }}</td>
                                                            <td>{{ $application->description }}</td>
                                                            <td>{{ $application->solrcore_url }}</td>
                                                            <td>
                                                                <button type="button" class="btn text-danger btn-sm deleteTarget" data-target="application" data-id="{{ $application->id }}"><i class="fa fa-trash"></i></button>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="tab-pane" id="authgroups">
                                            <div class="py-2">
                                                <button type="button" class="btn btn-primary btn-sm" id="addAuthGroup"><i class="fa fa-plus"></i> Add New</button>
                                            </div>
                                            <table class="table table-bordered table-hover" style="height: 52vh !important; display: block; width: 100% !important; overflow-y: scroll;">
                                                <thead>
                                                <tr class="w-100">
                                                    <th style="width: 10%">#</th>
                                                    <th style="width: 10%">Code</th>
                                                    <th style="width: 35%">Description</th>
                                                    <th style="width: 35%">Applications</th>
                                                    <th style="width: 10%">Opérations</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach (App\AuthGroup::get() as $index => $authGroup)
                                                    <tr>
                                                        <td>{{ ++$index }}</td>
                                                        <td>{{ $authGroup->code ?? '---' }}</td>
                                                        <td>{{ $authGroup->description ?? '---' }}</td>
                                                        <td>{{ $authGroup->applications ?? '---' }}</td>
                                                        <td>
                                                            <button type="button" class="btn text-info btn-sm editAuthGroup" data-auth-group="{{ json_encode($authGroup) }}"><i class="fa fa-edit"></i></button>
                                                            <button type="button" class="btn text-danger btn-sm deleteTarget" data-target="authgroup" data-id="{{ $authGroup->id }}"><i class="fa fa-trash"></i></button>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                            </div>


                        </div>
                    </div>


                </div>

            </div>
            <!--End row-->


        </div>
    </div><!-- end app-content-->


    @include('admin.parameter.referentiels.modal.modal')

@endsection


@section('scoped_js')
    <script>

        // $(document).ready(function () {
        //     $('.select2').each(function (e) {
        //         $(this).select2();
        //     })
        // });
        $('.datatable').each(function () {
            $(this).DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
            });
        });


        // Fonction
        $('.editFonction').each(function () {
            $(this).on('click', function (e) {
                e.preventDefault();
                
                // Get Fonction data from Fonction Object (PHP)
                var fonction = $(this).data('fonction');

                // Set applications as array and handle it

                // var fonctionArr = [];
                if (fonction.applications.indexOf(',')) {
                    fonctionArr = fonction.applications.split(',');
                }
                var selectedVal = new Array();
                jQuery.each(fonctionArr, function(index) {
                    selectedVal[index] = fonctionArr[index];
                });
                // Set values for the fonction fields
                $('#fonctionId').val(fonction.id);
                $('#fonctionDescFr').val(fonction.desc_fr);
                $('#fonctionDescEn').val(fonction.desc_en);
                $('#fonction_applicationsEdit').select2();
                $('#fonction_applicationsEdit').val(selectedVal).trigger('change');

                // Set button text
                $('#fonctionEditModal button[type="submit"]').text('Mettre à jour');

                // Show Fonction Modal
                $('#fonctionEditModal').modal('show');
            });
        });

        $('#addFonction').on('click', function (e) {
            e.preventDefault();
            // Set button text
            $('#fonctionCreateModal button[type="submit"]').text('Enregistrer');
            $('#fonction_code, #fonction_applications').select2();
            // Show Fonction Modal
            $('#fonctionCreateModal').modal('show');
        });

        // Secteur
        $('.editSecteur').each(function () {
            $(this).on('click', function (e) {
                e.preventDefault();
                
                // Get secteur data from secteur Object (PHP)
                var secteur = $(this).data('secteur');

                // var fonctionArr = [];
                if (secteur.applications.indexOf(',')) {
                    secteurArr = secteur.applications.split(',');
                }
                var selectedVal = new Array();
                jQuery.each(secteurArr, function(index) {
                    selectedVal[index] = secteurArr[index];
                });
                
                // Set values for the secteur fields
                $('#secteurId').val(secteur.id);
                $('#secteurDescFr').val(secteur.desc_fr);
                $('#secteurDescEn').val(secteur.desc_en);
                $('#secteur_applicationsEdit').select2();
                $('#secteur_applicationsEdit').val(selectedVal).trigger('change');

                // Set button text
                $('#secteurEditModal button[type="submit"]').text('Mettre à jour');

                // Show secteur Modal
                $('#secteurEditModal').modal('show');
            });
        });

        $('#addSecteur').on('click', function (e) {
            e.preventDefault();
            // Set button text
            $('#secteurCreateModal button[type="submit"]').text('Enregistrer');
            $('#secteur_code, #secteur_applications').select2();
            // Show secteur Modal
            $('#secteurCreateModal').modal('show');
        });

        // Theme
        $('.editTheme').each(function () {
            $(this).on('click', function (e) {
                e.preventDefault();
                
                // Get theme data from theme Object (PHP)
                var theme = $(this).data('theme');

                // var fonctionArr = [];
                if (theme.applications.indexOf(',')) {
                    themeArr = theme.applications.split(',');
                }
                var selectedVal = new Array();
                jQuery.each(themeArr, function(index) {
                    selectedVal[index] = themeArr[index];
                });
                // Set values for the theme fields
                $('#themeId').val(theme.id);
                $('#themeDescFr').val(theme.desc_fr);
                $('#themeDescEn').val(theme.desc_en);
                $('#theme_applicationsEdit').select2();
                $('#theme_applicationsEdit').val(selectedVal).trigger('change');

                // Set button text
                $('#themeEditModal button[type="submit"]').text('Mettre à jour');

                // Show theme Modal
                $('#themeEditModal').modal('show');
            });
        });

        $('#addTheme').on('click', function (e) {
            e.preventDefault();
            // Set button text
            $('#themeCreateModal button[type="submit"]').text('Enregistrer');
            $('#theme_code, #theme_applications').select2();
            // Show theme Modal
            $('#themeCreateModal').modal('show');
        });

        
        // TypeInfo
        $('.editTypeInfo').each(function () {
            $(this).on('click', function (e) {
                e.preventDefault();
                
                // Get TypeInfo data from TypeInfo Object (PHP)
                var typedeinfo = $(this).data('typedeinfo');

                // var fonctionArr = [];
                if (typedeinfo.applications.indexOf(',')) {
                    typedeinfoArr = typedeinfo.applications.split(',');
                }
                var selectedVal = new Array();
                jQuery.each(typedeinfoArr, function(index) {
                    selectedVal[index] = typedeinfoArr[index];
                });
                // Set values for the typedeinfo fields
                $('#typedeinfoId').val(typedeinfo.id);
                $('#typedeinfoDescFr').val(typedeinfo.desc_fr);
                $('#typedeinfoDescEn').val(typedeinfo.desc_en);
                $('#typedeinfo_applicationsEdit').select2();
                $('#typedeinfo_applicationsEdit').val(selectedVal).trigger('change');

                // Set button text
                $('#typedeinfoEditModal button[type="submit"]').text('Mettre à jour');

                // Show TypeInfo Modal
                $('#typedeinfoEditModal').modal('show');
            });
        });

        $('#addTypeInfo').on('click', function (e) {
            e.preventDefault();
            // Set button text
            $('#typedeinfoCreateModal button[type="submit"]').text('Enregistrer');
            $('#typedeinfo_code, #typedeinfo_applications').select2();
            // Show typedeinfo Modal
            $('#typedeinfoCreateModal').modal('show');
        });

        // Langue
        $('.editLangue').each(function () {
            $(this).on('click', function (e) {
                e.preventDefault();
                
                // Get Langue data from Langue Object (PHP)
                var langue = $(this).data('langue');

                // Set values for the langue fields
                $('#langueId').val(langue.id);
                $('#langueDescription').val(langue.description);

                // Set button text
                $('#langueEditModal button[type="submit"]').text('Mettre à jour');

                // Show Langue Modal
                $('#langueEditModal').modal('show');
            });
        });

        // Application
        $('#addApplication').on('click', function (e) {
            e.preventDefault();
            // Set button text
            $('#applicationCreateModal button[type="submit"]').text('Enregistrer');
            $('#application_code').select2();
            // Show application Modal
            $('#applicationCreateModal').modal('show');
        });


        // Auth Group
        $('#addAuthGroup').on('click', function (e) {
            e.preventDefault();
            // Set button text
            $('#authGroupCreateModal button[type="submit"]').text('Enregistrer');
            $('#authGroupApplications').select2();
            // Show authGroup Modal
            $('#authGroupCreateModal').modal('show');
        });
        $('.editAuthGroup').each(function () {
            $(this).on('click', function (e) {
                e.preventDefault();

                // Get Langue data from Langue Object (PHP)
                var authGroup = $(this).data('authGroup');
                console.log(authGroup);
                // Set values for the langue fields
                $('#authGroupIdEdit').val(authGroup.id);
                $('#authGroupCodeEdit').html(authGroup.code);
                $('#authGroupDescEdit').val(authGroup.description);
                if(authGroup.applications != null){
                    $('#authGroupApplicationsEdit').val(authGroup.applications.split(',')).select2();
                } else {
                    $('#authGroupApplicationsEdit').select2();
                }
                // Set button text
                $('#authGroupEditModal button[type="submit"]').text('Mettre à jour');

                // Show Langue Modal
                $('#authGroupEditModal').modal('show');
            });
        });



        // Deleting Items
        $('.deleteTarget').each(function () {
            var target = $(this).data('target');
            var id = $(this).data('id');
            $(this).on('click', function () {
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You want to delete this ?",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.value) {
                        var data = {_token: "{{ csrf_token() }}", id: id, target: target};
                        $.post("{{ route('admin.parameter.referentiels-delete-items') }}", data, function (res) {
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: res.message,
                                showConfirmButton: false,
                                timer: 2500,
                                toast: true
                            });

                            setTimeout(function () {
                                window.location.reload();
                            }, 3500);

                        })
                    }
                })
            });
        })

        $('.tab-btn').each(function () {
            $(this).on('click', function () {
                var name = $(this).text();
                $('#tabName').text(name);
            })
        })

    </script>
@endsection

