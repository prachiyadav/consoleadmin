<!-- Fonction Create  Modal -->
<div class="modal fade" id="fonctionCreateModal" tabindex="-1" role="dialog" aria-labelledby="fonctionCreateModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form action="{{ route('admin.parameter.referentiels-create-fonction') }}" method="post">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="fonctionCreateModalTitle">Create New Fonction</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{-- <div class="form-group">
                        <label for="fonctionCode">Code</label>
                        <input type="text" class="form-control" name="fonction_code" id="fonctionCode" placeholder="Code">
                    </div> --}}
                    <div class="form-group">
                        <label>Code</label>
                        {{-- <input type="text" class="form-control" id="code" placeholder="Code" name="code" /> --}}
                        <select class="form-control required select2" id="fonction_code" name="fonction_code">
                            <option value="" disabled>--Select Code--</option>
                            <option value="newcode" selected>New Code</option>
                            @foreach (App\Fonction::where('code', 'LIKE', '%F-_')->orWhere('code', 'LIKE', '%F-__')->get() as $fonction)
                                <option value="{{ $fonction->code }}">{{ $fonction->desc_fr }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="fonctionDescFr">Description FR</label>
                        <input type="text" class="form-control" name="fonction_desc_fr" placeholder="Description FR">
                    </div>
                    <div class="form-group">
                        <label for="fonctionDescEN">Description EN</label>
                        <input type="text" class="form-control" name="fonction_desc_en" placeholder="Description EN">
                    </div>
                    <div class="form-group">
                        <label>Applications</label>
                        <select class="form-control required select2" id="fonction_applications" name="fonction_applications[]" multiple>
                            <option value="" disabled>--Select Application--</option>
                            @foreach (App\Application::get() as $app)
                                <option value="{{ $app->name }}">{{ $app->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-success">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Fonction Edit  Modal -->
<div class="modal fade" id="fonctionEditModal" tabindex="-1" role="dialog" aria-labelledby="fonctionEditModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form action="{{ route('admin.parameter.referentiels-update-fonction') }}" method="post">
                @csrf
                <input type="hidden" name="fonction_id" id="fonctionId">
                <div class="modal-header">
                    <h5 class="modal-title" id="fonctionEditModalTitle">Edit Fonction</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="fonctionDescFr">Description FR</label>
                        <input type="text" class="form-control" name="fonction_desc_fr" id="fonctionDescFr" placeholder="Description FR">
                    </div>
                    <div class="form-group">
                        <label for="fonctionDescEn">Description EN</label>
                        <input type="text" class="form-control" name="fonction_desc_en" id="fonctionDescEn" placeholder="Description EN">
                    </div>
                    <div class="form-group">
                        <label>Applications</label>
                        <select class="form-control required select2" id="fonction_applicationsEdit" name="fonction_applications[]" multiple>
                            <option value="" disabled>--Select Application--</option>
                            @foreach (App\Application::get() as $app)
                                <option value="{{ $app->name }}">{{ $app->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-success">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>


{{-- Secteur --}}
<!-- Secteur Create  Modal -->
<div class="modal fade" id="secteurCreateModal" tabindex="-1" role="dialog" aria-labelledby="secteurCreateModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form action="{{ route('admin.parameter.referentiels-create-secteur') }}" method="post">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="secteurCreateModalTitle">Create New Secteur</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Code</label>
                        {{-- <input type="text" class="form-control" id="code" placeholder="Code" name="code" /> --}}
                        <select class="form-control required select2" id="secteur_code" name="secteur_code">
                            <option value="" disabled>--Select Code--</option>
                            <option value="newcode" selected>New Code</option>
                            @foreach (App\Secteur::where('code', 'LIKE', '%S-_')->orWhere('code', 'LIKE', '%S-__')->get() as $secteur)
                                <option value="{{ $secteur->code }}">{{ $secteur->desc_fr }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="secteurDescFr">Description FR</label>
                        <input type="text" class="form-control" name="secteur_desc_fr" placeholder="Description FR">
                    </div>
                    <div class="form-group">
                        <label for="secteurDescEN">Description EN</label>
                        <input type="text" class="form-control" name="secteur_desc_en" placeholder="Description EN">
                    </div>
                    <div class="form-group">
                        <label>Applications</label>
                        <select class="form-control required select2" id="secteur_applications" name="secteur_applications[]" multiple>
                            <option value="" disabled>--Select Application--</option>
                            @foreach (App\Application::get() as $app)
                                <option value="{{ $app->name }}">{{ $app->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-success">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Secteur Edit  Modal -->
<div class="modal fade" id="secteurEditModal" tabindex="-1" role="dialog" aria-labelledby="secteurEditModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form action="{{ route('admin.parameter.referentiels-update-secteur') }}" method="post">
                @csrf
                <input type="hidden" name="secteur_id" id="secteurId">
                <div class="modal-header">
                    <h5 class="modal-title" id="secteurEditModalTitle">Edit Secteur</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="secteurDescFr">Description FR</label>
                        <input type="text" class="form-control" name="secteur_desc_fr" id="secteurDescFr" placeholder="Description FR">
                    </div>
                    <div class="form-group">
                        <label for="secteurDescEn">Description EN</label>
                        <input type="text" class="form-control" name="secteur_desc_en" id="secteurDescEn" placeholder="Description EN">
                    </div>
                    <div class="form-group">
                        <label>Applications</label>
                        <select class="form-control required select2" id="secteur_applicationsEdit" name="secteur_applications[]" multiple>
                            <option value="" disabled>--Select Application--</option>
                            @foreach (App\Application::get() as $app)
                                <option value="{{ $app->name }}">{{ $app->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-success">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>



{{-- Theme --}}
<!-- Theme Create  Modal -->
<div class="modal fade" id="themeCreateModal" tabindex="-1" role="dialog" aria-labelledby="themeCreateModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form action="{{ route('admin.parameter.referentiels-create-theme') }}" method="post">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="themeCreateModalTitle">Create New Theme</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Code</label>
                        {{-- <input type="text" class="form-control" id="code" placeholder="Code" name="code" /> --}}
                        <select class="form-control required select2" id="theme_code" name="theme_code">
                            <option value="" disabled>--Select Code--</option>
                            <option value="newcode" selected>New Code</option>
                            @foreach (App\Theme::where('code', 'LIKE', '%T-_')->orWhere('code', 'LIKE', '%T-__')->get() as $theme)
                                <option value="{{ $theme->code }}">{{ $theme->desc_fr }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="themeDescFr">Description FR</label>
                        <input type="text" class="form-control" name="theme_desc_fr" placeholder="Description FR">
                    </div>
                    <div class="form-group">
                        <label for="themeDescEN">Description EN</label>
                        <input type="text" class="form-control" name="theme_desc_en" placeholder="Description EN">
                    </div>
                    <div class="form-group">
                        <label>Applications</label>
                        <select class="form-control required select2" id="theme_applications" name="theme_applications[]" multiple>
                            <option value="" disabled>--Select Application--</option>
                            @foreach (App\Application::get() as $app)
                                <option value="{{ $app->name }}">{{ $app->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-success">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- theme Edit  Modal -->
<div class="modal fade" id="themeEditModal" tabindex="-1" role="dialog" aria-labelledby="themeEditModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form action="{{ route('admin.parameter.referentiels-update-theme') }}" method="post">
                @csrf
                <input type="hidden" name="theme_id" id="themeId">
                <div class="modal-header">
                    <h5 class="modal-title" id="themeEditModalTitle">Edit Theme</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="themeDescFr">Description FR</label>
                        <input type="text" class="form-control" name="theme_desc_fr" id="themeDescFr" placeholder="Description FR">
                    </div>
                    <div class="form-group">
                        <label for="themeDescEn">Description EN</label>
                        <input type="text" class="form-control" name="theme_desc_en" id="themeDescEn" placeholder="Description EN">
                    </div>
                    <div class="form-group">
                        <label>Applications</label>
                        <select class="form-control required select2" id="theme_applicationsEdit" name="theme_applications[]" multiple>
                            <option value="" disabled>--Select Application--</option>
                            @foreach (App\Application::get() as $app)
                                <option value="{{ $app->name }}">{{ $app->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-success">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>


{{-- TypeInfo --}}
<!-- TypeInfo Create  Modal -->
<div class="modal fade" id="typedeinfoCreateModal" tabindex="-1" role="dialog" aria-labelledby="typedeinfoCreateModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form action="{{ route('admin.parameter.referentiels-create-typedeinfo') }}" method="post">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="typedeinfoCreateModalTitle">Create New TypeInfo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Code</label>
                        {{-- <input type="text" class="form-control" id="code" placeholder="Code" name="code" /> --}}
                        <select class="form-control required select2" id="typedeinfo_code" name="typedeinfo_code">
                            <option value="" disabled>--Select Code--</option>
                            <option value="newcode" selected>New Code</option>
                            @foreach (App\TypeInfo::where('code', 'LIKE', '%TI-_')->orWhere('code', 'LIKE', '%TI-__')->get() as $typedeinfo)
                                <option value="{{ $typedeinfo->code }}">{{ $typedeinfo->desc_fr }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="typedeinfoDescFr">Description FR</label>
                        <input type="text" class="form-control" name="typedeinfo_desc_fr" placeholder="Description FR">
                    </div>
                    <div class="form-group">
                        <label for="typedeinfoDescEN">Description EN</label>
                        <input type="text" class="form-control" name="typedeinfo_desc_en" placeholder="Description EN">
                    </div>
                    <div class="form-group">
                        <label>Applications</label>
                        <select class="form-control required select2" id="typedeinfo_applications" name="typedeinfo_applications[]" multiple>
                            <option value="" disabled>--Select Application--</option>
                            @foreach (App\Application::get() as $app)
                                <option value="{{ $app->name }}">{{ $app->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-success">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- TypeInfo Edit  Modal -->
<div class="modal fade" id="typedeinfoEditModal" tabindex="-1" role="dialog" aria-labelledby="typedeinfoEditModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form action="{{ route('admin.parameter.referentiels-update-typedeinfo') }}" method="post">
                @csrf
                <input type="hidden" name="typedeinfo_id" id="typedeinfoId">
                <div class="modal-header">
                    <h5 class="modal-title" id="typedeinfoEditModalTitle">Edit TypeInfo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="typedeinfoDescFr">Description FR</label>
                        <input type="text" class="form-control" name="typedeinfo_desc_fr" id="typedeinfoDescFr" placeholder="Description FR">
                    </div>
                    <div class="form-group">
                        <label for="typedeinfoDescEn">Description EN</label>
                        <input type="text" class="form-control" name="typedeinfo_desc_en" id="typedeinfoDescEn" placeholder="Description EN">
                    </div>
                    <div class="form-group">
                        <label>Applications</label>
                        <select class="form-control required select2" id="typedeinfo_applicationsEdit" name="typedeinfo_applications[]" multiple>
                            <option value="" disabled>--Select Application--</option>
                            @foreach (App\Application::get() as $app)
                                <option value="{{ $app->name }}">{{ $app->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-success">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- Langue --}}
<!-- langue Edit  Modal -->
<div class="modal fade" id="langueEditModal" tabindex="-1" role="dialog" aria-labelledby="langueEditModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form action="{{ route('admin.parameter.referentiels-update-langue') }}" method="post">
                @csrf
                <input type="hidden" name="langue_id" id="langueId">
                <div class="modal-header">
                    <h5 class="modal-title" id="langueEditModalTitle">Edit Langue</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="langueDescription">Description</label>
                        <input type="text" class="form-control" name="langue_description" id="langueDescription" placeholder="Description">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-success">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>



{{-- application --}}
<!-- application Edit  Modal -->
<div class="modal fade" id="applicationCreateModal" tabindex="-1" role="dialog" aria-labelledby="applicationCreateModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form action="{{ route('admin.parameter.referentiels-create-application') }}" method="post">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="applicationCreateModalTitle">Edit application</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="applicationName">Name</label>
                        <input type="text" class="form-control" name="application_name" id="applicationName" placeholder="Name">
                    </div>
                    <div class="form-group">
                        <label for="applicationDescription">Description</label>
                        <textarea type="text" class="form-control" name="application_description" id="applicationDescription" placeholder="Description" rows="4" style="resize: none"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="applicationSolrcoreUrl">SolrcoreUrl</label>
                        <input type="text" class="form-control" name="application_solrcore_url" id="applicationSolrcoreUrl" placeholder="Solrcore Url">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-success">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>



{{-- AuthGroup --}}
<!-- AuthGroup Create  Modal -->
<div class="modal fade" id="authGroupCreateModal" tabindex="-1" role="dialog" aria-labelledby="authGroupCreateModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form action="{{ route('admin.parameter.referentiels-create-authgroup') }}" method="post">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="typedeinfoEditModalTitle">Create new auth group</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
{{--                    <div class="form-group">--}}
{{--                        <select class="form-control required select2" id="authGroupCode" name="auth_groups_code">--}}
{{--                            <option value="" disabled>--Select Application--</option>--}}
{{--                            <option value="">New Code</option>--}}
{{--                            @foreach (App\AuthGroup::get() as $ag)--}}
{{--                                <option value="{{ $ag->code }}">{{ $ag->code }}</option>--}}
{{--                            @endforeach--}}
{{--                        </select>--}}
{{--                    </div>--}}
                    <div class="form-group">
                        <label for="authGroupDesc">Description</label>
                        <textarea rows="4" class="form-control" name="auth_group_desc" id="authGroupDesc" placeholder="Description"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Applications</label>
                        <select class="form-control required select2" id="authGroupApplications" name="auth_group_applications[]" multiple>
                            <option value="" disabled>--Select Application--</option>
                            @foreach (App\Application::get() as $apps)
                                <option value="{{ $apps->name }}">{{ $apps->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-success">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- AuthGroup Edit  Modal -->
<div class="modal fade" id="authGroupEditModal" tabindex="-1" role="dialog" aria-labelledby="authGroupEditModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form action="{{ route('admin.parameter.referentiels-update-authgroup') }}" method="post">
                @csrf
                <input type="hidden" name="auth_group_id" id="authGroupIdEdit">
                <div class="modal-header">
                    <h5 class="modal-title" id="authGroupEditModalTitle">Edit AuthGroup</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <h3>Code: <span id="authGroupCodeEdit"></span></h3>
{{--                        <label for="authGroupCodeEdit">Code</label>--}}
{{--                        <input type="text" class="form-control" name="auth_group_code" id="authGroupCodeEdit" placeholder="Code">--}}
                    </div>
                    <div class="form-group">
                        <label for="authGroupDescEdit">Description</label>
                        <textarea rows="4" class="form-control" name="auth_group_desc" id="authGroupDescEdit" placeholder="Description"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Applications</label>
                        <select class="form-control required select2" id="authGroupApplicationsEdit" name="auth_group_applications[]" multiple>
                            <option value="" disabled>--Select Application--</option>
                            @foreach (App\Application::get() as $apps)
                                <option value="{{ $apps->name }}">{{ $apps->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-success">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
