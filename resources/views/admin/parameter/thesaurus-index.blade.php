@extends('layouts.admin.master')

@section('title', 'Thesaurus')

@section('scoped_css')
    <style>
        th, *{
            text-transform: none !important;
        }
    </style>
@endsection

@section('content')
    <div class="app-content">
        <div class="side-app">

            <!--Page header-->
            <div class="page-header">
                <div class="page-leftheader">
                    <h4 class="page-title">Thesaurus</h4>
                    <ol class="breadcrumb pl-0">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item">Paramètres</li>
                        <li class="breadcrumb-item active" aria-current="page">Thesaurus</li>
                    </ol>
                </div>
            </div>
            <!--End Page header-->


            <!--Row-->
            <div class="row">
                <div class="col-md-12">
                    <h4 class="p-0 m-0 text-muted">Tableau des Thesaurus</h4>
                    <hr class="my-3">
                    {{-- Tab 1 --}}
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title mr-auto">Ci-dessous la liste des Thesaurus disponibles. Veuillez selectionner un Thesaurus avant d'éffectuer une action.</h3>
                            <button class="btn btn-info"><i class="fa fa-plus"></i> Créer</button>
                        </div>
                        <div class="card-body p-0" style="overflow-x: scroll">


                            <div class="panel panel-primary">
                                <div class=" tab-menu-heading">
                                    <div class="tabs-menu1 ">
                                        <!-- Tabs -->
                                        <ul class="nav panel-tabs">
                                            <li class=""><a href="#fr" class="active" data-toggle="tab"><img src="{{ asset('assets/images/flags/fr.png') }}" alt="French" style="width: 30px"></a></li>
                                            <li><a href="#en" data-toggle="tab"><img src="{{ asset('assets/images/flags/en.png') }}" alt="English" style="width: 30px"></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="panel-body tabs-menu-body">
                                    <div class="tab-content">
                                        <div class="tab-pane active " id="fr">
                                            <table id="dataTableFr" class="table table-bordered table-hover datatable">
                                                <thead>
                                                <tr>
                                                    <th>Code</th>
                                                    <th>Description FR</th>
                                                    <th>Description EN</th>
                                                    <th>URLs</th>
                                                    <th>Profondeur Du Crawl</th>
                                                    <th>Nombre Limite de Documents</th>
                                                    <th>Langue</th>
                                                    <th>Fonctions</th>
                                                    <th>Secteurs</th>
                                                    <th>Types D'infos</th>
                                                    <th>Thèmes</th>
                                                    <th>Opérations</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>sddkj</td>
                                                    <td>jas</td>
                                                    <td> sdja</td>
                                                    <td>sfj</td>
                                                    <td>sfj</td>
                                                    <td>sfj</td>
                                                    <td>sfj</td>
                                                    <td>sfj</td>
                                                    <td>sfj</td>
                                                    <td>sfj</td>
                                                    <td>sfj</td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="tab-pane" id="en">
                                            <table id="dataTableEn" class="table table-bordered table-hover datatable">
                                                <thead>
                                                <tr>
                                                    <th>Code</th>
                                                    <th>Description FR</th>
                                                    <th>Description EN</th>
                                                    <th>URLs</th>
                                                    <th>Profondeur Du Crawl</th>
                                                    <th>Nombre Limite de Documents</th>
                                                    <th>Langue</th>
                                                    <th>Fonctions</th>
                                                    <th>Secteurs</th>
                                                    <th>Types D'infos</th>
                                                    <th>Thèmes</th>
                                                    <th>Opérations</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>sddkj</td>
                                                    <td>jas</td>
                                                    <td> sdja</td>
                                                    <td>sfj</td>
                                                    <td>sfj</td>
                                                    <td>sfj</td>
                                                    <td>sfj</td>
                                                    <td>sfj</td>
                                                    <td>sfj</td>
                                                    <td>sfj</td>
                                                    <td>sfj</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>








                        </div>
                    </div>


                </div>

            </div>
            <!--End row-->


        </div>
    </div><!-- end app-content-->
@endsection


@section('scoped_js')
    <script>
        $('.datatable').each(function () {
            $(this).DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
            });
        });
    </script>
@endsection

