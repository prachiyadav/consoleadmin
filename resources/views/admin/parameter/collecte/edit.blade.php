@extends('layouts.admin.master')

@section('title', 'Dashboard - Command')

@section('scoped_css')
    <style>
        .side-menu .slide .slide-menu .slide-item:hover{
            color: #fff !important;
            /*background: #0b7ec4;*/
        }
        .side-menu .side-menu__item:hover .side-menu__label, .side-menu .side-menu__item:hover .side-menu__icon{
            color: #fff !important;
        }
        .select2 {
            width: 100% !important;
        }
    </style>
@endsection

@section('content')
    <div class="app-content">
        <div class="side-app">

            <!--Page header-->
            <div class="page-header">
                <div class="page-leftheader">
                    <h4 class="page-title">Collecte</h4>
                    <ol class="breadcrumb pl-0">
                        <li class="breadcrumb-item"><a href="{{ route('admin.command.index') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.command.index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Collecte</li>
                    </ol>
                </div>
            </div>
            <!--End Page header-->


            <!--Row-->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header bg-light">
                            <h5 class="card-title">
                                Edit Collecte
                            </h5>
                        </div>
                    
                        <div class="card-body">
                            <form action="{{ route('admin.parameter.collect-update') }}" method="POST">
                                @csrf
                                <input type="hidden" name="id" value="{{ $collecte->id }}">
                                <div class="form-group">
                                    <label class="form-group-label" for="name">Nom</label>
                                    <input class="form-control" name="name" id="name" placeholder="Name" value="{{ old('name') ?? $collecte->nom }}" />
                                </div>
                                <div class="form-group">
                                    <label class="form-group-label" for="urls">URLs</label>
                                    <textarea class="form-control" name="urls" id="urls" rows="3">{{ old('urls') ?? $collecte->urls }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label class="form-group-label" for="result">Result</label>
                                    <textarea class="form-control" name="result" id="result" rows="10">{{ old('result') ?? $collecte->result }}</textarea>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!--End row-->
        </div>
    </div><!-- end app-content-->



    <!-- Modal -->
    <div class="modal fade" id="collectNameModal" tabindex="-1" role="dialog" aria-labelledby="collectNameModalTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            
            <div class="modal-content">
                <form action="{{ route('admin.publication.update') }}" method="POST">
                    @csrf
                    <input type="hidden" name="id" id="publicationIdInput">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Create New Collecte</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="collecteName">Name For The Collecte</label>
                            <input type="text" class="form-control" name="collecte_name" id="collecteName" placeholder="Collect Name">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="saveCollecte">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


@endsection


@section('scoped_js')
    <script>
        $(document).ready(function () {
            

            $('.select2').each(function (e) {
                $(this).select2();
            })

            $('#createCollectBtn').on('click', function () {
                $('#collectNameModal').modal('show'); 
            });

            // Collect Save Functionality
            $('#saveCollecte').on('click', function () {

                var data = {
                    _token: '{{ csrf_token() }}',
                    origines: $('#origines').val(),
                    name: $('#collecteName').val()
                }

                $.post("{{ route('admin.parameter.collect-store') }}", data, function (res) {
                    if (res.status == 'alert') {
                        Swal.fire({
                            title: res.message,
                            text: 'Do you want to update it with new value ?',
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Update'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                var upData = {
                                    _token: '{{ csrf_token() }}',
                                    update: true,
                                    origines: $('#origines').val(),
                                    name: $('#collecteName').val()
                                }
                                $.post("{{ route('admin.parameter.collect-store') }}", upData, function (response) {
                                    Swal.fire('Updated!', 'Nom is updated with new selected values.', 'success')

                                    $('#origines').val('');
                                    $('#collecteName').val('');
                                    $('#collectNameModal').modal('hide');
                                    window.location.reload()
                                })
                            }
                        })
                    } else {
                        Swal.fire('Success !', res.message, 'success');

                        $('#origines').val('');
                        $('#collecteName').val('');
                        $('#collectNameModal').modal('hide');
                        window.location.reload()
                    }
                });
            })

        });

    </script>
@endsection

