@extends('layouts.admin.master')

@section('title', 'Dashboard - Command')

@section('scoped_css')
    <style>
        .side-menu .slide .slide-menu .slide-item:hover{
            color: #fff !important;
            /*background: #0b7ec4;*/
        }
        .side-menu .side-menu__item:hover .side-menu__label, .side-menu .side-menu__item:hover .side-menu__icon{
            color: #fff !important;
        }
        .select2 {
            width: 100% !important;
        }
    </style>
@endsection

@section('content')
    <div class="app-content">
        <div class="side-app">

            <!--Page header-->
            <div class="page-header">
                <div class="page-leftheader">
                    <h4 class="page-title">Collecte</h4>
                    <ol class="breadcrumb pl-0">
                        <li class="breadcrumb-item"><a href="{{ route('admin.command.index') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.command.index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Collecte</li>
                    </ol>
                </div>
            </div>
            <!--End Page header-->


            <!--Row-->
            <div class="row">
                <div class="col-12">

                    <div id="accordion">

                        {{-- <h4 class="text-muted">Collecte</h4>
                        <hr class="my-3"> --}}
                        <div class="card">
                            <div class="card-header bg-light p-0" id="headingOne">
                                <h5 class="mb-0 w-100" style="cursor: pointer" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <button class="btn bg-transparent text-dark">
                                        Create Collecte
                                    </button>
                                </h5>
                            </div>
                        
                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">
                                    <div class="row align-items-center">
                                        <div class="col-lg-8">
                                            <form action="">
                                                <div class="form-group">
                                                    <label for="crawlerInput">Sélectionner les origines</label>
                                                    <select name="origines[]" id="origines" class="form-control select2" multiple>
                                                        {{-- @foreach (App\Collecte::get() as $index => $collecte)
                                                            <option value="{{ $collecte->id }}">{{ $collecte->nom }}</option>
                                                        @endforeach --}}
                                                        {{-- <option value="" selected disabled>--Sélectionner les origines--</option> --}}
                                                        @foreach (App\Origin::get() as $index => $origine)
                                                            <option value="{{ $origine->origin_url }}">{{ $origine->desc_fr }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-lg-4 d-flex flex-lg-column">
                                            {{-- <button class="btn btn-info mb-2 w-50 mr-1">Lancement</button> --}}
                                            <a href="#" id="createCollectBtn" class="btn btn-info mt-3 w-50">Create</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header bg-light p-0" id="headingTwo">
                                <h5 class="mb-0 w-100" style="cursor: pointer" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                    <button class="btn bg-transparent text-dark">
                                        Edit Collecte
                                    </button>
                                </h5>
                            </div>
                        
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                <div class="card-body">
                                    <div class="row align-items-center">
                                        <div class="col-lg-8">
                                            <form action="">
                                                <div class="form-group">
                                                    <label for="crawlerInput">Sélectionner les noms</label>
                                                    <select name="collectes" id="collecteInput" class="form-control select2">
                                                        <option value="" selected>--Select A Nom To Edit</option>
                                                        @foreach (App\Collecte::where('nom', 'LIKE', '%C-%')->get() as $index => $collecte)
                                                            <option value="{{ $collecte->id }}">{{ $collecte->nom }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-lg-4 d-flex flex-lg-column">
                                            <a href="javascript:void(0)" id="editCollectBtn" class="btn btn-info mt-3 w-50">Edition</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!--End row-->
        </div>
    </div><!-- end app-content-->



    <!-- Modal -->
    <div class="modal fade" id="collectNameModal" tabindex="-1" role="dialog" aria-labelledby="collectNameModalTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            
            <div class="modal-content">
                <form action="{{ route('admin.publication.update') }}" method="POST">
                    @csrf
                    <input type="hidden" name="id" id="publicationIdInput">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Create New Collecte</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <label for="collecteName">Name For The Collecte</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                              <span class="input-group-text" id="collecte-name-prefix">C-</span>
                            </div>
                            <input type="text" class="form-control" name="collecte_name" id="collecteName" placeholder="Collect Name" aria-describedby="collecte-name-prefix">
                        </div>
                        {{-- <div class="form-group">
                            <label for="collecteName">Name For The Collecte</label>
                            <input type="text" class="form-control" name="collecte_name" id="collecteName" placeholder="Collect Name">
                        </div> --}}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="saveCollecte">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


@endsection


@section('scoped_js')
    <script>
        $(document).ready(function () {
            

            $('.select2').each(function (e) {
                $(this).select2();
            })

            $('#createCollectBtn').on('click', function () {
                if ($('#origines').val() == null || $('#origines').val() == "") {
                    Swal.fire('Warning !', 'Please select 1 or more Origine(s) !', 'warning')
                } else {
                    $('#collectNameModal').modal('show'); 
                }
            });

            // Collect Save Functionality
            $('#saveCollecte').on('click', function () {

                var data = {
                    _token: '{{ csrf_token() }}',
                    origines: $('#origines').val(),
                    name: $('#collecteName').val()
                }

                $.post("{{ route('admin.parameter.collect-store') }}", data, function (res) {
                    if (res.status == 'alert') {
                        Swal.fire({
                            title: res.message,
                            text: 'Do you want to update it with new value ?',
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Update'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                var upData = {
                                    _token: '{{ csrf_token() }}',
                                    update: true,
                                    origines: $('#origines').val(),
                                    name: $('#collecteName').val()
                                }
                                $.post("{{ route('admin.parameter.collect-store') }}", upData, function (response) {
                                    Swal.fire('Updated!', 'Nom is updated with new selected values.', 'success')

                                    $('#origines').val('');
                                    $('#collecteName').val('');
                                    $('#collectNameModal').modal('hide');
                                    window.location.reload()
                                })
                            }
                        })
                    } else {
                        Swal.fire('Success !', res.message, 'success');

                        $('#origines').val('');
                        $('#collecteName').val('');
                        $('#collectNameModal').modal('hide');
                        window.location.reload()
                    }
                });
            })



            // Edit
            $('#collecteInput').on('change', function () {
                if ($(this).val()) {
                    $('#editCollectBtn').attr('href', '{{ route("admin.parameter.collect-edit") }}/'+$(this).val())
                } else {
                    $('#editCollectBtn').attr('href', 'javascript:void(0)')
                }
            });


        });

    </script>
@endsection

