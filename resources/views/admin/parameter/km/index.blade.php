@extends('layouts.admin.master')

@section('title', 'KM')

@section('scoped_css')
    <style>
        th, *{
            text-transform: none !important;
        }
        tr td button.btn {
            background: transparent;
            outline: none !important;
            box-shadow: none !important
        }
    </style>
@endsection

@section('content')
    <div class="app-content">
        <div class="side-app">

            <!--Page header-->
            <div class="page-header">
                <div class="page-leftheader">
                    <h4 class="page-title">Key Metadata</h4>
                    <ol class="breadcrumb pl-0">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item">Paramètres</li>
                        <li class="breadcrumb-item active" aria-current="page">Key Metadata</li>
                    </ol>
                </div>
            </div>
            <!--End Page header-->


            <!--Row-->
            <div class="row">
                <div class="col-md-12">
                    <h4 class="p-0 m-0 text-muted">Key Metadata</h4>
                    <hr class="my-3">
                    {{-- Tab 1 --}}
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title mr-auto">Ci-dessous la liste des référentiels disponibles. Veuillez selectionner un Thesaurus avant d'éffectuer une action.</h3>
                            <button class="btn btn-info" id="addKeyMeta"><i class="fa fa-plus"></i> Créer</button>
                        </div>
                        <div class="card-body" style="overflow-x: scroll">
                            <table id="dataTableFr" class="table table-bordered table-hover datatable">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Key Metadata</th>
                                        <th>Fonction</th>
                                        <th>Secteur</th>
                                        <th>Theme</th>
                                        <th>Type De Info</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @if ($keyMetas->count() > 0)
                                        @foreach ($keyMetas as $index => $keyMeta)
                                            <tr>
                                                <td>{{ ++$index }}</td>
                                                <td>{{ $keyMeta->key_metadata }}</td>
                                                <td>{{ App\Fonction::whereCode($keyMeta->fonction)->first()->desc_fr }}</td>
                                                <td>{{ App\Secteur::whereCode($keyMeta->secteur)->first()->desc_fr }}</td>
                                                <td>{{ App\Theme::whereCode($keyMeta->theme)->first()->desc_fr }}</td>
                                                <td>{{ App\TypeInfo::whereCode($keyMeta->type_de_info)->first()->desc_fr }}</td>
                                                <td>
                                                    <button type="button" class="btn text-info btn-sm editKeyMeta" data-key-meta="{{ json_encode($keyMeta) }}"><i class="fa fa-edit"></i></button>
                                                    <button type="button" class="btn text-danger btn-sm deleteKeyMeta" data-id="{{ $keyMeta->id }}"><i class="fa fa-trash"></i></button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif

                                    
                                </tbody>
                            </table>
                        </div>
                    </div>


                </div>

            </div>
            <!--End row-->
            <div class="container">
                {{-- <form action="{{ route('admin.parameter.km-import') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="custom-file">
                        <input type="file" name="csvfile" class="custom-file-input" id="csvfile">
                        <label class="custom-file-label" for="csvfile">Choose file</label>
                      </div>
                </form> --}}
                {{-- <button type="button" class="btn btn-info btn-sm">Import</button>
                <a href="{{ route('admin.parameter.km-export') }}" class="btn btn-info btn-sm">Export CSV</a> --}}
            </div>
        </div>
    </div><!-- end app-content-->


    <!-- Add Key Meta Modal -->
    <div class="modal fade" id="addKeyMetadataModal" tabindex="-1" role="dialog" aria-labelledby="addKeyMetadataModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form action="{{ route('admin.parameter.km-store') }}" method="POST">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="addKeyMetadataModalTitle">Add Key Metadata</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="key_metadata">Key Metadata</label>
                            <input type="text" class="form-control" name="key_metadata" placeholder="Key Metadata">
                        </div>
                        <div class="form-group">
                            <label for="key_metadata">Fonction</label>
                            <select class="form-control select2" name="fonction" style="width: 100%; display: block">
                                @foreach (App\Fonction::get() as $index => $fonction)
                                    <option value="{{ $fonction->code }}">{{ $fonction->desc_fr }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="key_metadata">Secteur</label>
                            <select class="form-control select2" name="secteur" style="width: 100%; display: block">
                                @foreach (App\Secteur::get() as $index => $secteur)
                                    <option value="{{ $secteur->code }}">{{ $secteur->desc_fr }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="key_metadata">Theme</label>
                            <select class="form-control select2" name="theme" style="width: 100%; display: block">
                                @foreach (App\Theme::get() as $index => $theme)
                                    <option value="{{ $theme->code }}">{{ $theme->desc_fr }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="key_metadata">Type De Info</label>
                            <select class="form-control select2" name="typedeinfo" style="width: 100%; display: block">
                                @foreach (App\TypeInfo::get() as $index => $typedeinfo)
                                    <option value="{{ $typedeinfo->code }}">{{ $typedeinfo->desc_fr }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Edit Key Meta Modal -->
    <div class="modal fade" id="editKeyMetadataModal" tabindex="-1" role="dialog" aria-labelledby="editKeyMetadataModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form action="{{ route('admin.parameter.km-update') }}" method="POST">
                    @csrf
                    <input type="hidden" name="id" id="key_metadata_id">
                    <div class="modal-header">
                        <h5 class="modal-title" id="editKeyMetadataModalTitle">Edit Key Metadata</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="key_metadata">Key Metadata</label>
                            <input type="text" class="form-control" name="key_metadata" id="key_metadata" placeholder="Key Metadata">
                        </div>
                        <div class="form-group">
                            <label for="key_metadata">Fonction</label>
                            <select class="form-control select2" name="fonction" id="fonction" style="width: 100%; display: block">
                                @foreach (App\Fonction::get() as $index => $fonction)
                                    <option value="{{ $fonction->code }}">{{ $fonction->desc_fr }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="key_metadata">Secteur</label>
                            <select class="form-control select2" name="secteur" id="secteur" style="width: 100%; display: block">
                                @foreach (App\Secteur::get() as $index => $secteur)
                                    <option value="{{ $secteur->code }}">{{ $secteur->desc_fr }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="key_metadata">Theme</label>
                            <select class="form-control select2" name="theme" id="theme" style="width: 100%; display: block">
                                @foreach (App\Theme::get() as $index => $theme)
                                    <option value="{{ $theme->code }}">{{ $theme->desc_fr }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="key_metadata">Type De Info</label>
                            <select class="form-control select2" name="typedeinfo" id="typedeinfo" style="width: 100%; display: block">
                                @foreach (App\TypeInfo::get() as $index => $typedeinfo)
                                    <option value="{{ $typedeinfo->code }}">{{ $typedeinfo->desc_fr }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Edit Key Meta Modal -->
    <div class="modal fade" id="importCsvModal" tabindex="-1" role="dialog" aria-labelledby="importCsvModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form action="{{ route('admin.parameter.km-import') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="importCsvModalTitle">Edit Key Metadata</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="custom-file">
                            <label class="custom-file-label" for="csvfile">Choose file</label>
                            <input type="file" name="csvfile" class="custom-file-input" id="csvfile" accept=".csv">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
@endsection


@section('scoped_js')
    <script>
        $('.datatable').each(function () {
            $(this).DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
            });
        });

        function openImportModal() {
            $('#importCsvModal').modal('show');
        }
        $(document).ready(function () {
            $('.select2').each(function () {
                $(this).select2();
            });

            var importExportBtn = `
                <button type="button" class="btn btn-info btn-sm" onclick="openImportModal()">Import</button>
                <a href="{{ route('admin.parameter.km-export') }}" class="btn btn-info btn-sm mr-3">Export CSV</a>
            `;

            $('#dataTableFr_filter').prepend(importExportBtn);
            $('#csvfile').change(function() {
                var i = $(this).prev('label').clone();
                var file = $('#csvfile')[0].files[0].name;
                $(this).prev('label').text(file);
            });
        });


        
        // Add Meta
        $('#addKeyMeta').on('click', function () {
            // Set button text
            $('#addKeyMetadataModal button[type="submit"]').text('Enregisterer');
            // Show keyMeta Modal
            $('#addKeyMetadataModal').modal('show');
        });

        // Edit KeyMeta
        $(document).on("click", '.editKeyMeta', function () {
            $(this).on('click', function (e) {
                e.preventDefault();
                
                // Get KeyMeta data from KeyMeta Object (PHP)
                var keyMeta = $(this).data('keyMeta');
                // console.log(keyMeta);

                // Set values for the keyMeta fields
                $('#key_metadata_id').val(keyMeta.id);
                $('#key_metadata').val(keyMeta.key_metadata);
                $('#fonction').val(keyMeta.fonction).trigger("change");
                $('#secteur').val(keyMeta.secteur).trigger("change");
                $('#theme').val(keyMeta.theme).trigger("change");
                $('#typedeinfo').val(keyMeta.type_de_info).trigger("change");

                // Set button text
                $('#editKeyMetadataModal button[type="submit"]').text('Mettre à jour');

                // Show keyMeta Modal
                $('#editKeyMetadataModal').modal('show');
            });
        });

        // Deleting Item
        $(document).on("click", '.deleteKeyMeta', function () {
            var id = $(this).data('id');
            $(this).on('click', function () {
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You want to delete this ?",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.value) {
                        var data = {_token: "{{ csrf_token() }}", id: id};
                        $.post("{{ route('admin.parameter.km-delete') }}", data, function (res) {
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: res.message,
                                showConfirmButton: false,
                                timer: 2500,
                                toast: true
                            });

                            setTimeout(function () {
                                window.location.reload();
                            }, 2500);

                        })
                    }
                })
            });
        })



    </script>
@endsection

