@extends('layouts.admin.master')

@section('title', 'Origines')

@section('scoped_css')
    <style>
        th, *{
            text-transform: none !important;
        }
        .edit_code_list {
            position: relative;
            height: 55vh;
            max-height: 55vh;
            /* background: red; */
            overflow-y: scroll;
            overflow-x: hidden;
        }
        .edit-codes {
            position: relative;
        }
        ul.parent-codes {
            position: relative;
            width: 250px;
            box-shadow: 0 0 3px 0 rgba(0, 0, 0, 0.3);
            border-radius: 2px;
        }

        ul.child-codes {
            position: absolute;
            top: 0;
            left: 100%;
            width: 250px;
            box-shadow: 0 0 3px 0 rgba(0, 0, 0, 0.3);
            border-radius: 2px;
            display: none;
            opacity: 0;
        }

        .code-list {
            position: relative;
            display: block;
        }
        .code-link {
            position: relative;
            display: block;
            padding: 12px 16px;
            border-bottom: 1px solid #eaeaea;
        }

        .code-link:hover + ul.child-codes {
            display: block;
            opacity: 1;
        }



    </style>
@endsection

@section('content')
    <div class="app-content">
        <div class="side-app">

            <!--Page header-->
            <div class="page-header">
                <div class="page-leftheader">
                    <h4 class="page-title">Origines</h4>
                    <ol class="breadcrumb pl-0">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item">Paramètres</li>
                        <li class="breadcrumb-item active" aria-current="page">Origines</li>
                    </ol>
                </div>
            </div>
            <!--End Page header-->


            <!--Row-->
            <div class="row">
                <div class="col-md-12">

                    <div id="accordion">
                        <h4 class="p-0 m-0 text-muted">Sources de documents</h4>
                        <hr class="my-3">
                        <div class="card">
                            <div class="card-header bg-light p-0" id="headingTwo">
                                <h5 class="mb-0 w-100 px-3" style="cursor: pointer" data-toggle="collapse" data-target="#originEditSelectSection"
                                    aria-expanded="false" aria-controls="collapseTwo">
                                    Ci-dessous la liste des Origines disponibles. Veuillez selectionner une origin avant d'éffectuer une action.
                                    <span class="float-right" style="font-weight: bold;">&darr;</span>
                                </h5>
                                <a href="{{ route('admin.parameter.origin-create') }}" class="btn btn-info mr-3"><i class="fa fa-plus"></i> Créer</a>
                            </div>
                            <div id="originEditSelectSection" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                <div class="card-body">
                                    <!-- edit_code_list starts here -->
                                    <div class="edit_code_list">
                                        <!-- row starts here -->
                                        <div class="row">
                                            <!-- col-md-4 starts here -->
                                            <div class="col-md-4 offset-md-2">
                                                <!-- edit-codes starts here -->
                                                <div class="edit-codes">
                                                    <!-- parent-codes starts here -->
                                                    <ul class="parent-codes">
                                                        @foreach (App\Origin::where('code', 'LIKE', '%O-__')->get() as $origin)
                                                            <li class="code-list">
                                                                <a href="{{ route('admin.parameter.origin-edit', $origin->code) }}" class="code-link">{{ $origin->desc_fr }} <span class="float-right">&raquo;</span></a>
                                                                <!-- child-codes starts here -->
                                                                <ul class="child-codes">
                                                                    @foreach (App\Origin::where('parent', '=', $origin->code)->get() as $childOrigin)
                                                                        <li class="code-list">
                                                                            <a href="{{ route('admin.parameter.origin-edit', $childOrigin->code) }}" class="code-link">{{ $childOrigin->desc_fr }}  <span class="float-right">&raquo;</span></a>
                                                                        </li>
                                                                    @endforeach
                                                                </ul> <!-- child-codes ends here -->
                                                            </li>
                                                        @endforeach
                                                    </ul> <!-- parent-codes ends here -->
                                                </div> <!-- edit-codes ends here -->
                                            </div> <!-- col-md-4 ends here -->
                                        </div> <!-- row ends here -->
                                    </div> <!-- edit_code_list ends here -->
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- Tab 1 --}}
                    {{-- <div class="card">
                        <div class="card-header px-3">
                            <h3 class="card-title mr-auto">Ci-dessous la liste des Origines disponibles. Veuillez selectionner une origin avant d'éffectuer une action.</h3>
                            <a href="{{ route('admin.parameter.origin-create') }}" class="btn btn-info"><i class="fa fa-plus"></i> Créer</a>
                        </div>
                        <div class="card-body">

                            
                            <!-- edit_code_list starts here -->
                            <div class="edit_code_list">


                                <!-- row starts here -->
                                <div class="row">
                                    <!-- col-md-4 starts here -->
                                    <div class="col-md-4 offset-md-2">
                                        <!-- edit-codes starts here -->
                                        <div class="edit-codes">
                                            <!-- parent-codes starts here -->
                                            <ul class="parent-codes">

                                                @foreach (App\Origin::where('code', 'LIKE', '%O-__')->get() as $origin)
                                                    <li class="code-list">
                                                        <a href="{{ route('admin.parameter.origin-edit', $origin->code) }}" class="code-link">{{ $origin->desc_fr }} <span class="float-right">&raquo;</span></a>
                                                        <!-- child-codes starts here -->
                                                        <ul class="child-codes">
                                                            @foreach (App\Origin::where('parent', '=', $origin->code)->get() as $childOrigin)
                                                                <li class="code-list">
                                                                    <a href="{{ route('admin.parameter.origin-edit', $childOrigin->code) }}" class="code-link">{{ $childOrigin->desc_fr }}  <span class="float-right">&raquo;</span></a>
                                                                </li>
                                                            @endforeach
                                                        </ul> <!-- child-codes ends here -->
                                                    </li>
                                                @endforeach
                                            </ul> <!-- parent-codes ends here -->
                                        </div> <!-- edit-codes ends here -->

                                    </div> <!-- col-md-4 ends here -->
                                </div> <!-- row ends here -->


                                
                            </div> <!-- edit_code_list ends here -->

                            {{-- <div class="row align-items-center">
                                <div class="col-lg-8">
                                    <form action="">
                                        <div class="row">





                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="parentOrigin">Sélectionner les origines (Parent)</label>
                                                    <select name="parent_code" id="parentOrigin" class="form-control select2">
                                                        <option value="" selected disabled>--Sélectionner les origines--</option>
                                                        @foreach (App\Origin::where('code', 'LIKE', '%O-__')->get() as $index => $origine)
                                                            <option value="{{ $origine->desc_fr }}">{{ $origine->desc_fr }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="childOrigin">Sélectionner les origines (Child)</label>
                                                    <select name="child_code" id="childOrigin" class="form-control select2">
                                                        <option value="" selected disabled>--Sélectionner les origines--</option>
                                                        @foreach (App\Origin::where('code', 'LIKE', '%O-__-__%')->get() as $index => $origine)
                                                            <option value="{{ $origine->desc_fr }}">{{ $origine->desc_fr }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-lg-4 d-flex flex-lg-column">
                                    <a href="{{ route('admin.parameter.origin-edit', 1) }}" id="crawlerEditBtn" class="btn btn-info w-50 mt-3">Edition</a>
                                </div>
                            </div> --}}

                        </div>
                    </div> 


                </div>

            </div>
            <!--End row-->


        </div>
    </div><!-- end app-content-->
@endsection


@section('scoped_js')
    <script>
        $(document).ready(function () {
            $('.select2').each(function (e) {
                $(this).select2();
            });

            $('#dataTable').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
            });
        });
    </script>
@endsection

