@extends('layouts.admin.master')

@section('title', 'Create Origin')

@section('scoped_css')
    <style>
        .side-menu .slide .slide-menu .slide-item:hover{
            color: #fff !important;
            /*background: #0b7ec4;*/
        }
        .side-menu .side-menu__item:hover .side-menu__label, .side-menu .side-menu__item:hover .side-menu__icon{
            color: #fff !important;
        }
        .select2 {
            width: 100% !important;
        }
    </style>
@endsection

@section('content')
    <div class="app-content">
        <div class="side-app">

            <!--Page header-->
            <div class="page-header">
                <div class="page-leftheader">
                    <h4 class="page-title">Origin</h4>
                    <ol class="breadcrumb pl-0">
                        <li class="breadcrumb-item"><a href="{{ route('admin.command.index') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.command.index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Origine</li>
                    </ol>
                </div>
            </div>
            <!--End Page header-->


            {{-- <create-origin :applications="{{ App\Application::get() }}" /> --}}

            <!--Row-->
                <div class="row">
                    <div class="col-12">
                        <div id="accordion">
                            <h4 class="text-muted">Basic Information</h4>

                            <hr class="my-3" />
                            <div class="card">
                                <div class="card-header bg-light p-0" id="headingOne">
                                    <h5 class="mb-0 w-100" style="cursor: pointer" data-toggle="collapse" data-target="#collapseOne"
                                        aria-expanded="true" aria-controls="collapseOne">
                                        <button class="btn bg-transparent text-dark">Basic Information</button>
                                    </h5>
                                </div>

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label>Code <span class="text-danger">*</span></label>
                                                    {{-- <input type="text" class="form-control" id="code" placeholder="Code" name="code" /> --}}
                                                    <select class="form-control required select2" id="code" name="code">
                                                        <option value="" disabled>--Select Type De Doc--</option>
                                                        <option value="newcode" selected>New Parent Code</option>
                                                        @foreach (App\Origin::where('code', 'LIKE', '%O-__')->get() as $origin)
                                                            <option value="{{ $origin->code }}">{{ $origin->desc_fr }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label>Desc Fr <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control required" id="descfr" placeholder="Desc Fr" name="descfr" />
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label>Desc En</label>
                                                    <input type="text" class="form-control" id="descen" placeholder="Desc En" name="descen" />
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label>Origin URL <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control required" id="originurl" placeholder="Origin URL" name="originurl" />
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="form-group">
                                                    <label>Langue <span class="text-danger">*</span></label>
                                                    <select class="form-control required select2" id="langue" name="langue">
                                                        <option value="" disabled>--Select Langue--</option>
                                                        @foreach (App\Langue::get() as $index => $langue)
                                                            <option value="{{ $langue->code }}" @if($index == 0) selected @endif>{{ $langue->description }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="form-group">
                                                    <label>Crawling Depth <span class="text-danger">*</span></label>
                                                    <select class="form-control required select2" id="crawlingdepth" name="crawlingdepth">
                                                        <option value="0" disabled selected>--Select Crawling Depth--</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="form-group">
                                                    <label>Limiter le nombre de documents <span class="text-danger">*</span></label>
                                                    <select class="form-control required select2" id="limitdedoc" name="limitdedoc">
                                                        <option value="0" disabled selected>--Select Limiter le nombre de documents--
                                                        </option>
                                                        <option value="50">50</option>
                                                        <option value="100">100</option>
                                                        <option value="500">500</option>
                                                        <option value="1000">1000</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="form-group">
                                                    <label>Type De Doc <span class="text-danger">*</span></label>
                                                    <select class="form-control required select2" id="typededoc" name="typededoc" multiple>
                                                        <option value="" disabled>--Select Type De Doc--</option>
                                                        <option value="html" selected>html</option>
                                                        <option value="pdf" selected>pdf</option>
                                                        <option value="xml" selected>xml</option>
                                                        <option value="jpeg">jpeg</option>
                                                        <option value="png">png</option>
                                                        <option value="xls">xls</option>
                                                        <option value="csv">csv</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-4">
                                                <div class="form-group">
                                                    <label>Application <span class="text-danger">*</span></label>
                                                    <select class="form-control required select2" id="application" name="application[]" multiple>
                                                        <option value disabled>--Select Type De Doc--</option>
                                                        @foreach (App\Application::get() as $application)
                                                        <option value="{{ $application->name }}">{{ $application->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-4">
                                                <div class="form-group">
                                                    <label>Auth groups <span class="text-danger">*</span></label>
                                                    <select class="form-control required select2" id="authgroups" name="authgroups[]" multiple>
                                                        <option value disabled>--Select auth group--</option>
                                                        @foreach (App\AuthGroup::get() as $authGroup)
                                                            <option value="{{ $authGroup->code }}" @if($authGroup->code == 'AG0') selected @endif>{{ $authGroup->code }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="form-group">
                                                    <label class="d-flex align-items-center">
                                                        <input type="checkbox" name="a_indexer" id="a_indexer" value="1" checked>
                                                        <span class="d-block ml-3">Indexer</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h4 class="text-muted">Filter</h4>
                            <hr class="my-3" />
                            <div class="card">
                                <div class="card-header bg-light p-0" id="headingTwo">
                                    <h5 class="mb-0 w-100" style="cursor: pointer" data-toggle="collapse" data-target="#collapseTwo"
                                        aria-expanded="false" aria-controls="collapseTwo">
                                        <button class="btn btn-link text-dark collapsed">Filter</button>
                                    </h5>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label for="urlpascrawl">URL Pas Crawl</label>
                                            <textarea id="urlpascrawl" rows="3" class="form-control no-spaces" name="urlpascrawl" id="urlpascrawl" placeholder="Please enter comma seperated urls: Eg. google.com, youtube.com"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h4 class="text-muted">Indexes</h4>
                            <hr class="my-3" />
                            <div class="card">
                                <div class="card-header bg-light p-0" id="headingThree">
                                    <h5 class="mb-0 w-100" style="cursor: pointer" data-toggle="collapse" data-target="#collapseThree"
                                        aria-expanded="false" aria-controls="collapseThree">
                                        <button class="btn btn-link text-dark collapsed">Indexes</button>
                                    </h5>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label>Fonctions <span class="text-danger">*</span></label>
                                                    <select class="form-control required select2" id="fonction" name="fonction" multiple>
                                                        <option value="en" disabled>--Select Fonctions--</option>
                                                        @foreach (App\Fonction::orderBy('desc_fr', 'ASC')->get() as $fonction)
                                                            <option value="{{ $fonction->code }}">{{ $fonction->desc_fr }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label>Secteurs <span class="text-danger">*</span></label>
                                                    <select class="form-control required select2" id="secteurs" name="secteurs" multiple>
                                                        <option value="en" disabled>--Select Secteurs--</option>
                                                        @foreach (App\Secteur::orderBy('desc_fr', 'ASC')->get() as $secteur)
                                                            <option value="{{ $secteur->code }}">{{ $secteur->desc_fr }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label>Themes <span class="text-danger">*</span></label>
                                                    <select class="form-control required select2" id="themes" name="themes" multiple>
                                                        <option value="en" disabled>--Select Themes--</option>
                                                        @foreach (App\Theme::orderBy('desc_fr', 'ASC')->get() as $theme)
                                                            <option value="{{ $theme->code }}">{{ $theme->desc_fr }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label>Type de info <span class="text-danger">*</span></label>
                                                    <select class="form-control required select2" id="typedeinfo" name="typedeinfo" multiple>
                                                        <option value="en" disabled>--Select Type de info--</option>
                                                        @foreach (App\TypeInfo::orderBy('desc_fr', 'ASC')->get() as $typeInfo)
                                                            <option value="{{ $typeInfo->code }}">{{ $typeInfo->desc_fr }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h4 class="text-muted">Xml</h4>
                            <hr class="my-3" />
                            <div class="card">
                                <div class="card-header bg-light p-0" id="headingFour">
                                    <h5 class="mb-0 w-100" style="cursor: pointer" data-toggle="collapse" data-target="#collapseFour"
                                        aria-expanded="false" aria-controls="collapseFour">
                                        <button class="btn btn-link text-dark collapsed">Xml</button>
                                    </h5>
                                </div>
                                <div id="collapseFour" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="form-group">
                                                    <label for="origine">Origine</label>
                                                    <input type="text" class="form-control no-spaces" name="origin" id="origin" placeholder="Origin" disabled>
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="form-group">
                                                    <label for="droit_access">Droit Access</label>
                                                    <input type="text" class="form-control no-spaces" name="droit_access" id="droit_access" placeholder="Droit Access">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="form-group">
                                                    <label for="encodage">Encodage</label>
                                                    <input type="text" class="form-control no-spaces" name="encodage" id="encodage" placeholder="Encodage">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="form-group">
                                                    <label for="origine_applicative">Origin Applicative</label>
                                                    <input type="text" class="form-control" name="origine_applicative" id="origine_applicative" placeholder="Origin Applicative">
                                                </div>
                                            </div>
                                            {{-- <div class="col-4">
                                                <div class="form-group">
                                                    <label for="origine_applicative">Origine Applicative</label>
                                                    <input type="text" class="form-control" name="origine_applicative" id="origine_applicative" placeholder="Origine Applicative">
                                                </div>
                                            </div> --}}
                                            <div class="col-4">
                                                <div class="form-group">
                                                    <label for="serveur">Serveur</label>
                                                    <input type="text" class="form-control no-spaces" name="serveur" id="serveur" placeholder="Serveur">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="form-group">
                                                    <label for="url">Url</label>
                                                    <input type="text" class="form-control no-spaces" name="url" id="url" placeholder="Url">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="form-group">
                                                    <label for="id_document">Id Document</label>
                                                    <input type="text" class="form-control no-spaces" name="id_document" id="id_document" placeholder="Id Document">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="form-group">
                                                    <label for="xmllangue">Langue <span class="text-danger">*</span></label>
                                                    <select class="form-control required select2" name="xmllangue" id="xmllangue" required>
                                                        <option value="" disabled>--Select Langue--</option>
                                                        @foreach (App\Langue::get() as $index => $langue)
                                                            <option value="{{ $langue->code }}" @if($index == 0) selected @endif>{{ $langue->description }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="form-group">
                                                    <label for="titre">Titre</label>
                                                    <input type="text" class="form-control no-spaces" name="titre" id="titre" placeholder="Titre">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="form-group">
                                                    <label for="km">Km (Key Metadata)</label>
                                                    <input type="text" class="form-control no-spaces" name="km" id="km" placeholder="Km">
                                                    <small class="text-danger">For multiple key metadata values, each path should be separated by a semicolon (;)</small>
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="form-group">
                                                    <label for="texte">Texte</label>
                                                    <input type="text" class="form-control no-spaces" name="texte" id="texte" placeholder="Texte">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="form-group">
                                                    <label for="long_doc">Long Doc</label>
                                                    <input type="text" class="form-control no-spaces" name="long_doc" id="long_doc" placeholder="Long Doc">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="form-group">
                                                    <label for="short_doc">Short Doc</label>
                                                    <input type="text" class="form-control no-spaces" name="short_doc" id="short_doc" placeholder="Short Doc">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="form-group">
                                                    <label for="id_pj_html">Id PJ HTML</label>
                                                    <input type="text" class="form-control no-spaces" name="id_pj_html" id="id_pj_html" placeholder="Id PJ HTML">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="form-group">
                                                    <label for="pj_audience_html">PJ Audience HTML</label>
                                                    <input type="text" class="form-control no-spaces" name="pj_audience_html" id="pj_audience_html" placeholder="PJ Audience HTML">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="form-group">
                                                    <label for="pj_text_html">PJ Text HTML</label>
                                                    <input type="text" class="form-control no-spaces" name="pj_text_html" id="pj_text_html" placeholder="PJ Text HTML">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="form-group">
                                                    <label for="id_pj_pdf">Id PJ PDF</label>
                                                    <input type="text" class="form-control no-spaces" name="id_pj_pdf" id="id_pj_pdf" placeholder="Id PJ PDF">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="form-group">
                                                    <label for="pj_audience_pdf">PJ Audience PDF</label>
                                                    <input type="text" class="form-control no-spaces" name="pj_audience_pdf" id="pj_audience_pdf" placeholder="PJ Audience PDF">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="form-group">
                                                    <label for="pj_text_pdf">PJ Text PDF</label>
                                                    <input type="text" class="form-control no-spaces" name="pj_text_pdf" id="pj_text_pdf" placeholder="PJ Text PDF">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="form-group">
                                                    <label for="pj_path">PJ Path</label>
                                                    <input type="text" class="form-control no-spaces" name="pj_path" id="pj_path" placeholder="PJ Path">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <button type="button" id="onSave" class="btn btn-info">Enregistrement</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End row-->
        </div>
    </div><!-- end app-content-->
@endsection


@section('scoped_js')
    <script>
        $(document).ready(function () {
            $('.select2').each(function (e) {
                $(this).select2();
            })
        });

        $('#originurl').on('input', function (e) {
            $('#origin').val($(this).val());
        });
        //
        // function validateRequiredFields() {
        //     $('.required').each(function () {
        //         if ($(this).val() == '' || $(this).val() == null) {
        //             $(this).css('box-shadow', '0 0 0 1px red');
        //             if($(this).siblings('label').text().indexOf('*') != -1 ){}
        //             else{
        //                 $(this).siblings('label').append(' <span class="text-danger">*</span>');
        //             }
        //             $('#onSave').attr('disabled', 'disabled');
        //         } else {
        //             $(this).css('box-shadow', 'none');
        //             $('#onSave').removeAttr('disabled');
        //         }
        //         console.log($(this).val())
        //     });
        // }
        // validateRequiredFields();

        // $('input').on('input', function () {
        //     validateRequiredFields();
        // });

        $('#onSave').click(function (e) {
            e.preventDefault();

            if($('#descfr').val() == null || $('#descfr').val() == ''){
                $('#descfr').css('box-shadow', '0 0 0 1px red');
                toastError('Desc FR is required!');
                return;
            } else {
                $('#descfr').css('box-shadow', 'none');
            }

            if($('#originurl').val() == null || $('#originurl').val() == ''){
                $('#originurl').css('box-shadow', '0 0 0 1px red');
                toastError('Origin URL is required!');
                return;
            } else {
                $('#originurl').css('box-shadow', 'none');
            }

            if($('#crawlingdepth').val() == null || $('#crawlingdepth').val().length <= 0){
                $('#crawlingdepth').css('box-shadow', '0 0 0 1px red');
                toastError('Crawling Depth is required!');
                return;
            } else {
                $('#crawlingdepth').css('box-shadow', 'none');
            }

            if($('#limitdedoc').val() == null || $('#limitdedoc').val().length <= 0){
                $('#limitdedoc').css('box-shadow', '0 0 0 1px red');
                toastError('Limiter le nombre de documents is required!');
                return;
            } else {
                $('#limitdedoc').css('box-shadow', 'none');
            }

            if($('#typededoc').val() == null || $('#typededoc').val().length <= 0){
                $('#typededoc').css('box-shadow', '0 0 0 1px red');
                toastError('Type de doc is required!');
                return;
            } else {
                $('#typededoc').css('box-shadow', 'none');
            }
            if($('#application').val() == null || $('#application').val().length <= 0){
                $('#application').css('box-shadow', '0 0 0 1px red');
                toastError('Application is required!');
                return;
            } else {
                $('#application').css('box-shadow', 'none');
            }

            if($('#authgroups').val() == null || $('#authgroups').val().length <= 0){
                $('#authgroups').css('box-shadow', '0 0 0 1px red');
                toastError('Auth Groups is required!');
                return;
            } else {
                $('#authgroups').css('box-shadow', 'none');
            }

            if($('#fonction').val() == null || $('#fonction').val().length <= 0){
                $('#fonction').css('box-shadow', '0 0 0 1px red');
                toastError('Fonctions is required!');
                return;
            } else {
                $('#fonction').css('box-shadow', 'none');
            }

            if($('#secteurs').val() == null || $('#secteurs').val().length <= 0){
                $('#secteurs').css('box-shadow', '0 0 0 1px red');
                toastError('Secteurs is required!');
                return;
            } else {
                $('#secteurs').css('box-shadow', 'none');
            }

            if($('#themes').val() == null || $('#themes').val().length <= 0){
                $('#themes').css('box-shadow', '0 0 0 1px red');
                toastError('Themes is required!');
                return;
            } else {
                $('#themes').css('box-shadow', 'none');
            }

            if($('#typedeinfo').val() == null || $('#typedeinfo').val().length <= 0){
                $('#typedeinfo').css('box-shadow', '0 0 0 1px red');
                toastError('Themes is required!');
                return;
            } else {
                $('#typedeinfo').css('box-shadow', 'none');
            }
            if($('#xmllangue').val() == null || $('#xmllangue').val().length <= 0){
                $('#xmllangue').css('box-shadow', '0 0 0 1px red');
                toastError('XML Langue is required!');
                return;
            } else {
                $('#xmllangue').css('box-shadow', 'none');
            }

            saveOrigin();
        });


        function saveOrigin() {

            let data = {};
            data.code = $('#code').val();
            data.descfr = $('#descfr').val();
            data.descen = $('#descen').val();
            data.origin_url = $('#originurl').val();
            data.a_indexer = $('#a_indexer').is(":checked") ? 1 : 0;
            data.langue = $('#langue').val();
            data.crawlingdepth = $('#crawlingdepth').val();
            data.limitdedoc = $('#limitdedoc').val();
            data.typededoc = $('#typededoc').val();
            data.application = $('#application').val();
            data.authgroups = $('#authgroups').val();

            data.fonction = $('#fonction').val();
            data.secteurs = $('#secteurs').val();
            data.themes = $('#themes').val();
            data.type_info = $('#typedeinfo').val();

            axios.post("{{ route('admin.parameter.origin-store') }}", data)
                .then(res => {
                    if (res.status === 200 || res.status === 201) {
                        saveFilter(res.data.code);
                        saveXml(res.data.code);
                        toastSuccess('Origin, Filter, Indexes & XML Created Successfully !');
                        setTimeout(function () {
                            window.location.reload();
                        }, 1500);
                    } else {
                        setTimeout(function () {
                            toastError('There was an error while creating Origin !');
                        }, 1000);
                    }

                })
                .catch(function (err) {
                    setTimeout(function () {
                        toastError(err.response.data.message);
                    }, 500);
                });

        }

        function saveFilter(origin_code) {
            let data = {};
            data.origin_code = origin_code;
            data.URL_Pas_Crawl = $('#urlpascrawl').val();

            axios.post("{{ route('admin.parameter.origin-filter-store') }}", data)
                .then(res => {
                    //
                })
                .catch(err => console.error(err));

        }

        function saveXml(origin_code) {

            let data = {};
            data.origin = $('#origin').val();
            data.droit_access = $('#droit_access').val();
            data.encodage = $('#encodage').val();
            data.origine_applicative = $('#origine_applicative').val();
            data.origine_code = origin_code;
            data.serveur = $('#serveur').val();
            data.url = $('#url').val();
            data.id_document = $('#id_document').val();
            data.xmllangue = $('#xmllangue').val();
            data.titre = $('#titre').val();
            data.km = $('#km').val();
            data.texte = $('#texte').val();
            data.long_doc = $('#long_doc').val();
            data.short_doc = $('#short_doc').val();
            data.id_pj_html = $('#id_pj_html').val();
            data.pj_audience_html = $('#pj_audience_html').val();
            data.pj_text_html = $('#pj_text_html').val();
            data.id_pj_pdf = $('#id_pj_pdf').val();
            data.pj_audience_pdf = $('#pj_audience_pdf').val();
            data.pj_text_pdf = $('#pj_text_pdf').val();
            data.pj_path = $('#pj_path').val();

            axios.post("{{ route('admin.parameter.origin-xml-store') }}", data)
                .then(res => {
                    //
                })
                .catch(err => console.error(err));

        }

        $('.no-spaces').on('input', function () {
            var sanitizedValue = $(this).val().replace(/\s+/g, '');
            $(this).val(sanitizedValue);
        });

    </script>
@endsection

