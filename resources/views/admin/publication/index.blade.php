@extends('layouts.admin.master')

@section('title', 'Publication')

@section('scoped_css')
    <style>

    </style>
@endsection

@section('content')
    <div class="app-content">
        <div class="side-app">

            <!--Page header-->
            <div class="page-header">
                <div class="page-leftheader">
                    <h4 class="page-title">Publication</h4>
                    <ol class="breadcrumb pl-0">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Publication</li>
                    </ol>
                </div>
            </div>
            <!--End Page header-->


            <!--Row-->
            <div class="row">
                <div class="col-md-12">
                    <h4 class="p-0 m-0 text-muted">Publication</h4>
                    <hr class="my-3">
                    {{-- Tab 1 --}}
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title mr-auto">Ci-dessous la liste des Publications faites précédemment..</h3>
                            {{-- <button class="btn btn-info"><i class="fa fa-plus"></i> Add New</button> --}}
                        </div>
                        <div class="card-body">

                            <table id="dataTable" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Origine</th>
                                    <th>Applications</th>
                                    <th>Operations</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>sddkj</td>
                                    <td>jas</td>
                                    <td> sdja</td>
                                    <td>
                                        <button class="btn btn-info"><i class="fa fa-edit"></i> Edit</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>sddkj</td>
                                    <td>jas</td>
                                    <td> sdja</td>
                                    <td>
                                        <button class="btn btn-info"><i class="fa fa-edit"></i> Edit</button>
                                    </td>
                                </tr>
                            </table>

                        </div>
                    </div>


                </div>

            </div>
            <!--End row-->


        </div>
    </div><!-- end app-content-->
@endsection


@section('scoped_js')
    <script>
        $('#dataTable').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
        });
    </script>
@endsection

