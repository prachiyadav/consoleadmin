@extends('layouts.admin.master')

@section('title', 'Crawler\'s Status')

@section('scoped_css')
    <style>
        th, *{
            text-transform: none !important;
        }
        .pagination {
            margin-bottom: 0
        }
    </style>
@endsection

@section('content')
    <div class="app-content">
        <div class="side-app">

            <!--Page header-->
            <div class="page-header">
                <div class="page-leftheader">
                    <h4 class="page-title">Crawlers Status</h4>
                    <ol class="breadcrumb pl-0">
                        <li class="breadcrumb-item"><a href="{{ route('admin.command.index') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.command.index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.command.index') }}">Command</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Crawlers Status</li>
                    </ol>
                </div>
            </div>
            <!--End Page header-->


            <!--Row-->
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header bg-light d-flex justify-content-between align-items-center">
                            <h3 class="card-title mr-auto p-3">Crawlers Status.</h3>
                            @if (!empty($jobs))
                                {{ $jobs->links() }}
                            @endif
                        </div>
                        <div class="card-body p-0">
                            @if (!empty($jobs) && count($jobs) > 0)
                                <table id="dataTable" class="table table-bordered table-hover">
                                    <thead> 
                                        <tr class="bg-light">
                                            <th>Origin Name </th>
                                            <th>Status</th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($jobs as $index=>$job)
                                            @if ($payload = json_decode($job->payload))
                                                @if (property_exists($payload, 'nom'))
                                                    @php
                                                        if ($job->state == 'success') {
                                                            $class = 'bg-success';
                                                        } else {
                                                            $class = 'bg-warning';
                                                        }
                                                    @endphp
                                                    <tr>
                                                        <td>{{ json_decode($job->payload)->nom }}</td>
                                                        <td class="{{ $class }}">{{ $job->state }}</td>
                                                        <td>{{ Carbon\Carbon::parse($job->created_at)->format('d-m-Y H:i:s') }}</td>
                                                        <td>{{ Carbon\Carbon::parse($job->finished_at)->format('d-m-Y H:i:s') }}</td>
                                                        
                                                    </tr>
                                                @endif
                                            @endif
                                        @empty
                                            <p>No data</p>
                                        @endforelse
                                    </tbody>
                                    {{-- <tfoot>
                                        <tr>
                                            <td colspan="4">
                                                
                                            </td>
                                        </tr>
                                    </tfoot> --}}
                                </table>
                                
                            @else
                                <p class="text-center h4">No data</p>
                            @endif
                        </div>
                    </div>
                    
                </div>

            </div>
            <!--End row-->


        </div>
    </div><!-- end app-content-->
@endsection


@section('scoped_js')
    <script>
        $('.datatable').each(function () {
            $(this).DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
            });
        });
    </script>
@endsection

