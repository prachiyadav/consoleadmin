@extends('layouts.admin.master')

@section('title', 'Dashboard - Command')

@section('scoped_css')
    <style>
        .side-menu .slide .slide-menu .slide-item:hover{
            color: #fff !important;
            /*background: #0b7ec4;*/
        }
        .side-menu .side-menu__item:hover .side-menu__label, .side-menu .side-menu__item:hover .side-menu__icon{
            color: #fff !important;
        }
        .select2 {
            width: 100% !important;
        }
    </style>
@endsection

@php

    $originNoms = [];
    $originOptions = [];
    foreach (App\Origin::get() as $index => $origine){
        array_push($originNoms, $origine->desc_fr);
        array_push($originOptions, $origine->desc_fr);
    }

    $collectes = App\Collecte::whereNotIn('nom', $originNoms)->where(function($query){
        $query->where('nom', '!=', null);
    })->get();

    foreach ($collectes as $index => $collecte){
        array_push($originOptions, 'collecte-'.$collecte->nom);
    }

@endphp

@section('content')
    <div class="app-content">
        <div class="side-app">

            <!--Page header-->
            <div class="page-header">
                <div class="page-leftheader">
                    <h4 class="page-title">Commandes</h4>
                    <ol class="breadcrumb pl-0">
                        <li class="breadcrumb-item"><a href="{{ route('admin.command.index') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.command.index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Commandes</li>
                    </ol>
                </div>
            </div>
            <!--End Page header-->


            <!--Row-->
            <div class="row">
                <div class="col-12">

                    <div id="accordion">

                        <h4 class="text-muted">Crawler</h4>

                        <hr class="my-3">
                        <div class="card">
                            <div class="card-header bg-light p-0" id="headingOne">
                                <h5 class="mb-0 w-100" style="cursor: pointer" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <button class="btn bg-transparent text-dark">
                                        Crawler
                                    </button>
                                </h5>
                            </div>

                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">
                                    <div class="row align-items-center">
                                        <div class="col-lg-8">
                                            <form action="">
                                                <div class="form-group">
                                                    <label for="crawlerInput">Sélectionner les origines</label>
                                                    <select name="crawler" id="crawlerInput" class="form-control select2">
                                                        {{-- @foreach (App\Collecte::get() as $index => $collecte)
                                                            <option value="{{ $collecte->id }}">{{ $collecte->nom }}</option>
                                                        @endforeach --}}
                                                        <option value="" selected disabled>--Sélectionner les origines--</option>
                                                        @foreach ($originOptions as $index => $origine)
                                                            @php
                                                                if (substr($origine, 0, 9) == 'collecte-') {
                                                                    $origineLabel = substr($origine, 9, strlen($origine));
                                                                } else {
                                                                    $origineLabel = $origine;
                                                                }
                                                            @endphp
                                                            <option value="{{ $origine }}">{{ $origineLabel }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-lg-4 d-flex flex-lg-column">
                                            <button class="btn btn-info mb-2 w-50 mr-1 loadingTrigger" id="crawlerLancementBtn">Lancement</button>
                                            <a href="#" id="crawlerEditBtn" class="btn btn-info mb-2 w-50">Edition</a>
                                            <a href="{{ route('admin.command.crawler-status') }}" id="crawlerStatusBtn" class="btn btn-info mb-2 w-50">Statut</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h4 class="text-muted">Parser</h4>
                        <hr class="my-3">
                        <div class="card">
                            <div class="card-header bg-light p-0" id="headingTwo">
                                <h5 class="mb-0 w-100" style="cursor: pointer" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    <button class="btn btn-link text-dark collapsed">
                                        Parser
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                <div class="card-body">

                                    <div class="row align-items-center">
                                        <div class="col-lg-8">
                                            <form action="">
                                                <div class="form-group">

                                                    <label for="">Sélectionner les origines</label>
                                                    <select name="parser" id="parserInput" class="form-control select2">
                                                        {{-- @foreach (App\Indexationengine::get() as $index => $parser)
                                                            <option value="{{ $parser->id }}">{{ $parser->origine }}</option>
                                                        @endforeach --}}
                                                        <option value="" selected disabled>--Sélectionner les origines--</option>
                                                        @foreach (App\Origin::where('code', 'like', 'O-__')->get() as $index => $origine)
                                                            <option value="{{ $origine->code }}">{{ $origine->desc_fr }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-lg-4 d-flex flex-lg-column">
                                            {{-- <button class="btn btn-info mb-2 w-50 mr-1">Lancement</button> --}}
                                            <a href="#" id="parserEditBtn" class="btn btn-info mt-3 w-50">Edition</a>
                                        </div>
                                    </div>

                                    <div class="row align-items-center mt-4">
                                        <div class="col-lg-8">
                                            <form action="">
                                                <div class="form-group">

                                                    <label for="">Sélectionner les collecte</label>
                                                    <select name="parser" id="parserInputAPI" class="form-control select2" multiple>
                                                        @foreach (App\Collecte::where('nom', '!=', null)->get() as $index => $collecte)
                                                            <option value="{{ $collecte->id }}">{{ $collecte->nom }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-lg-4 d-flex flex-lg-column">
                                            <button class="btn btn-info mt-3 w-50 mr-1 loadingTrigger" type="button" id="parserLancementBtn">Lancement</button>
                                            <a href="{{ route('admin.command.parser-status') }}" id="parserStatusBtn" class="btn btn-info mt-3 mb-2 w-50">Statut</a>
                                            {{-- <a href="#" id="parserEditBtn" class="btn btn-info mt-3 w-50">Edition</a> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h4 class="text-muted">Mise à jour applications</h4>
                        <hr class="my-3">
                        <div class="card">
                            <div class="card-header bg-light p-0" id="headingThree">
                                <h5 class="mb-0 w-100" style="cursor: pointer" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    <button class="btn btn-link text-dark collapsed">
                                        Mise à jour applications
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                <div class="card-body">
                                    <div class="row align-items-center">
                                        <div class="col-lg-8">
                                            <form action="">
                                                <div class="form-group">
                                                    <label for="">Sélection des mise à jour applications</label>
                                                    <select name="application" id="application" class="form-control select2">
                                                        <option value="" selected disabled>--Select applications--</option>
                                                        @foreach (App\Application::get() as $index => $application)
                                                            <option value="{{ $application->solrcore_url . '^^' . $application->procedure . '^^' . $application->name . '^^' . $application->action }}">{{ $application->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-lg-4">
                                            <a href="#" class="btn btn-info mb-2 w-50 loadingTrigger" id="biEngine">Lancement</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <h4 class="text-muted">Publication</h4>
                        <hr class="my-3">
                        <div class="card">
                            <div class="card-header bg-light p-0" id="headingFour">
                                <h5 class="mb-0 w-100" style="cursor: pointer" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    <button class="btn btn-link text-dark collapsed">
                                        Publication
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseFour" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                <div class="card-body">
                                    <div class="row align-items-center">
                                        <div class="col-lg-8">
                                            <form action="">
                                                <div class="form-group">

                                                    <label for="">Sélectionner les origines</label>
                                                    <select name="publication" id="publicationInput" class="form-control select2">
                                                        {{-- @foreach (App\Indexationengine::get() as $index => $parser)
                                                            <option value="{{ $parser->id }}">{{ $parser->origine }}</option>
                                                        @endforeach --}}
                                                        <option value="" selected disabled>--Sélectionner les origines--</option>
                                                        @foreach (App\Origin::where('code', 'like', 'O-__')->get() as $index => $origine)
                                                            <option value="{{ $origine->code }}">{{ $origine->desc_fr }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-lg-4 d-flex flex-lg-column">
                                            {{-- <button class="btn btn-info mb-2 w-50 mr-1">Lancement</button> --}}
                                            <a href="#" id="publicationEditBtn" class="btn btn-info mb-2 w-50" style="margin-top: 20px">Edition</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                   </div>
                </div>
            </div>
            <!--End row-->


        </div>
    </div><!-- end app-content-->



    <!-- Modal -->
    <div class="modal fade" id="editPublicationFormModal" tabindex="-1" role="dialog" aria-labelledby="editPublicationFormModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">

        <div class="modal-content">
            <form action="{{ route('admin.publication.update') }}" method="POST">
                @csrf
                <input type="hidden" name="id" id="publicationIdInput">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Edit Publication</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <input type="text" class="form-control" name="sku" id="publicationSkuInput" placeholder="SKU">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="origine" id="publicationOrigineInput" placeholder="origine">
                    </div>
                    <div class="form-group">
                        <label>
                            <input type="checkbox" name="publicated" id="publicationPublicatedInput" class="mr-2" value="1">
                            Publicated
                        </label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
    </div>


@endsection


@section('scoped_js')
    <script>
        $(document).ready(function () {
            $('#crawlerInput, #demo2, #demo3').select2({placeholder: 'Choisir les crawlers'});

            $('.select2').each(function (e) {
                $(this).select2();
            })

            $('#dataTable').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
            });

            // Crawler Select
            $('#crawlerEditBtn').attr("href", "#");
            $('#crawlerInput').on('change', function (e) {
                let id = $('#crawlerInput').val();
                let type = $(this).val();

                if (type.substring(0, 9) == 'collecte-') {
                    $('#crawlerEditBtn').attr("disabled", "disabled").addClass('disabled');
                } else if(type.substring(0, 9) != 'collecte-') {
                    $('#crawlerEditBtn').attr("href", "crawler-edit/"+id).removeAttr("disabled").removeClass('disabled');
                }



                // $('#crawlerStatusBtn').attr("href", "crawler-status/"+id).removeAttr("disabled").removeClass('disabled');



                // if (type.indexOf("collecte-") >= 0) {
                //     alert('Collecte');
                // } else if(type.indexOf("collecte-") < 0) {
                //     alert('Origine');
                // }

                // $('#crawlerEditBtn').attr("href", "crawler-edit/"+id);
            });

            $('#crawlerLancementBtn').on('click', function () {
                var nom = $('#crawlerInput').val();

                if (nom.substring(0, 9) == 'collecte-') {
                    nom = nom.substring(9, nom.length);
                }

                var data = {
                    "nom": nom
                }

                if (nom != "" || nom != null) {
                    // $.post("{{ env('CRAWLER_API_ENDPOINT') }}", JSON.stringify(data), function (response) {
                    //     $('#crawlerLancementBtn').html('Lancement').removeAttr('disabled');
                    //     Swal.fire('Job is queued and will be executed shortly !');
                    // }).fail(function(err, status) {
                    //     $('#crawlerLancementBtn').html('Lancement').removeAttr('disabled');
                    //     Swal.fire('Error !', 'There was an error while fetching API.', 'error');
                    // });

                    $.ajax("{{ config('app.crawler_api_url') }}", {
                        data: JSON.stringify(data),
                        type: 'POST',
                        processData: false,
                        contentType: 'application/json',
                        dataType: 'json',
                        success: function(response){
                            if (response) {
                                $('#crawlerLancementBtn').html('Lancement').removeAttr('disabled');
                                Swal.fire('Job is queued and will be executed shortly !');
                            }
                        },
                        error: function(err, status){
                            $('#crawlerLancementBtn').html('Lancement').removeAttr('disabled');
                            Swal.fire('Error !', 'There was an error while fetching API.', 'error');
                        }
                    });

                }

            });




            $('#parserEditBtn').attr("href", "#");
            $('#parserInput').change(function (e) {
                let id = $('#parserInput').val();
                console.log(id);
                $('#parserEditBtn').attr("href", "parser-list/"+id);
            });

            $('#parserLancementBtn').on('click', function (e) {
                var collecteIds = $('#parserInputAPI').val();
                var collecteIdsAPI = '';
                if (collecteIds.length == 1) {
                    collecteIdsAPI = collecteIds[0];
                } else {
                    collecteIds.forEach((collecteId, index) => {
                        if (index == collecteIds.length - 1) {
                            collecteIdsAPI += collecteId;
                        } else {
                            collecteIdsAPI += collecteId+',';
                        }
                    });
                }

                $.post("{{ route('check-noms-in-jobs') }}", {_token: "{{ csrf_token() }}", collecteIds: collecteIdsAPI}, function (response) {
                    // console.log(response);
                    var collectePassedIds = '';
                    var collecteFailedNoms = '';
                    response.passed.forEach((passedId, index) => {
                        if (index == response.passed.length - 1) {
                            collectePassedIds += passedId.id;
                        } else {
                            collectePassedIds += passedId.id+',';
                        }
                    });
                    response.failed.forEach((failedNom, index) => {
                        if (index == response.failed.length - 1) {
                            collecteFailedNoms += failedNom;
                        } else {
                            collecteFailedNoms += failedNom+',';
                        }
                    });



                    $.get("{{ config('app.parser_api_url') }}/"+collectePassedIds, {}, function (response, status) {
                        $('#parserLancementBtn').html('Lancement').removeAttr('disabled');
                        // Swal.fire('Success', 'Job is queued and will be executed shortly <br> but <b>'+collecteFailedNoms+'</b> not found !', 'success');
                    }).fail(function(err, status) {
                        $('#parserLancementBtn').html('Lancement').removeAttr('disabled');
                        // Swal.fire('Error !', 'There was an error while fetching API ! <br> And <b>'+collecteFailedNoms+'</b> not found.', 'error');
                    });

                    setTimeout(function () {
                        $('#parserLancementBtn').html('Lancement').removeAttr('disabled');
                        Swal.fire('Success', 'Job is sent to API and queued and will be executed shortly !');
                    }, 5000); // 5 Seconds

                    // console.log(collecteFailedNoms);
                    // console.log(collectePassedIds);

                })


                // if (collecteIdsAPI != "" || collecteIdsAPI != null) {
                //     $.get("{{ env('PARSER_API_ENDPOINT') }}/"+collecteIdsAPI, {}, function (response, status) {
                //         $('#parserLancementBtn').html('Lancement').removeAttr('disabled');
                //         Swal.fire('Job is queued and will be executed shortly !');
                //     }).fail(function(err, status) {
                //         $('#parserLancementBtn').html('Lancement').removeAttr('disabled');
                //         Swal.fire('Error !', 'There was an error while fetching API.', 'error');
                //     });
                // }

            });



            $('#publicationEditBtn').attr("href", "#");
            $('#publicationInput').change(function (e) {
                let id = $('#publicationInput').val();
                $('#publicationEditBtn').attr("href", "publication-list/"+id);
            });

            $('.editPublication').each(function () {

                $(this).on('click', function (e) {
                    e.preventDefault();

                    var id = $(this).data('id');
                    var sku = $(this).data('sku');
                    var origine = $(this).data('origine');
                    var publicated = $(this).data('publicated');

                    $('#publicationIdInput').val(id);
                    $('#publicationSkuInput').val(sku);
                    $('#publicationOrigineInput').val(origine);
                    if (publicated == 1) {
                        $('#publicationPublicatedInput').attr('checked', 'checked');
                    }
                    $('#editPublicationFormModal').modal('show');
                });

            });

            $('.deletePublication').each(function () {
                $(this).on('click', function (e) {
                    e.preventDefault();
                    var id = $(this).data('id');

                    if (confirm('Are you sure you want to delete this record ?')) {
                        window.open("{{ url('admin/publication-delete') }}/"+id, "_self");
                    }

                });
            });



            var el = '<button class="btn btn-success mr-2">Export Data To CSV</button>';

            $('#dataTable_filter').prepend(el);


            {{--$(document).on('click', '#updateLogSearch', function (e) {--}}
            {{--    e.preventDefault();--}}
            {{--    $.ajax({--}}
            {{--        url: "{{ route('admin.import.log') }}",--}}
            {{--        method: "GET",--}}
            {{--        success: function (res) {--}}
            {{--            $('#updateLogSearch').html('Mise à jour').removeAttr('disabled');--}}
            {{--            Swal.fire('Success', res.message);--}}
            {{--        }--}}
            {{--    });--}}
            {{--});--}}

            $(document).on('click', '#biEngine', function (e) {
                e.preventDefault();
                var val = $('#application').val();
                var url = val.split('^^')[0];
                var procedure = val.split('^^')[1];
                var applicationName = val.split('^^')[2];
                var action = val.split('^^')[3];

                if (1 == procedure){
                    $.ajax({
                        url: "{{ config('app.update_solr_url') }}?application=" + applicationName + "&url=" + url + 'dataimport?command=full-import',
                        method: "GET",
                        success: function (res) {}
                    });
                    // setTimeout(function () {
                    //     $.ajax({
                    //         url: url + 'dataimport?command=full-import',
                    //         method: "GET",
                    //         success: function (res) {}
                    //     });
                    // }, 3000);
                    setTimeout(function () {
                        $('#biEngine').html('Lancement').removeAttr('disabled');
                        Swal.fire('Success', applicationName + ' will be updated soon');
                    }, 5000);
                } else if('log_import' == action){
                    $.ajax({
                        url: "{{ route('admin.import.log') }}",
                        method: "GET",
                        success: function (res) {
                            $('#biEngine').html('Lancement').removeAttr('disabled');
                            Swal.fire('Success', res.message);
                        }
                    });
                } else {
                    $.ajax({
                        url: url + 'dataimport?command=full-import',
                        method: "GET",
                        success: function (res) {}
                    });

                    setTimeout(function () {
                        $('#biEngine').html('Lancement').removeAttr('disabled');
                        Swal.fire('Success', applicationName + ' will be updated soon');
                    }, 5000);
                }

            })
        });

    </script>
@endsection

