@extends('layouts.admin.master')

@section('title', 'Edit Crawlers')

@section('scoped_css')
    <style>
        th, *{
            text-transform: none !important;
        }
    </style>
@endsection

@section('content')
    <div class="app-content">
        <div class="side-app">

            <!--Page header-->
            <div class="page-header">
                <div class="page-leftheader">
                    <h4 class="page-title">Edit Crawlers</h4>
                    <ol class="breadcrumb pl-0">
                        <li class="breadcrumb-item"><a href="{{ route('admin.command.index') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.command.index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.command.index') }}">Command</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Edit Crawlers</li>
                    </ol>
                </div>
            </div>
            <!--End Page header-->


            <!--Row-->
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <h4 class="p-0 m-0 text-muted">Origine sélectionnée: ({{ $desc_fr ?? 'Not found' }})</h4>
                    <hr class="my-3">
                    
                    {{-- Tab 1 --}}
                    <div class="card">
                        <div class="card-header bg-light">
                            <h3 class="card-title mr-auto">Edition.</h3>
                            {{-- <button class="btn btn-info"><i class="fa fa-plus"></i> Créer</button> --}}
                        </div>
                        <div class="card-body">

                            @if (!empty($collecte) && count($collecte) > 0)
                                <form method="POST" action="{{ route('admin.command.crawler-update') }}">
                                    @csrf
                                    <input type="hidden" name="id" value="{{ $collecte[0]->id }}">
                                    <div class="row">

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="nom">Nom</label>
                                                <input type="text" id="nom" name="nom" class="form-control" value="{{ $collecte[0]->nom }}">
                                            </div>
                                        </div>
                                        {{-- <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="status">Status</label>
                                                <input type="text" id="status" name="status" class="form-control" value="{{ $collecte[0]->status }}">
                                            </div>
                                        </div>  --}}
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="urls">Urls</label>
                                                <textarea class="form-control" id="urls" name="urls" rows="3">@foreach (json_decode($collecte[0]->urls) as $item){{ count(json_decode($collecte[0]->urls)) > 1 ?  $item.', ' : $item }}@endforeach</textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-info  float-right">Enregistrement</button>
                                                <a href="{{ url('admin/crawler-result/'.$collecte[0]->id) }}" class="btn btn-info">Edit Result</a>
                                            </div>
                                        </div>

                                    </div>
                                </form>

                            @else
                                <p class="text-center h4">No data</p>
                            @endif

                        
                        </div>
                    </div>
                    
                </div>

            </div>
            <!--End row-->


        </div>
    </div><!-- end app-content-->
@endsection


@section('scoped_js')
    <script>
        $('.datatable').each(function () {
            $(this).DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
            });
        });


    </script>
@endsection

