@extends('layouts.admin.master')

@section('title', 'Publication Result')

@section('scoped_css')
    <style>
        th, *{
            text-transform: none !important;
        }
        .publishunpublish {
            transform: scale(1.75)
        }
    </style>
@endsection

@section('content')
    <div class="app-content">
        <div class="side-app">

<!--Page header-->
<div class="page-header">
    <div class="page-leftheader">
        <h4 class="page-title">Publication Result</h4>
        <ol class="breadcrumb pl-0">
            <li class="breadcrumb-item"><a href="{{ route('admin.command.index') }}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ route('admin.command.index') }}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ route('admin.command.index') }}">Command</a></li>
            <li class="breadcrumb-item active" aria-current="page">Publication Result</li>
        </ol>
    </div>
</div>
<!--End Page header-->


            <!--Row-->
            <div class="row">
                <div class="col-md-12">
                    <h4 class="p-0 m-0 text-muted">Origine sélectionnée: ({{ $originName }})</h4>
                    <hr class="my-3">
                    {{-- Tab 1 --}}
                    <div class="card">
                        <div class="card-header bg-light">
                            <h3 class="card-title mr-auto">Results.</h3>
                            {{-- <small><b>Note:</b> Please click on publicated button to unpublish and unpublicated button to publish</small> --}}
                            {{-- <button class="btn btn-info"><i class="fa fa-plus"></i> Créer</button> --}}
                        </div>
                        <div class="card-body">
                            @if (!empty($publications) && count($publications) > 0)
                            <table id="dataTable" class="table table-bordered table-hover" style="height: 52vh !important; display:block; width: 100%; overflow-y: scroll;">
                                <thead> 
                                    <tr>
                                        <th>#</th>
                                        <th>Origine</th>
                                        <th>SKU</th>
                                        <th>Publicated</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($publications as $index=>$publication)
                                            <tr>
                                                <td>{{ ++$index }}</td>
                                                <td>{{ $publication->origine }}</td>
                                                <td>{{ $publication->sku }}</td>
                                                <td>
                                                    <input type="checkbox" class="publishunpublish" name="publicated" value="{{ $publication->id }}" @if ($publication->publicated) checked @endif>      
                                                </td>
                                               
                                            </tr>
                                    @empty
                                        <p>No data</p>
                                    @endforelse
                                </tbody>
                            </table>
                            @else
                                <p class="text-center h4">No data</p>
                            @endif
                        </div>
                    </div>


                </div>

            </div>
            <!--End row-->


        </div>
    </div><!-- end app-content-->
@endsection


@section('scoped_js')
    <script>
        $('.dataTable').each(function () {
            $(this).DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
            });
        });

        // $('.deleteParser').each(function(){
        //     $(this).on('click', function(e){
        //         if (confirm('Are you sure you want to delete this record?')) {
        //             var id = $(this).data('id');

        //             window.open("{{ url('admin/parser-delete') }}/"+id, "_self");
                    
        //         }
        //     });
        // });

        $('.deleteParser').each(function () {
            var parserId = $(this).data('id');
            $(this).click(function (e) {
                e.preventDefault();
                if (confirm('Are you sure you want to delete this record ?')) {
                    $.get("{{ url('admin/parser-delete') }}/"+parserId, {}, function (response) {
                        Swal.fire('Success', response, 'success');
                        setTimeout(function () {
                            window.location.reload();
                        }, 1500);
                    });
                }
            });
        });


        $('.publishunpublish').each(function () {
            $(this).on('change', function (e) {
                e.preventDefault();
                let id = $(this).val();
                $.get("{{ url('admin/publication-publishunpublish') }}", {id:id}, function (response) {
                    console.log(response);


                    Swal.fire({
                        position: 'top-end',
                        toast: true,
                        icon: 'success',
                        text: response,
                        showConfirmButton: false,
                        timer: 2000
                    })


                    // Swal.fire('Success', response, 'success');
                    // setTimeout(function () {
                    //     // window.location.reload();
                    // }, 1500);
                })
            });
        })

        $('.publish').each(function () {
            $(this).on('click', function (e) {
                e.preventDefault();
                let id = $(this).data('id');
                $.get("{{ url('admin/publication-publish') }}/"+id, {}, function (response) {
                    Swal.fire('Success', response, 'success');
                    setTimeout(function () {
                        window.location.reload();
                    }, 1500);
                })
            });
        })

        $('.unpublish').each(function () {
            $(this).on('click', function (e) {
                e.preventDefault();
                let id = $(this).data('id');
                $.get("{{ url('admin/publication-unpublish') }}/"+id, {}, function (response) {
                    Swal.fire('Success', response, 'success');
                    setTimeout(function () {
                        window.location.reload();
                    }, 1500);
                })
            });
        })

    </script>
@endsection

