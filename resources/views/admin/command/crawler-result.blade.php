@extends('layouts.admin.master')

@section('title', 'Crawler Result')

@section('scoped_css')
	<style>
		th, * {
			text-transform: none !important;
		}
	</style>
@endsection

@section('content')
	<div class="app-content">
		<div class="side-app">

			<!--Page header-->
			<div class="page-header">
				<div class="page-leftheader">
					<h4 class="page-title">Crawlers Result</h4>
					<ol class="breadcrumb pl-0">
						<li class="breadcrumb-item"><a href="{{ route('admin.command.index') }}">Home</a></li>
						<li class="breadcrumb-item"><a href="{{ route('admin.command.index') }}">Dashboard</a></li>
						<li class="breadcrumb-item"><a href="{{ route('admin.command.index') }}">Command</a></li>
						<li class="breadcrumb-item active" aria-current="page">Crawlers Result</li>
					</ol>
				</div>
			</div>
			<!--End Page header-->


			<!--Row-->
			<div class="row">
				<div class="col-md-12">
					<h4 class="p-0 m-0 text-muted">Crawlers Result</h4>
					<hr class="my-3">
					{{-- Tab 1 --}}
					<div class="card">
						<div class="card-header bg-light">
							<h3 class="card-title mr-auto">Results.</h3>
							{{-- <button class="btn btn-info"><i class="fa fa-plus"></i> Créer</button> --}}
						</div>
						<div class="card-body" style="overflow-x: scroll">
							<table id="dataTable" class="table table-bordered table-hover dataTable">
								<thead>
								<tr>
									<th class="d-none">#</th>
									<th style="max-width: 350px">Url</th>
									<th>Fonctions</th>
									<th>Secteurs</th>
									<th>Themes</th>
									<th>Typed Info</th>
									<th>Lang</th>
									<th>Type</th>
									{{-- <th>Counter</th> --}}
									<th>Action</th>
								</tr>
								</thead>
								<tbody>
								@if ($results)
									@forelse ($results as $index => $result)
										<tr>
											<td class="d-none">{{ $index }}</td>
											<td style="max-width: 350px">{{ $result->url }}</td>
											<td>
												@foreach ($result->fonctions as $fonctions)
													{{ $fonctions.', ' }}
												@endforeach
											</td>
											<td>
												@foreach ($result->secteurs as $secteurs)
													{{ $secteurs.', ' }}
												@endforeach
											</td>
											<td>
												@foreach ($result->themes as $theme)
													{{ $theme.', ' }}
												@endforeach
											</td>
											<td>
												@foreach ($result->types_infos as $types_infos)
													{{ $types_infos.', ' }}
												@endforeach
											</td>
											<td>{{ $result->Lang }}</td>
											<td>{{ $result->Type }}</td>

											{{--											<td>{{ $result->counter }}</td>--}}

											<td>
												<a href="{{ url('admin/crawler-result-edit/'.$id.'/'.$index) }}"
												   class="p-1"><i class="fa fa-edit"></i></a>
												<a href="javascript:void(0);" class="p-1 text-danger deleteResult"
												   data-id="{{ $id }}" data-index="{{ $index }}"
												   data-form="deleteResultForm{{ $index }}"><i class="fa fa-trash"></i></a>

											</td>

										</tr>
									@empty
										<p>No data</p>
									@endforelse
								@endif

								</tbody>
							</table>
						</div>
					</div>


				</div>

			</div>
			<!--End row-->


		</div>
	</div><!-- end app-content-->
@endsection


@section('scoped_js')
	<script>
        $('.dataTable').each(function () {
            $(this).DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
            });
        });


        {{--document.querySelectorAll('.deleteResult').forEach(function (btn) {--}}
        {{--    btn.addEventListener('click', function (e) {--}}
        {{--        if (confirm('Are you sure you want to delete this record?')) {--}}
        {{--            var id = btn.dataset.id;--}}
        {{--            var index = btn.dataset.index;--}}
        {{--            // var form = btn.dataset.form;--}}
        {{--            window.open("{{ url('admin/crawler-result-delete') }}/" + id + "/" + index, "_self");--}}
        {{--        }--}}
        {{--    });--}}
        {{--});--}}


        $(document).on('click', '.deleteResult', function () {
            if (confirm('Are you sure you want to delete this record?')) {
                var id = $(this).data('id');
                var index = $(this).data('index');
                var form = $(this).data('form');


                window.open("{{ url('admin/crawler-result-delete') }}/" + id + "/" + index, "_self");

            }
        });
	</script>
@endsection

