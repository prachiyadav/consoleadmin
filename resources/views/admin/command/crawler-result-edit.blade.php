@extends('layouts.admin.master')

@section('title', 'Edit Crawlers Result')

@section('scoped_css')
    <style>
        th, *{
            text-transform: none !important;
        }
    </style>
@endsection

@section('content')
    <div class="app-content">
        <div class="side-app">

            <!--Page header-->
            <div class="page-header">
                <div class="page-leftheader">
                    <h4 class="page-title">Edit Crawlers Result</h4>
                    <ol class="breadcrumb pl-0">
                        <li class="breadcrumb-item"><a href="{{ route('admin.command.index') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.command.index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.command.index') }}">Command</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Edit Crawlers Result</li>
                    </ol>
                </div>
            </div>
            <!--End Page header-->


            <!--Row-->
            <div class="row">
                <div class="col-md-8">
                    <h4 class="p-0 m-0 text-muted">Edit Crawlers Result</h4>
                    <hr class="my-3">
                    
                    {{-- Tab 1 --}}
                    <div class="card">
                        <div class="card-header bg-light">
                            <h3 class="card-title mr-auto">Edition.</h3>
                            {{-- <button class="btn btn-info"><i class="fa fa-plus"></i> Créer</button> --}}
                        </div>
                        <div class="card-body">
                            
                            <form method="POST" action="{{ route('admin.command.crawler-result-update') }}">
                                @csrf
                                <input type="hidden" name="id" value="{{ $id }}">
                                <input type="hidden" name="index" value="{{ $index }}">
                                <div class="row">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="url">Url</label>
                                            <input type="text" id="url" name="url" class="form-control" value="{{ $result->url }}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="lang">Lang</label>
                                            <input type="text" id="lang" name="lang" class="form-control" value="{{ $result->Lang }}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="type">Type</label>
                                            <input type="text" id="type" name="type" class="form-control" value="{{ $result->Type }}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="themes">Themes</label>
                                            <textarea class="form-control" id="themes" name="themes" rows="2" style="resize: none">@foreach ($result->themes as $theme){{ count($result->themes) > 1 ?  $theme.', ' : $theme }}@endforeach</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="secteurs">Secteurs</label>
                                            <textarea class="form-control" id="secteurs" name="secteurs" rows="2" style="resize: none">@foreach ($result->secteurs as $secteur){{ count($result->secteurs) > 1 ?  $secteur.', ' : $secteur }}@endforeach</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="fonctions">Fonctions</label>
                                            <textarea class="form-control" id="fonctions" name="fonctions" rows="2" style="resize: none">@foreach ($result->fonctions as $fonction){{ count($result->fonctions) > 1 ?  $fonction.', ' : $fonction }}@endforeach</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="types_infos">Types Infos</label>
                                            <textarea class="form-control" id="types_infos" name="types_infos" rows="2" style="resize: none">@foreach ($result->types_infos as $types_info){{ count($result->types_infos) > 1 ?  $types_info.', ' : $types_info }}@endforeach</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-info">Enregistrement</button>
                                        </div>
                                    </div>

                                </div>
                            </form>

                        </div>
                    </div>

                </div>

            </div>
            <!--End row-->
        </div>
    </div><!-- end app-content-->
@endsection


@section('scoped_js')
    <script>
        $('.datatable').each(function () {
            $(this).DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
            });
        });
    </script>
@endsection

