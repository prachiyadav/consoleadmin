@extends('layouts.admin.master')

@section('title', 'Edit Parser')

@section('scoped_css')
    <style>
        th, *{
            text-transform: none !important;
        }
    </style>
@endsection

@section('content')
    <div class="app-content">
        <div class="side-app">

            <!--Page header-->
            <div class="page-header">
                <div class="page-leftheader">
                    <h4 class="page-title">Edit Parser</h4>
                    <ol class="breadcrumb pl-0">
                        <li class="breadcrumb-item"><a href="{{ route('admin.command.index') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.command.index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.command.index') }}">Command</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Edit Parser</li>
                    </ol>
                </div>
            </div>
            <!--End Page header-->


            <!--Row-->
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <h4 class="p-0 m-0 text-muted">Origine sélectionnée</h4>
                    <hr class="my-3">
                    <div class="card">
                        <div class="card-header bg-light">
                            <h3 class="card-title mr-auto">Edition.</h3>
                            {{-- <button class="btn btn-info"><i class="fa fa-plus"></i> Créer</button> --}}
                        </div>
                        <div class="card-body">
                            @if (!empty($parser))
                                <form method="POST" action="{{ route('admin.command.parser-update') }}">
                                    @csrf
                                    <input type="hidden" name="id" value="{{ $parser->id }}">
                                    <div class="row">

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="title">Title</label>
                                                <input type="text" id="title" name="title" class="form-control" value="{{ $parser->title }}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="fonction">Fonction</label>
                                                <input type="text" id="fonction" name="fonction" class="form-control" value="{{ $parser->fonction }}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="secteur">Secteur</label>
                                                <input type="text" id="secteur" name="secteur" class="form-control" value="{{ $parser->secteur }}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="typedinfo">Type De Info</label>
                                                <input type="text" id="typedinfo" name="typedinfo" class="form-control" value="{{ $parser->Typedinfo }}">
                                            </div>
                                        </div>
                                        {{-- <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="sku">SKU</label>
                                                <input type="text" id="sku" name="sku" class="form-control" value="{{ $parser->sku }}">
                                            </div>
                                        </div> --}}

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="theme">Theme</label>
                                                <input type="text" id="theme" name="theme" class="form-control" value="{{ $parser->theme }}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="origine">Origine</label>
                                                <input type="text" id="origine" name="origine" class="form-control" value="{{ $parser->origine }}" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="dynamic">Dynamic</label>
                                                <input type="text" id="dynamic" name="dynamic" class="form-control" value="{{ $parser->dynamic }}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="language">Language</label>
                                                <input type="text" id="language" name="language" class="form-control" value="{{ $parser->language }}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="typededoc">Typededoc</label>
                                                <input type="text" id="typededoc" name="typededoc" class="form-control" value="{{ $parser->typededoc }}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="status">Status</label>
                                                <input type="text" id="status" name="status" class="form-control" value="{{ $parser->status }}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="sku">SKU</label>
                                                <input type="text" id="sku" name="sku" class="form-control" value="{{ $parser->sku }}" readonly>
                                            </div>
                                        </div>
                                        {{-- <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="publicated">
                                                    <input type="checkbox" id="publicated" name="publicated" class="mr-2" value="1" @if ($parser->publicated) checked @endif>
                                                Publicated</label>
                                            </div>
                                        </div> --}}


                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-info">Enregistrement</button>
                                                <button type="button" data-code="{{ $code }}" data-parserid="{{ $parser->id }}" class="btn btn-danger float-right delete-parser">Deletion</b>
                                            </div>
                                        </div>

                                    </div>
                                </form>

                            @else
                                <p class="text-center h4">No data</p>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
            <!--End row-->

        </div>
    </div><!-- end app-content-->
@endsection


@section('scoped_js')
    <script>
        $('.datatable').each(function () {
            $(this).DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
            });
        });

        $(document).on('click', '.deleteParser', function (e) {
            e.preventDefault();
            var parserId = $(this).data('parserid');
            var parserCode = $(this).data('code');
            // $(this).click(function (e) {
                if (confirm('Are you sure you want to delete this record ?')) {
                    $.get("{{ url('admin/parser-delete') }}/"+parserId+'/'+parserCode, {}, function (response) {
                        Swal.fire('Success', response, 'success');
                        setTimeout(function () {
                            window.location.href = "{{ url('admin/parser-list') }}/"+parserCode;
                        }, 1500);
                    });
                }
            // });
        });

    </script>
@endsection

