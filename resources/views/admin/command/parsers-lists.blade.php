@extends('layouts.admin.master')

@section('title', 'Parsers Result')

@section('scoped_css')
	<style>
		th, * {
			text-transform: none !important;
		}
	</style>
@endsection

@section('content')
	<div class="app-content">
		<div class="side-app">

			<!--Page header-->
			<div class="page-header">
				<div class="page-leftheader">
					<h4 class="page-title">Parser Result</h4>
					<ol class="breadcrumb pl-0">
						<li class="breadcrumb-item"><a href="{{ route('admin.command.index') }}">Home</a></li>
						<li class="breadcrumb-item"><a href="{{ route('admin.command.index') }}">Dashboard</a></li>
						<li class="breadcrumb-item"><a href="{{ route('admin.command.index') }}">Command</a></li>
						<li class="breadcrumb-item active" aria-current="page">Parsers Result</li>
					</ol>
				</div>
			</div>
			<!--End Page header-->


			<!--Row-->
			<div class="row">
				<div class="col-md-12">
					<h4 class="p-0 m-0 text-muted">Origine sélectionnée: ({{ $originName }})</h4>
					<hr class="my-3">
					{{-- Tab 1 --}}
					<div class="card">
						<div class="card-header bg-light">
							<h3 class="card-title mr-auto">Results.</h3>
							{{-- <button class="btn btn-info"><i class="fa fa-plus"></i> Créer</button> --}}
						</div>
						<div class="card-body" style="overflow-x: scroll">
							<table id="dataTable" class="table table-bordered table-hover dataTable">
								<thead>
								<tr>
									<th class="d-none">#</th>
									<th>Origine</th>
									<th>Title</th>
									<th>Fonction</th>
									<th>Sector</th>
									<th>Theme</th>
									<th>Typedinfo</th>
									<th>Dynamic</th>
									<th>Lang</th>
									<th>Type de doc</th>
									<th>Status</th>
									<th>SKU</th>
									{{-- <th>Publicated</th> --}}
									<th>Action</th>
								</tr>
								</thead>
								<tbody>

								@forelse ($parsers as $index=>$parser)

									<form action="#" method="POST">
										<tr>
											<td class="d-none">{{ $index }}</td>
											<td>{{ $parser->origine }}</td>
											<td>{{ $parser->title }}</td>
											<td>{{ $parser->fonction }}</td>
											<td>{{ $parser->secteur }}</td>
											<td>{{ $parser->theme }}</td>
											<td>{{ $parser->Typedinfo }}</td>
											<td>{{ $parser->dynamic }}</td>
											<td>{{ $parser->language }}</td>
											<td>{{ $parser->typededoc }}</td>
											<td>{{ $parser->status }}</td>
											<td>{{ $parser->sku }}</td>
											{{-- <td>{!! $parser->publicated == 1 ? '<span class="badge badge-success"><b>Publicated</b></span>' : '<span class="badge badge-danger"><b>Not Publicated</b></span>' !!}</td> --}}
											<td>
												<a href="{{ url('admin/parser-edit/'.$parser->id.'/'.$code) }}"
												   class="p-1"><i class="fa fa-edit"></i></a>
												<a href="javascript:void(0);" class="p-1 text-danger deleteParser"
												   data-code="{{ $code }}" data-id="{{ $parser->id }}"><i
															class="fa fa-trash"></i></a>

											</td>

										</tr>
									</form>

								@empty
									<p>No replies</p>
								@endforelse
								</tbody>
							</table>
						</div>
					</div>


				</div>

			</div>
			<!--End row-->


		</div>
	</div><!-- end app-content-->
@endsection


@section('scoped_js')
	<script>
        $('.dataTable').each(function () {
            $(this).DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
            });
        });

        // $('.deleteParser').each(function(){
        //     $(this).on('click', function(e){
        //         if (confirm('Are you sure you want to delete this record?')) {
        //             var id = $(this).data('id');

        //             window.open("{{ url('admin/parser-delete') }}/"+id, "_self");

        //         }
        //     });
        // });

        $(document).on('click', '.deleteParser', function (e) {
            var parserId = $(this).data('id');
            var parserCode = $(this).data('code');
            e.preventDefault();
            if (confirm('Are you sure you want to delete this record ?')) {
                $.get("{{ url('admin/parser-delete') }}/" + parserId + '/' + parserCode, {}, function (response) {
                    Swal.fire('Success', response, 'success');
                    setTimeout(function () {
                        window.location.reload();
                    }, 1500);
                });
            }
        });

	</script>
@endsection

