<!-- User Create  Modal -->
<div class="modal fade" id="userCreateModal" tabindex="-1" role="dialog" aria-labelledby="userCreateModalTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form action="{{ route('admin.parameter.referentiels-create-fonction') }}" method="post">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="userCreateModalTitle">Ajouter un nouvel utilisateur</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="userName">Nom</label>
                        <input type="text" class="form-control" name="user_name" placeholder="Nom">
                    </div>
                    <div class="form-group">
                        <label for="userEmail">Email</label>
                        <input type="text" class="form-control" name="user_email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label for="userSub">ID Sub</label>
                        <input type="number" class="form-control" name="user_sub" id="userSub" placeholder="Sub">
                    </div>
                    <div class="form-group">
                        <select class="form-control required select2" id="user_isAdmin" name="Admin?">
                            <option value="0" selected>Non</option>
                            <option value="1" selected>Oui</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <select class="form-control required select2" id="fonction_code" name="fonction_code">
                            <option value="0" selected>Non</option>
                            <option value="1" selected>Oui</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Groupe d'authorisations</label>
                        <select class="form-control required select2" id="fonction_applications"
                                name="fonction_applications[]" multiple>
                            <option value="" disabled>--Choisir un groupe--</option>
                            @foreach (App\AuthGroup::get() as $authgroup)
                                <option value="{{ $authgroup->code }}">{{ $authgroup->code }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-success">Enregistrer</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Fonction Edit  Modal -->
<div class="modal fade" id="fonctionEditModal" tabindex="-1" role="dialog" aria-labelledby="userEditModalTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form action="{{ route('admin.parameter.referentiels-update-fonction') }}" method="post">
                @csrf
                <input type="hidden" name="fonction_id" id="fonctionId">
                <div class="modal-header">
                    <h5 class="modal-title" id="userEditModalTitle">Modifier un utilisateur</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="userName">Nom</label>
                        <input type="text" class="form-control" name="user_name" id="userName" placeholder="Nom">
                    </div>
                    <div class="form-group">
                        <label for="userEmail">Email</label>
                        <input type="text" class="form-control" name="user_email" id="userEmail" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label for="userSub">ID Sub</label>
                        <input type="number" class="form-control" name="user_sub" id="userSub" placeholder="Sub">
                    </div>
                    <div class="form-group">
                        <select class="form-control required select2" id="user_isAdmin" name="Admin?">
                            <option value="0" selected>Non</option>
                            <option value="1" selected>Oui</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <select class="form-control required select2" id="fonction_code" name="fonction_code">
                            <option value="0" selected>Non</option>
                            <option value="1" selected>Oui</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Groupe d'authorisation</label>
                        <select class="form-control required select2" id="fonction_applicationsEdit"
                                name="fonction_applications[]" multiple>
                            <option value="" disabled>--Choisissez le groupe d'authorisation--</option>
                            @foreach (App\AuthGroup::get() as $authgroup)
                                <option value="{{ $authgroup->name }}">{{ $authgroup->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-success">Enregistrer</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- AuthGroup --}}
<!-- AuthGroup Create  Modal -->
<div class="modal fade" id="authGroupCreateModal" tabindex="-1" role="dialog"
     aria-labelledby="authGroupCreateModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form action="{{ route('admin.parameter.referentiels-create-authgroup') }}" method="post">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="typedeinfoEditModalTitle">Create new auth group</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{--                    <div class="form-group">--}}
                    {{--                        <select class="form-control required select2" id="authGroupCode" name="auth_groups_code">--}}
                    {{--                            <option value="" disabled>--Select Application--</option>--}}
                    {{--                            <option value="">New Code</option>--}}
                    {{--                            @foreach (App\AuthGroup::get() as $ag)--}}
                    {{--                                <option value="{{ $ag->code }}">{{ $ag->code }}</option>--}}
                    {{--                            @endforeach--}}
                    {{--                        </select>--}}
                    {{--                    </div>--}}
                    <div class="form-group">
                        <label for="authGroupDesc">Description</label>
                        <textarea rows="4" class="form-control" name="auth_group_desc" id="authGroupDesc"
                                  placeholder="Description"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Applications</label>
                        <select class="form-control required select2" id="authGroupApplications"
                                name="auth_group_applications[]" multiple>
                            <option value="" disabled>--Select Application--</option>
                            @foreach (App\Application::get() as $apps)
                                <option value="{{ $apps->name }}">{{ $apps->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-success">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- AuthGroup Edit  Modal -->
<div class="modal fade" id="authGroupEditModal" tabindex="-1" role="dialog" aria-labelledby="authGroupEditModalTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form action="{{ route('admin.parameter.referentiels-update-authgroup') }}" method="post">
                @csrf
                <input type="hidden" name="auth_group_id" id="authGroupIdEdit">
                <div class="modal-header">
                    <h5 class="modal-title" id="authGroupEditModalTitle">Edit AuthGroup</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <h3>Code: <span id="authGroupCodeEdit"></span></h3>
                        {{--                        <label for="authGroupCodeEdit">Code</label>--}}
                        {{--                        <input type="text" class="form-control" name="auth_group_code" id="authGroupCodeEdit" placeholder="Code">--}}
                    </div>
                    <div class="form-group">
                        <label for="authGroupDescEdit">Description</label>
                        <textarea rows="4" class="form-control" name="auth_group_desc" id="authGroupDescEdit"
                                  placeholder="Description"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Applications</label>
                        <select class="form-control required select2" id="authGroupApplicationsEdit"
                                name="auth_group_applications[]" multiple>
                            <option value="" disabled>--Select Application--</option>
                            @foreach (App\Application::get() as $apps)
                                <option value="{{ $apps->name }}">{{ $apps->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-success">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
