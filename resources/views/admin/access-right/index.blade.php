@extends('layouts.admin.master')

@section('title', 'Droits d\'accès')

@section('scoped_css')
    <style>
        th, * {
            text-transform: none !important;
        }

        tr td button.btn {
            background: transparent;
            outline: none !important;
            box-shadow: none !important
        }

        .select2-container {
            width: 100%;
            display: block;
        }
    </style>
@endsection

@section('content')
    <div class="app-content">
        <div class="side-app">

            <!--Page header-->
            <div class="page-header">
                <div class="page-leftheader">
                    <h4 class="page-title">Droit d'accès</h4>
                    <ol class="breadcrumb pl-0">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Utilisateurs</li>
                    </ol>
                </div>
            </div>
            <!--End Page header-->

            <div id="access-right">
                <users-table applications="{{json_encode($applications)}}" users="{{ json_encode($users) }}"
                             authgroups="{{ json_encode($authgroups) }}">
                </users-table>
            </div>


        </div>
    </div><!-- end app-content-->


    @include('admin.access-right.modal.modal')

@endsection


@section('scoped_js')
    <script>
        $('.datatable').each(function () {
            $(this).DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
            });
        });


        // Fonction
        $('.editUser').each(function () {
            $(this).on('click', function (e) {
                e.preventDefault();

                // Get Fonction data from Fonction Object (PHP)
                var user = $(this).data('user');

                // Set applications as array and handle it

                // var fonctionArr = [];
                if (user.authgroups.indexOf(',')) {
                    userArr = user.authgroups.split(',');
                }
                var selectedVal = new Array();
                jQuery.each(userArr, function (index) {
                    selectedVal[index] = userArr[index];
                });
                // Set values for the fonction fields
                $('#fonctionId').val(user.id);
                $('#userName').val(user.desc_fr);
                $('#userEmail').val(user.desc_en);
                $('#fonction_applicationsEdit').select2();
                $('#fonction_applicationsEdit').val(selectedVal).trigger('change');

                // Set button text
                $('#fonctionEditModal button[type="submit"]').text('Mettre à jour');

                // Show Fonction Modal
                $('#fonctionEditModal').modal('show');
            });
        });

        $('#addUser').on('click', function (e) {
            e.preventDefault();
            // Set button text
            $('#userCreateModal button[type="submit"]').text('Enregistrer');
            $('#fonction_code, #fonction_applications').select2();
            // Show Fonction Modal
            $('#userCreateModal').modal('show');
        });

        // Deleting Items
        $('.deleteUser').each(function () {
            var target = $(this).data('target');
            var id = $(this).data('id');
            $(this).on('click', function () {
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You want to delete this ?",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.value) {
                        var data = {_token: "{{ csrf_token() }}", id: id, target: target};
                        $.post("{{ route('admin.parameter.referentiels-delete-items') }}", data, function (res) {
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: res.message,
                                showConfirmButton: false,
                                timer: 2500,
                                toast: true
                            });

                            setTimeout(function () {
                                window.location.reload();
                            }, 3500);

                        })
                    }
                })
            });
        })

        $('.tab-btn').each(function () {
            $(this).on('click', function () {
                var name = $(this).text();
                $('#tabName').text(name);
            })
        })

    </script>
@endsection

