@extends('layouts.admin.master')

@section('title', 'Home')

@section('scoped_css')
    <style>
        .side-menu .slide .slide-menu .slide-item:hover{
            color: #fff !important;
            /*background: #0b7ec4;*/
        }
        .side-menu .side-menu__item:hover .side-menu__label, .side-menu .side-menu__item:hover .side-menu__icon{
            color: #fff !important;
        }
    </style>
@endsection

@section('content')
    <div class="app-content">
        <div class="side-app">

            <!--Page header-->
            <div class="page-header">
                <div class="page-leftheader">
                    <h4 class="page-title">Dashboard</h4>
                    <ol class="breadcrumb pl-0">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                    </ol>
                </div>
            </div>
            <!--End Page header-->


            <!--Row-->
            <div class="row">
                <div class="col-lg-7 col-md-12">

                    {{-- Tab 1 --}}
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h3>
                        </div>
                        <div class="card-body">

                            <form action="">
                                <div class="form-group">
                                    <label for="">Lorem ipsum.</label>
                                    <select name="demo" id="demo" class="form-control"multiple="multiple">
                                        <option value="0">Lorem ipsum.</option>
                                        <option value="0">ipsum lorem.</option>
                                        <option value="0">Lorem ipsum.</option>
                                        <option value="0">Lorem ipsum.</option>
                                        <option value="0">Lorem ipsum.</option>
                                    </select>
                                </div>
                            </form>
                        </div>
                    </div>

                    {{-- Tab 2 --}}
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Lorem ipsum dolor sit amet.</h3>
                        </div>
                        <div class="card-body">

                            <form action="">
                                <div class="form-group">
                                    <label for="">Lorem ipsum.</label>
                                    <select name="demo2" id="demo2" class="form-control" multiple="multiple">
                                        <option value="0">Lorem ipsum.</option>
                                        <option value="0">ipsum lorem.</option>
                                        <option value="0">Lorem ipsum.</option>
                                        <option value="0">Lorem ipsum.</option>
                                        <option value="0">Lorem ipsum.</option>
                                    </select>
                                </div>
                            </form>
                        </div>
                    </div>

                    {{-- Tab 3 --}}
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Lorem ipsum dolor sit.</h3>
                        </div>
                        <div class="card-body">

                            <form action="">
                                <div class="form-group">
                                    <label for="">Lorem ipsum.</label>
                                    <select name="demo3" id="demo3" class="form-control" multiple="multiple">
                                        <option value="0">Lorem ipsum.</option>
                                        <option value="0">ipsum lorem.</option>
                                        <option value="0">Lorem ipsum.</option>
                                        <option value="0">Lorem ipsum.</option>
                                        <option value="0">Lorem ipsum.</option>
                                    </select>
                                </div>
                            </form>
                        </div>
                    </div>



                </div>

                <div class="col-lg-5 col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Action</h3>
                        </div>
                        <div class="card-body">
                            <p>Veuillez d'abord choisir des Urls </p>
                        </div>
                    </div>
                </div>

            </div>
            <!--End row-->


        </div>
    </div><!-- end app-content-->
@endsection


@section('scoped_js')
    <script>
        $('#demo, #demo2, #demo3').select2();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
        });
    </script>
@endsection

