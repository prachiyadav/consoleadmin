
<!-- Style css -->
<link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" />

<!--Sidemenu css -->
<link href="{{ asset('assets/plugins/sidemenu/sidemenu.css') }}" rel="stylesheet">

<!-- P-scroll bar css-->
<link href="{{ asset('assets/plugins/p-scrollbar/p-scrollbar.css') }}" rel="stylesheet" />

<!---Icons css-->
<link href="{{ asset('assets/plugins/web-fonts/icons.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/plugins/web-fonts/font-awesome/font-awesome.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/plugins/web-fonts/plugin.css') }}" rel="stylesheet" />

<!-- Skin css-->
<link href="{{ asset('assets/css/skins.css') }}" rel="stylesheet" />
{{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
<link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" />

