<!--aside open-->
<aside class="app-sidebar">
    <ul class="side-menu">
        {{-- Here we check if we are on the regular console app --}}
        @if(Request::url() == route('admin.access-right.index'))

            <li class="menu-header text-primary p-3">
                Droits d'accès
            </li>
            <li>
                <a class="side-menu__item" href="{{ route('admin.access-right.index') }}"><i class="side-menu__icon fe fe-users"></i><span class="side-menu__label">Utilisateurs</span></a>
            </li>
        {{-- Here we check if the user is admin and if we are on the access_rights app --}}
        @else
            <li class="menu-header text-primary p-3">
                Console
            </li>
            <li>
                <a class="side-menu__item" href="{{ route('admin.command.index') }}"><i class="side-menu__icon">>_</i><span class="side-menu__label">Commandes</span></a>
            </li>
            <li class="slide">
                <a class="side-menu__item"  data-toggle="slide" href="#"><i class="side-menu__icon fe fe-code"></i><span class="side-menu__label">Paramètres</span><i class="angle fa fa-angle-right"></i></a>
                <ul class="slide-menu">
                    <li><a class="slide-item"  href="{{ route('admin.parameter.origin-index') }}"><span>Origine</span></a></li>
                    <li><a class="slide-item"  href="{{ route('admin.parameter.referentiels-index') }}"><span>Référentiels</span></a></li>
                    <li><a class="slide-item"  href="{{ route('admin.parameter.km-index') }}"><span>KM</span></a></li>
                    <li><a class="slide-item"  href="{{ route('admin.parameter.collect-index') }}"><span>Collecte</span></a></li>
                </ul>
            </li>
        @endif
    </ul>
</aside>
<!--aside closed-->
