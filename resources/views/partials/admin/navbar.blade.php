<div class="app-header header top-header">
    <div class="container-fluid">
        <div class="d-flex">
            <a class="header-brand" href="{{ config('app.search_app_url') }}">
                <img src="{{ asset('assets/images/brand/logo.png') }}" class="header-brand-img desktop-lgo" alt="Clont logo">
                <img src="{{ asset('assets/images/brand/favicon.png') }}" class="header-brand-img mobile-logo" alt="Clont logo">
            </a>

            @if (!Route::is('admin.command.index'))
                <div class="dropdown side-nav">
                    <a  class="app-sidebar__toggle nav-link icon mt-1" onclick="goBack()" href="javascript:void(0)">
                        <i class="fe fe-arrow-left"></i>
                    </a><!-- Go back -->
                </div>
            @endif
            <div class="dropdown side-nav">
                <a aria-label="Hide Sidebar" class="app-sidebar__toggle nav-link icon mt-1" data-toggle="sidebar" href="#">
                    <i class="fe fe-align-left"></i>
                </a><!-- sidebar-toggle-->
            </div>
            <div class="dropdown side-nav ml-auto " >
                <a  class="nav-link icon"href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    <i class="mdi mdi-logout-variant"></i>
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </div>
    </div>
</div>
