<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>

<!-- Jquery js-->
<script src="{{ asset('assets/js/vendors/jquery-3.4.0.min.js') }}"></script>

<!-- Bootstrap4 js-->
<script src="{{ asset('assets/plugins/bootstrap/popper.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>

<!--Othercharts js-->
<script src="{{ asset('assets/plugins/othercharts/jquery.sparkline.min.js') }}"></script>

<!-- Circle-progress js-->
<script src="{{ asset('assets/js/vendors/circle-progress.min.js') }}"></script>

<!-- Jquery-rating js-->
<script src="{{ asset('assets/plugins/rating/jquery.rating-stars.js') }}"></script>

<!--Sidemenu js-->
<script src="{{ asset('assets/plugins/sidemenu/sidemenu.js') }}"></script>

<!-- P-scroll js-->
{{-- <script src="{{ asset('assets/plugins/p-scrollbar/p-scrollbar.js') }}"></script> --}}
{{-- <script src="{{ asset('assets/plugins/p-scrollbar/p-scroll.js') }}"></script> --}}

<!-- ECharts js -->
{{-- <script src="{{ asset('assets/plugins/echarts/echarts.js') }}"></script> --}}

<!-- Peitychart js-->
{{-- <script src="{{ asset('assets/plugins/peitychart/jquery.peity.min.js') }}"></script> --}}
{{-- <script src="{{ asset('assets/plugins/peitychart/peitychart.init.js') }}"></script> --}}

<!-- Apexchart js-->
{{-- <script src="{{ asset('assets/js/apexcharts.js') }}"></script> --}}

<!-- Index js-->
{{-- <script src="{{ asset('assets/js/index1.js') }}"></script> --}}

<!-- Custom js-->
<script src="{{ asset('assets/js/custom.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<script>
    $('.loadingTrigger').each(function () {
        $(this).on('click', function (e) {
            var oldWidth = $(this).width();
            var oldHeight = $(this).height();
            var text = $(this).data('loadingText') ? $(this).data('loadingText') : 'Loading...';
            $(this).attr('disabled', 'disabled');
            $(this).html('<span><img src="{{ asset('assets/images/loading.gif') }}" style="width: 14px" /> &nbsp;'+text+'</span>');
            $(this).width(oldWidth);
        });
    })
</script>
