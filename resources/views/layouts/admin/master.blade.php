<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" sizes="192x192" href="images/decision-favicon.png">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> @yield('title') | {{ config('app.name', 'WDream') }}</title>
    <!-- Styles -->


    @include('partials.admin.css')
    <style>
        .side-menu .slide .slide-menu .slide-item:hover{
            color: #fff !important;
            background: #2D66F7;
        }
        .side-menu .side-menu__item:hover, .side-menu .side-menu__item:hover{
            color: #fff !important;
            background: #2D66F7;
        }
        .side-menu .side-menu__item:hover .side-menu__label, .side-menu .side-menu__item:hover .side-menu__icon{
            color: #fff !important;
        }
    </style>
    @yield('scoped_css')

</head>
<body class="app sidebar-mini">

    <!---Global-loader-->
    <div id="global-loader" >
        <img src="{{ asset('assets/images/svgs/loader.svg') }}" alt="loader">
    </div>
    <div class="page"  id="app">
        <div class="page-main">

            @include('partials.admin.navbar')
            @include('partials.admin.sidebar')

            <main>
                @yield('content')
            </main>
        </div>

    @include('partials.admin.footer')

</div>
@include('partials.admin.js')
<script>
    function goBack() {
        window.history.back();
    }

    function toastSuccess(msg, timer = 2500, position = 'top-end') {
        Swal.fire({
            position: position,
            icon: 'success',
            title: msg,
            showConfirmButton: false,
            toast: true,
            timer: timer
        });
    }

    function toastError(msg, timer = 2500, position = 'top-end') {
        Swal.fire({
            position: position,
            icon: 'error',
            title: msg,
            showConfirmButton: false,
            toast: true,
            timer: timer
        });
    }

    function toastAlert(msg, timer = 2500) {
        toastError(msg, timer);
    }

</script>


@if (Session::has('success'))
    <script>
        Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: '{{ Session::get("success") }}',
            showConfirmButton: false,
            timer: 1500,
            toast: true
        });
    </script>
@elseif(Session::has('alert'))
    <script>
        Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: '{{ Session::get("alert") }}',
            showConfirmButton: false,
            timer: 1500,
            toast: true
        });
    </script>
@endif

    @yield('scoped_js')

</body>
</html>
