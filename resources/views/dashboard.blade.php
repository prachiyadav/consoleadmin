<x-app-layout>

    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Applications
        </h2>
    </x-slot>

    <div class="py-12">
        <div class=" flex flex-col justify-center max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="grid lg:grid-cols-4 md:grid-cols-3 sm:grid-cols-2 grid-cols-1 gap-4 px-4">
                <!-- SMALL CARD ROUNDED -->
                <a href="{{config('app.search_app_url')}}">
                    <div
                        class="bg-white border-blue-600 dark:bg-gray-800 bg-opacity-95 border-opacity-60 | p-4 border-solid rounded-3xl border-2 | flex justify-around cursor-pointer | hover:bg-blue-400 dark:hover:bg-blue-600 hover:border-transparent | transition-colors duration-500">
                   <span style="font-size: 4rem; color: Dodgerblue;">
                         <i class="fas fa-search"></i>
                   </span>

                        <div class="flex flex-col justify-center">
                            <p class="text-gray-900 dark:text-white font-semibold">Recherche</p>
                        </div>
                    </div>
                </a>
                <!-- END SMALL CARD ROUNDED -->
                <!-- SMALL CARD ROUNDED -->
                <a href="{{config('app.crm_app_url')}}">
                    <div
                        class="bg-white border-blue-600 dark:bg-gray-800 bg-opacity-95 border-opacity-60 | p-4 border-solid rounded-3xl border-2 | flex justify-around cursor-pointer | hover:bg-blue-400 dark:hover:bg-blue-600 hover:border-transparent | transition-colors duration-500">
                   <span style="font-size: 4rem; color: Dodgerblue;">
                         <i class="fas fa-search"></i>
                   </span>

                        <div class="flex flex-col justify-center">
                            <p class="text-gray-900 dark:text-white font-semibold">CRM</p>
                        </div>
                    </div>
                </a>
                <!-- END SMALL CARD ROUNDED -->

                <!-- END SMALL CARD ROUNDED -->

                <!-- SMALL CARD ROUNDED -->
                <a href="{{config('app.360_app_url')}}">
                    <div
                        class="bg-white border-blue-600 dark:bg-gray-800 bg-opacity-95 border-opacity-60 | p-4 border-solid rounded-3xl border-2 | flex justify-around cursor-pointer | hover:bg-blue-400 dark:hover:bg-blue-600 hover:border-transparent | transition-colors duration-500">
                   <span style="font-size: 4rem; color: Dodgerblue;">
                         <i class="fas fa fa-globe-americas"></i>
                   </span>

                        <div class="flex flex-col justify-center">
                            <p class="text-gray-900 dark:text-white font-semibold">360°</p>
                        </div>
                    </div>
                </a>

                <!-- SMALL CARD ROUNDED -->
                <a href="{{config('app.admin_app_url')}}">
                    <div
                        class="bg-white border-blue-600 dark:bg-gray-800 bg-opacity-95 border-opacity-60 | p-4 border-solid rounded-3xl border-2 | flex justify-around cursor-pointer | hover:bg-blue-400 dark:hover:bg-blue-600 hover:border-transparent | transition-colors duration-500">
                   <span style="font-size: 4rem; color: Dodgerblue;">
                         <i class="fas fa-terminal"></i>
                   </span>

                        <div class="flex flex-col justify-center">
                            <p class="text-gray-900 dark:text-white font-semibold">Console d'administration</p>
                        </div>
                    </div>
                </a>
                <!-- END SMALL CARD ROUNDED -->
            </div>
        </div>
        <!--
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    You're logged in!
                </div>
            </div>
        </div>
           -->
    </div>
</x-app-layout>
<footer class="footer bg-white relative pt-1 border-b-2 border-blue-700">
    <div class="container mx-auto px-6">
        <div class="border-gray-300 flex flex-col items-center">
            <div class="sm:w-2/3 text-center py-6">
                <p class="text-sm text-blue-600 font-bold">
                    © {{date("Y")}} Décisionnel
                </p>
            </div>
        </div>
    </div>
</footer>
